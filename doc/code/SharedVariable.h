template<class functionality>
class SharedVariable: public functionality {
public:
	typedef typename functionality::Type Type;
	explicit SharedVariable(varid_t id, nodeid_t owningNode = NOOWNER);
	void start();
};
