...		
virtual void getAdvertiseUpdatePDUBuffer(varid_t varid, nodeid_t owner, 
										 size_t size, timestamp_t timestamp, 
										 SharingType type, 
										 reflex::Buffer* destBuf) 
{
	uint16 probability = 0;
	if(neighbours > 0) {
		if(neighbours < targetCount)
			range++;
		else
			probability = 	(UINT16_MAX / (uint16)neighbours) * 
							((uint16)targetCount - (uint16)replicates);
	}

	AdvertiseUpdate pdu = AdvertiseUpdate(	varid, owner, range, 
											timestamp, size, type );
	pdu.probability = probability;

	destBuf->reset();
	destBuf->write(pdu);

	replicates = 0;
	neighbours = 0;
}
...