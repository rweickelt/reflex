#ifndef LIS331_H
#define LIS331_H

#include "reflex/io/Ports.h"
#include "reflex/sinks/Event.h"
#include "reflex/usci/Registers.h"
#include "reflex/scheduling/ActivityFunctor.h"

namespace reflex {

class LIS331
{
    typedef msp430x::usci::RegistersA0 Registers;

public:

  enum PINS {
    PIN_CLK = 0x80,
    PIN_MISO = 0x20,
    PIN_MOSI = 0x40,
    PIN_CSN = 0x80
  };

  enum REGISTERS {
    WHO_AM_I    = 0x0F,
    CTRL_REG1   = 0x20,
    CTRL_REG2   = 0x21,
    CTRL_REG3   = 0x22,
    CTRL_REG4   = 0x23,
    CTRL_REG5   = 0x24,
    REFERENCE   = 0x26,
    STATUS_REG  = 0x27,
    OUT_X_L     = 0x28,
    OUT_X_H     = 0x29,
    OUT_Y_L     = 0x2A,
    OUT_Y_H     = 0x2B,
    OUT_Z_L     = 0x2C,
    OUT_Z_H     = 0x2D,
    INT1_CFG    = 0x30,
    INT1_SOURCE = 0x31,
    INT1_THS    = 0x32,
    INT1_DURATION = 0x33,
    INT2_CFG    = 0x34,
    INT2_SOURCE = 0x35,
    INT3_THS    = 0x36,
    INT4_DURATION = 0x37
  };

  enum MODE {
    READ        = 0x80,
    ADD_INC     = 0x40
  };

  Event requestData;     // triggered external

public:
  LIS331();

  void init();
  void getValues();

private:
  //! enable the module
  virtual void enable();
  //! disable the module
  virtual void disable();

  virtual void handle();

  void delay();


private:
  ActivityFunctor<LIS331, &LIS331::getValues> requestDataFunctor;
};

} //ns reflex

#endif // LIS331_H
