/*
 *      REFLEX - Real-time Event FLow EXecutive
 *
 *      A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EM430_LED_H
#define REFLEX_EM430_LED_H

#include "reflex/types.h"
#include "reflex/io/Ports.h"
#include "reflex/sinks/Sink.h"

namespace reflex {
class LED  : public Sink1<char>
{

public:
    LED();
    ~LED() {}

    enum Numeration {
      LED_1 = BIT_0,    // PORT 3.0
      LED_2 = BIT_1,    // PORT 3.1
      LED_3 = BIT_2,    // PORT 3.2
      LED_4 = BIT_3,    // PORT 3.3
      LED_5 = BIT_4     // PORT 3.4
    };

    void init();

    void toggleAll();

    /** This method sets the led to the wanted state.
     *  The value is inverted for the user, to get the
     *  more machine independet result.
     *  Implements the Sink1 interface.
     *  Uses numbers 0 to 5 instead Colour typ!
     *
     *  @param value : 0 to 5
     */
    void assign(char);

    void toggle(char);

};
}
#endif // LED_H
