#include "reflex/io/SHT21.h"
#include "reflex/MachineDefinitions.h"
#include "NodeConfiguration.h"

using namespace reflex;
using namespace msp430x;
using namespace usci;

SHT21::SHT21()
  : InterruptHandler(interrupts::USCIB0, PowerManageAble::PRIMARY )
  , startupTimer(VirtualTimer::ONESHOT)
  , timeoutTimer(VirtualTimer::ONESHOT)
  , getResultFunctor(*this)
  , timeoutFunctor(*this)
  , calculateFunctor(*this)
  , measureTempFunctor(*this)
  , measureHumidityFunctor(*this)
{
  startupTimer.init(resultAvailable);
  timeoutTimer.init(sensorTimeout);

  measureTemperature.init(&measureTempFunctor); // register activity for measuring temperature
  measureHumidity.init(&measureHumidityFunctor);// register activity for measuring humidity

  resultAvailable.init(&getResultFunctor);      // register Activity for interrupt
  sensorTimeout.init(&timeoutFunctor);          // register Activity for sensor timeout
  calculateResult.init(&calculateFunctor);

  this->setSleepMode(mcu::LPM4);
}


void SHT21::init(Sink1<int> *temperatureOutput, Sink1<int> *humidityOutput)
{
  this->temperature = temperatureOutput;
  this->humidity = humidityOutput;

  Port1()->SEL |= (PIN_SCL | PIN_SDA);

  //Baudrate - 100kHz
  Registers()->UCBxBR0 = 0x18;
  Registers()->UCBxBR1 = 0x00;

  Registers()->UCBxCTL1 |= (UCSWRST | UCSSELsmclk);
  Registers()->UCBxCTL0 = (UCMST | UCMODE3 | UCSYNC);
  Registers()->UCBxCTL1 &= ~UCSWRST;

  lock = ISFREE;
}

/**
 * Get result from SHT21.
 */
void SHT21::getResult()
{
  recFrameCount = 0;

  Registers()->UCBxCTL1 &= ~UCTR;       //receive mode
  Registers()->UCBxCTL1 |= UCTXSTT;     //start transmit

  Registers()->UCBxI2CSA = ADDRESS;
  Registers()->UCBxIE |= (UCTXIE | UCRXIE | UCSTTIE | UCSTPIE | UCALIE | UCNACKIE);
}

/**
 * Calculate the requested temperature or humidity.
 */
void SHT21::calculate()
{
  float result;
  uint16 value = (measuringResult[0] << 8) | (measuringResult[1] & 11111100);
  uint8 checksum = measuringResult[2];

//  Registers()->UCBxCTL1 |= UCSWRST;

  if(lock == TMEASURING) {
      result = -46.85 + (15.72 * value/65536);
      temperature->assign(int (result*10));
  } else if(lock == HMEASURING) {
      result = -6 + (125 * value/65536);
      humidity->assign(int (result*10));
  }

  disable();
  Registers()->UCBxCTL1 |= UCSWRST;
  lock = ISFREE;
}

void SHT21::timeout()
{
  Registers()->UCBxCTL1 |= UCTXSTP;
  lock = ISFREE;
}

/*
 * Handle communication interrupts.
 */
void SHT21::handle()
{
  if(Registers()->UCBxIFG & UCNACKIFG) {
      Registers()->UCBxCTL1 |= UCTXSTP;
      Registers()->UCBxIE = UCSTPIE;    //only stop ie
  } else if(Registers()->UCBxIFG & UCSTTIFG) {
      recFrameCount = 0;
      Registers()->UCBxIFG &= ~UCSTTIFG;
  } else if(Registers()->UCBxIFG & UCSTPIFG) {
      Registers()->UCBxIFG &= ~UCSTPIFG;

  } else if(Registers()->UCBxIFG & UCTXIFG) {
      //sensor ready, now start transfer (maybe, an Ack was send)
      Registers()->UCBxIFG &= ~UCTXIFG;
  } else if(Registers()->UCBxIFG & UCRXIFG) {
      measuringResult[recFrameCount] = Registers()->UCBxRXBUF;

      recFrameCount++;

      if(recFrameCount >= 3) {
        Registers()->UCBxCTL1 |= UCTXSTP;
        timeoutTimer.set(0);
        calculateResult.notify();
      }

      Registers()->UCBxIFG &= ~UCRXIFG;
  }
}

void SHT21::assign(uint8 t)
{

}


void SHT21::enable()
{
  Registers()->UCBxCTL1 &= ~UCSWRST;
}

void SHT21::disable()
{
  Registers()->UCBxCTL1 |= UCSWRST;
}

/**
 * delay
 * just a little spinning for clear signals
 */
inline void SHT21::delay() {
    volatile int i = 0;
    while (i != 1000) ++i;
}

void SHT21::triggerTempMeasurement()
{
  enable();
  Registers()->UCBxCTL1 &= ~UCSWRST;

  delay();

  if(lock == ISFREE) {
    lock = TMEASURING;

    Registers()->UCBxCTL1 |= (UCTR | UCTXSTT);
    Registers()->UCBxTXBUF = TEMPERATURE;
    Registers()->UCBxI2CSA = ADDRESS;
    Registers()->UCBxIE |= (UCTXIE | UCRXIE | UCSTTIE | UCSTPIE | UCALIE | UCNACKIE);

    startupTimer.set(SHT_MEASUREMENT_MSEC);
    timeoutTimer.set(SHT_TIMEOUT_MSEC);
  }
}


void SHT21::triggerHumidityMeasurement()
{
  enable();
  Registers()->UCBxCTL1 &= ~UCSWRST;

  delay();

  if(lock == ISFREE) {
    lock = HMEASURING;

    Registers()->UCBxCTL1 |= (UCTR | UCTXSTT);
    Registers()->UCBxTXBUF = HUMIDITY;
    Registers()->UCBxI2CSA = ADDRESS;
    Registers()->UCBxIE |= (UCTXIE | UCRXIE | UCSTTIE | UCSTPIE | UCALIE | UCNACKIE);

    startupTimer.set(SHT_MEASUREMENT_MSEC);
    timeoutTimer.set(SHT_TIMEOUT_MSEC);
  }
}
