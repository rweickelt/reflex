
#include <omnetpp.h>
#include "NetwToMacControlInfo.h"

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptVector.h"
#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Event.h"
#include "ReflexPacket_m.h"

namespace reflex {


template<mcu::interrupts::InterruptVector TGateInd>
class OutGateWrapper
	: protected Activity
{
protected:
	cSimpleModule& module;
	Sink0* senderNotification;

public:
	Queue<Buffer*> input;
public:
	OutGateWrapper()
		: module(getModule())
	{
		input.init(this);
		senderNotification = NULL;
	}

	void initOutGateWrapper(Sink0* sn) {
		senderNotification = sn;
	}
protected:
	virtual void run();

};
/**
 *
 */
template<mcu::interrupts::InterruptVector TGateInd>
void OutGateWrapper<TGateInd>::run() {
	Buffer* buffer = input.get();
	Assert(buffer);

	cGate* gate= module.gate("lowerGateOut",TGateInd);
	Assert(gate);

	ReflexPacket* reflexPkg = new ReflexPacket("ReflexPacket",TGateInd);

	// push top of stack to buffer
//	buffer->push( buffer->getFreeStackSpace() );

	unsigned length= buffer->getLength();
	uint8* data = buffer->getStart();
	reflexPkg->setPayLoadArraySize(length+1); //+1 for tos

	//write tos
	reflexPkg->setPayLoad(0,buffer->getFreeStackSpace());

	// the Mac layer needs the control info from the network layer to process
	// the destination address.
	NetwToMacControlInfo* cInfo = new NetwToMacControlInfo(LAddress::L2BROADCAST); // standard broadcast destination
	reflexPkg->setControlInfo(cInfo);

	for (unsigned i=0; i<length;++i ) {
		reflexPkg->setPayLoad(i+1,*(data++));
	}

	buffer->downRef();

	module.send(reflexPkg,gate);

	if (senderNotification) {
		senderNotification->notify();
	}
}


}
