/*
 * OMNeTRadio.h
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#ifndef OMNETRADIO_H_
#define OMNETRADIO_H_

#include "OMNeTRadio_Structure.h"
#include "ReflexPacket_m.h"

class OMNeTRadio : public OMNeTRadio_Structure<OMNeTRadio>{
private:

	bool withDebug;

public:
	OMNeTRadio(bool withDebug = false);

	void handleUpperLayerIn_Impl();

	void handleLowerLayerIn_Impl();

	void handleLowerControlIn_Impl();

	void passMessage( char* buffer, int length );
};

#endif /* OMNETRADIO_H_ */
