#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 S�ren H�ckenr
# *
# */

CC_SOURCES_PLATFORM += \
	timer/SystemTimer.cc \
	timer/HardwareTimerMilli.cc \
	scheduling/SchedulerWrapper.cc \
	power.cc


CC_SOURCES_OMNETPP += \
	ReflexBaseApp.cc


CC_SOURCES_LIB += \
	timer/VirtualTimer.cc \
	timer/VirtualizedTimer.cc \

MSG_SOURCES_PLATFORM = \
	msg/ReflexPacket.msg

C_SOURCES_PLATFORM += \


ASM_SOURCES_PLATFORM += \
