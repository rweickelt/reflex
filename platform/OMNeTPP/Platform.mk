#/**
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Soeren Hoeckner
# *
# */

CONTROLLER = linux

ifndef DEBUG_PLATFORM
	DEBUG_PLATFORM = 0
endif

ASFLAGS +=


CXXFLAGS += -O1 -g -Wall -Wno-non-virtual-dtor -DEMULATOR=OMNetPP


#ifeq ($(GENSYS), DARWIN)
#	CFLAGS += -fPIC
#	CXXFLAGS += -fPIC
#	LDFLAGS += -dynamiclib -fno-common -undefined dynamic_lookup
#endif
#
#ifeq ($(GENSYS), LINUX)
#	CFLAGS += -fpic
#	CXXFLAGS += -fpic
#	LDFLAGS += -shared
#endif
#
#ifeq ($(GENSYS), CYGWIN)
#	CFLAGS += -fpic
#	CXXFLAGS += -fpic
#	LDFLAGS += -shared
#endif


ifdef OMNETSTACKSIZE
	CXXFLAGS += -DOMNETSTACKSIZE=$(OMNETSTACKSIZE)
endif




#######################################################################

INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

#check for OMNetPP include paths
#ifdef OMNETPPPATH
#	DEFINED_REFLEX_VARS +=\t"OMNETPPPATH = "\t$(OMNETPPPATH)\n
#	INCLUDES +=	-I$(OMNETPPPATH)/include
#else
#	MISSING_REFLEX_VARS +=\t"OMNETPPPATH:"\t"Path to OMNetPP root"\n
#endif
#
#ifdef INETPATH
#	DEFINED_REFLEX_VARS +=\t"INETPATH = "\t$(INETPATH)\n
#	INCLUDES +=	-I$(INETPATH)/Base \
#				-I$(INETPATH)/World \
#				-I$(INETPATH)/Util \
#				-I$(INETPATH)/NetworkInterfaces/Ieee80211/Mac \
#				-I$(INETPATH)/NetworkInterfaces/MFCore \
#				-I$(INETPATH)/NetworkInterfaces/Radio \
#				-I$(INETPATH)/NetworkInterfaces/Contract \

#else
#	MISSING_REFLEX_VARS +=\t"INETPATH:"\t"Path to INET-Framework root "\n
#endif



### DebugFlags
ifneq ($(DEBUG_PLATFORM), 0)
 DEBUG_FLAGS += -g -DDEBUG_PLATFORM -fno-inline-functions -fno-inline
endif

# Include the Platform depending default sources
sinclude $(REFLEXPATH)/platform/$(PLATFORM)/Sources.mk

CXX_SOURCES += \
	$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/omnetpp/,$(CC_SOURCES_OMNETPP))

#place for generated files
GEN_DIR = gen

#add messagefiles used by platform
#MSG_SOURCES += \
	#$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/omnetpp/,$(MSG_SOURCES_PLATFORM))
MSG_SOURCES += \
	$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/omnetpp/,$(MSG_SOURCES_PLATFORM))


#add include paths to msg_files
#INCLUDES += $(addprefix -I,$(dir $(MSG_SOURCES)))
INCLUDES += $(addprefix -I,$(GEN_DIR))

#inputfiles for swig
SWIG_SOURCES += \



#Wrapper target to obtain usage of the standalone make-command


#add generated files to Sources
#CXX_SOURCES += $(MSG_SOURCES:.msg=_m.cc)
CXX_SOURCES += $(addprefix $(GEN_DIR)/,$(notdir $(MSG_SOURCES:.msg=_m.cc)))



#dependencies depended by make depend 	:-O
#MISSING_FILES += $(MSG_SOURCES:.msg=_m.gen)
MISSING_FILES += $(addprefix $(GEN_DIR)/,$(notdir $(MSG_SOURCES:.msg=_m.gen)))

#takes care of these files are generated before calculating the dependencies

vpath %.gen $(GEN_DIR)

vpath %.swig $(GEN_DIR)

#@echo $(MSG_SOURCES)
#@echo $(@:_m.gen=.msg)
#@echo $(filter %$(notdir $(@:_m.gen=.msg)),$(MSG_SOURCES))

_THE_FIRST_TARGET_: all

#generate the missing files. before calculate dependencies
#		mv --target-directory=$(GEN_DIR) $(notdir $(@:.gen=.*)) &&
%.gen:	$(GEN_DIR)
	@echo -n "generate OmnetPP MSG files $@ ... " && \
		opp_msgc $(filter %$(notdir $(@:_m.gen=.msg)),$(MSG_SOURCES)) -h && \
		mv -f $(notdir $(@:.gen=.*)) $(GEN_DIR) && \
		touch $@ && \
		echo  [done]


%.swig:
	@echo -n "generate wrapper with swig - $@ ..." && \
	swig 	-c++ -csharp -namespace Reflex -dllimport ReflexWrap \ 
			-I$(OMNETPPPATH)/include -I$(REFLEXPATH)/platform/OMNetPP/src/omnetpp/msg \
			-outdir $(GEN_DIR)/reflex_csharp \
			-o $(GEN_DIR)/$(basename $(notdir $@))_wrap.cc \
			-oh $(GEN_DIR)/$(basename $(notdir $@)_wrap.h \
			$@ && \
	echo [done]

$(GEN_DIR):
	@mkdir $@

### main() filter-out OmnetPP can't accept two of them
TMP_SOURCES := $(filter-out %main.cc,$(CC_SOURCES_APPLICATION))
CC_SOURCES_APPLICATION := $(TMP_SOURCES)

TMP_SOURCES := $(filter-out %main.c,$(C_SOURCES_APPLICATION))
C_SOURCES_APPLICATION := $(TMP_SOURCES)



