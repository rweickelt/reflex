/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Soeren Hoeckner
 */

#include "reflex/io/Serial.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptVector.h"

#include "reflex/emulator/NetworkNode.h"
#include "reflex/debug/opp_dbg.h"

#include "SerialMsg_m.h"
#include "reflex/debug/Assert.h"


namespace reflex
{

	Serial::Serial(Baudrate baud):
			InterruptHandler(mcu::interrupts::INTVEC_SERIAL_RX,PowerManageAble::PRIMARY),
			txEndHandler(mcu::interrupts::INTVEC_SERIAL_TX_END,*this,PowerManageAble::SECONDARY)

	{
		this->setSleepMode(mcu::LINUXIDLE);
		txEndHandler.setSleepMode(mcu::LINUXIDLE);
		sender = 0;
		receiver = 0;
		this->rate = baud;
	}

	void Serial::init( Sink0* sender, Sink1<char>* receiver)
	{
		reflex::Queue<Buffer*>::init(this);
		this->receiver = receiver;
		this->sender = sender;
	}

	void Serial::handle()
	{
		opp_dbg.prefix() << "Serial::handleRXI() ";

		//get waiting message
		//SerialMsg *msg = check_and_cast<SerialMsg*> (omnetNode->active_msg);
        //got problems with csharpsimplemodule
        SerialMsg *msg = (SerialMsg*) (omnetNode->active_msg);

		if (receiver && this->isEnabled())
		{
			opp_dbg<< "deliver msg to receiver\n";
			//unpack the message
			unsigned sendCount = msg->getPayloadArraySize();

			for (unsigned i = 0; i < sendCount; i++)
			{
				//throw payload above
				receiver->assign(msg->getPayload(i));
			}
		}
		else
		{
			opp_dbg.prefix() << " no receiver present or Serial disabled\n";
		}

		//remove msg from Omnet env
		omnetNode->cancelAndDelete(msg);
	}

	// Transmission-end simulated Interrupt
	void Serial::handleTXEnd()
	{
		if (sender)
		{
			sender->notify();
		}
		omnetNode->cancelAndDelete(omnetNode->active_msg);
		txEndHandler.switchOff();
		this->unlock();
	}


	void Serial::enable()
	{
		opp_dbg.prefix() << "Serial::enable() \n";
	}

	void Serial::disable()
	{
		opp_dbg.prefix() << "Serial::disable() \n";
	}

	void Serial::run()
	{
		opp_dbg.prefix() << "Serial::run()";

		//get Buffer from queue
		Buffer *current = this->get();
		if (!current)
		{
			opp_dbg<<"get zero from queue\n";
			return;			
		}
		txEndHandler.switchOn();

		opp_dbg<< "going to send Message\n";
		// create Msg-Obj and set kind-value to the InterruptVectorNumber
		SerialMsg *msg = new SerialMsg("serial",mcu::interrupts::INTVEC_SERIAL_RX);

		unsigned buffersize=current->getLength();
		msg->setPayloadArraySize(buffersize);
		//buffer is used as well as a coherend data field
		uint8* data = current->getStart();

		// copy msg data
		for (unsigned i = 0; i < buffersize; i++)
		{
			msg->setPayload(i,(data[i]));
		}

		msg->setLength(buffersize*8); //set bitLength

		//push message below
		omnetNode->sendMsg(msg);

		//free the buffer
		current->downRef();

		//double duration = (( ((double) (buffersize * 8 + 14)) / (this->rate) ))*(double)1000 ; //in millisecond
		//cMessage *msg_tx_end = new cMessage("ser_tx_end",INTVEC_SERIAL_TX_END);
		//EV<< "Duration :"<<duration <<"\n";
		//activeNode->sendSelfMsg((unsigned)duration,msg_tx_end);

		this->lock();
	//}
	//else
	//{
		//opp_dbg<<"Serial(sending) has been disabled\n";
		//current->downRef();
	//}
}

}//end namespace
