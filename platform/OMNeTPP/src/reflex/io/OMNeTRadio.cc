/*
 * OMNeTRadio.cpp
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#include "reflex/io/OMNeTRadio.h"
#include "NetwToMacControlInfo.h"
#include "MacToPhyControlInfo.h"

#include "NodeConfiguration.h"


#ifdef UDPPRINTER
#include "lib/components/UDPPrinter.h"
#endif

OMNeTRadio::OMNeTRadio(bool withDebug)
	: withDebug(withDebug)
{
}

void OMNeTRadio::handleUpperLayerIn_Impl()
{
	reflex::Buffer* buffer = upperLayerIn.get();


	ReflexPacket* reflexPkg = new ReflexPacket("ReflexPacket", 0);

	//write tos
	buffer->push( buffer->getFreeStackSpace() );

#ifdef UDPPRINTER
    /** PRINT TO UDP */
    networkOutput::UDPPrinter* udpPrinter = check_and_cast<networkOutput::UDPPrinter*>( simulation.getModuleByPath("udpPrinter") );
    udpPrinter->printBuffer( reflex::getModule().getParentModule()->getParentModule()->getIndex(), buffer->getStart(), buffer->getLength() );
    /** END PRINT TO UDP */
#endif


	unsigned length= buffer->getLength();
	uint8* data = buffer->getStart();
	reflexPkg->setPayLoadArraySize( length ); //+1 for tos


	// the Mac layer needs the control info from the network layer to process
	// the destination address.
	NetwToMacControlInfo* cInfo = new NetwToMacControlInfo(LAddress::L2BROADCAST); // standard broadcast destination
//	MacToPhyControlInfo* cInfo = new MacToPhyControlInfo();
	reflexPkg->setControlInfo(cInfo);

	for (unsigned i=0; i<length;++i ) {
		reflexPkg->setPayLoad(i,*(data++));
#ifdef DEBUG_PACKETS
        std::cerr << " " << (int) *(data - 1);
#endif
	}
#ifdef DEBUG_PACKETS
    std::cerr << std::endl;
#endif

	buffer->downRef();

	omnetGate.send(reflexPkg);
}

void OMNeTRadio::handleLowerLayerIn_Impl()
{
	 //we assume some cPkg here
	ReflexPacket* reflexPkg = check_and_cast<ReflexPacket*>(reflex::getSystem().get_currentMsg());

	if (upperLayerOut) {
		reflex::Buffer* recBuffer = new (pool) reflex::Buffer(pool);

		if (!recBuffer) {
			omnetGate.module.cancelAndDelete(reflexPkg);
			return;
		}

		recBuffer->initOffsets(reflexPkg->getPayLoad(0));

		for (unsigned i = 1; i < reflexPkg->getPayLoadArraySize(); ++i) {
			recBuffer->write(reflexPkg->getPayLoad(i));
		}

		if (withDebug)
		{
			int nodeId = reflex::getModule().getParentModule()->getParentModule()->getIndex();

			if (nodeId == 0)
			{
	            std::cerr << "-----------------------" << std::endl;

			    std::cerr << "(" << nodeId << ")RADIO: from Lower: " << (int)recBuffer->getLength() << std::endl;
			}
		}

		upperLayerOut->assign(recBuffer);
	}

	omnetGate.module.cancelAndDelete(reflexPkg);

}

void OMNeTRadio::handleLowerControlIn_Impl()
{
	cPacket* controlPacket = check_and_cast<cPacket*>(reflex::getSystem().get_currentMsg());

	controlGate.module.cancelAndDelete(controlPacket);
}

void OMNeTRadio::passMessage(char* buffer, int length)
{
/*   std::cerr << "-----------------------" << std::endl;
    int nodeId = reflex::getModule().getParentModule()->getParentModule()->getIndex();
    std::cerr << "(" << nodeId << ", " << length << ")RADIO: passMessage: ";
    for (unsigned i = 0; i < length; ++i) {
        std::cerr << " " << (int) buffer[ i ];
    }
    std::cerr << std::endl;
*/

    if (upperLayerOut)
    {
        reflex::Buffer* recBuffer = new (pool) reflex::Buffer(pool);

        if (!recBuffer) {
            return;
        }

        recBuffer->initOffsets( (uint8) buffer[ 0 ] );

        for (int i = 1; i < length; ++i) {
            recBuffer->write( buffer[ i ] );
        }



        upperLayerOut->assign(recBuffer);
    }
}

