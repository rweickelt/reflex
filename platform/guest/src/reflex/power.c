/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 
 *	Author:		Karsten Walther
 */
#include <unistd.h>
#include <stdio.h>

extern void _interruptsEnable();

/** This function is used for power management an brings the system to wait
 *  mode, since this is the guest environment it does nothing except
 *  enabling the interrupts, since this is expected by the scheduler since
 *  microcontrollers resume from wait state only with enabled interrupts.
 */
void _wait(){
	_interruptsEnable();
	usleep(100 * 1000 * 1000); //busy waiting
}

void _yield(){
	_interruptsEnable();
	pause(); // return to os yield
}
