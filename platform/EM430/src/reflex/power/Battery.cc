/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/power/Battery.h"
//#include "reflex/data_types/BCD.h"
//#include "reflex/data_types/Encode.h"

using namespace reflex;

Battery::Battery()
	: adcOut(0)
{
	input.init(this);
}

void Battery::run()
{
	ADC::Request request(ADC::Ref_Vref_AVss,ADC::InCh_AVccDIV,ADC::CH1);
	if(adcOut) adcOut->assign(request);
}


//subclass Calculator

Battery::Calculator::Calculator()
	: SingleValue1<uint16>(this)
	, valueOut(0)
{}

void Battery::Calculator::run()
{
	// Convert ADC value to "x.xx V"
	// Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
	// --> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
	uint16 voltage = (this->get()<<2)/41; // (adc * 4 / 4095) * 100
	if(valueOut) valueOut->assign(voltage);
}

