/*
 *      REFLEX - Real-time Event FLow EXecutive
 *
 *      A lightweight operating system for deeply embedded systems.
 *
 */

#include "reflex/io/LED.h"

using namespace reflex;

LED::LED()
{
  msp430x::Port1()->SEL &= ~GREEN;
  msp430x::Port1()->DIR |= GREEN;
  msp430x::Port1()->OUT |= GREEN;

  msp430x::Port3()->SEL &= ~RED;
  msp430x::Port3()->DIR |= RED;
  msp430x::Port3()->OUT |= RED;
}

void LED::setOffRed()
{
  msp430x::Port3()->OUT &= ~RED;
}

void LED::setOffGreen()
{
  msp430x::Port1()->OUT &= ~GREEN;
}

void LED::toggleRed()
{
  msp430x::Port3()->OUT ^= RED;
}

void LED::toggleGreen()
{
  msp430x::Port1()->OUT ^= GREEN;
}

void LED::toggleLeds()
{
  toggleGreen();
  toggleRed();
}
