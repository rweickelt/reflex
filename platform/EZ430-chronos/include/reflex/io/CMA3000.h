/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_CMA3000_H
#define REFLEX_EZ430_CHRONOS_CMA3000_H

#include "reflex/io/Ports.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/sinks/Sink.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/usci/USCI_SPI.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ProtoThread.h"
#include "reflex/scheduling/ProtoThreadTimer.h"

#include "reflex/driverConfiguration/ConfigurableDriver.h"
#include "reflex/driverConfiguration/AccelerationSensorConfiguration.h"

namespace reflex {

/**
 * contains values in milli-g
 */
const int16 MGRAV_PER_BIT_2G[7] = { 18, 36, 71, 143, 286, 571, 1142};

/**
 * contains values in milli-g
 */
const int16 MGRAV_PER_BIT_8G[7] = { 71, 143, 286, 571, 1142, 2286, 4571}; //mg

/**
 * contains values in ms for 400 Hz
 */
const uint16 THRESHOLD_TIME_400HZ[4] = { 2, 5, 10, 20};//ignore 2,5 ms -> take 2ms instead

/**
 * contains values in ms for 100 Hz
 */
const uint16 THRESHOLD_TIME_100HZ[4] = { 10, 20, 40, 80};

/**
 * contains values in ms for 10 Hz => only for motion detection
 */
const uint16 THRESHOLD_TIME_10HZ[3] = { 100, 200, 400};



/**
 * Driver for the CMA3000 3-axis acceleration sensor.
 * Uses USCI_A0 in SPI mode for direct spi communication
 */
class CMA3000 : private reflex::ProtoThread
				, private reflex::ProtoThreadTimer
				, private reflex::Sink0
				, public PowerManageAble
				, public reflex::Activity
				, public reflex::ConfigurableDriver<AccelerationSensorConfiguration>
{

public:

	/*
	 * operation modes, specified with setMode
	 */
	enum mode {
		  MEASURE_100 = 0x02
		, MEASURE_400 = 0x04
		, MEASURE_40 = 0x06
		, MOTION_DETECTION = 0x08
		, FREE_FALL_DETECTION_100 = 0x0A
		, FREE_FALL_DETECTION_400 = 0x0C
		, POWERDOWN = 0x0
	};

	/*
	 * resolution, specified with setMode
	 */
	enum resolution {
			 _2G = 0x80
			,_8G = 0x00
		};

private:
	enum registers {
		 WHO_AM_I = 0x00
		,REVID = 0x01
		,CTRL = 0x02
		,STATUS = 0x03
		,RSTR = 0x04
		,INT_STATUS = 0x05
		,DOUTX = 0x06
		,DOUTY = 0x07
		,DOUTZ = 0x08
		,MDTHR = 0x09
		,MDFFTMR = 0x0A
		,FFTHR = 0x0B
		,I2C_ADDR = 0x0C

	};

	enum bits {
		 WRITE_FLAG = 0x02
		//ctrl
		,G_RANGE = 0x80
		,INT_LEVEL = 0x40
		,MDET_EXIT = 0x20
		,I2C_DIS = 0x10
		,MODE_PD = 0x00
		,MODE_M100 = 0x02
		,MODE_M400 = 0x04
		,MODE_M40 = 0x06
		,MODE_MD = 0x08
		,MODE_FFD100 = 0x0A
		,MODE_FFD400 = 0x0C
		,INT_DIS = 0x01
		//reset
		,RESET = 0x02
		//int_status
		,FFDET  = 0x4
		,MDET_X = 0x01
		,MDET_Y = 0x02
		,MDET_Z = 0x03
		,MEDT_NO = 0x00
	};



	#define BIT(x) (1uL << (x))

public:

	/**
	 * default values:
	 *
	 * cfg.clear();
	 * Mode: MEASUREMENT)
	 * Resolution: 80
	 * SamplingRate: 400
	 * InterruptDriven: true
	*/
	CMA3000();

	~CMA3000() {}


	/**
	 * init
	 * connect the interrupt signal and data output of the sensor
	 *
	 * @param interruptOut interrupt signal
	 * @param dataOut x,y,z value output
	 */
	void init(Sink0* interruptOut, Sink3<int16, int16, int16>* dataOut) __attribute__((deprecated));


	/**
	 * connects the interrupt output (motion-, freefalldetction etc.) with an input
	 * @param sink input of a data sink
	 */
	inline void connect_out_interruptOut(Sink0* sink)
	{
		this->interruptOut = sink;
	}


	/**
	 * connects the data output with an input
	 * @param sink input of a data sink
	 */
	inline void connect_out_dataOut(Sink3<int16, int16, int16>* sink)
	{
		this->dataOut = sink;
	}

	/**
	 * @return the event which has to be triggered if data will be read
	 */
	inline reflex::Event* get_in_dataRequest()
	{
		return &sample;
	}


	/**
	 * endpoint of the interrupt dispatcher and handle method for
	 * interrupts from the sensor
	 */
	virtual void notify();


	/**
	 * derived from configurable driver
	 */
	virtual void configure();

	/**
	 * activity for readout of sensor values
	 */
	virtual void run();

	/**
	 * writes a CMA3000 register using spi
	 *
	 * @param address
	 * @param data
	 * @return status byte
	 */
	uint8 writeRegister(registers address, uint8 data);

	/**
	 * reads CMA3000 register using spi
	 *
	 * @param address
	 * @return
	 */
	uint8 readRegister(registers address);




protected:
	/**
	 * enabled the interrupt of the sensor
	 */
	virtual void enable();
	/**
	 * disables the interrupt of the sensor
	 */
	virtual void disable();


	/**
	 * convert raw register values to mgrav
	 *
	 * @param value
	 * @param resolution
	 * @return
	 */
	int16 convert_acceleration_value_to_mgrav(uint8 value, resolution res_val);

	/*
	 * internal pointer to the receiver of the interrupt signal
	 */
	Sink0* interruptOut;

	/**
	 * internal pointer to the data sink
	 */
	Sink3<int16,int16,int16>* dataOut;



	/**
	 * where the interrupts of port2 are delegated to
	 */
	data_types::Singleton< mcu::Port2::IVDispatcher > port2IV;

	/**
     * spi object
	 */
    msp430x::USCI_SPI<msp430x::usci::RegistersA0> spi;

private:

	/**
	 * converts any threshold to the approximated value for the driver
	 * @param threshold any threshold is converted to a value, that the driver can handle
	 * @param res the current resolution
	 * @param workingMode the current working mode -> needed to perform the correct conversion
	 */
	uint8 threshold_To_THR(int16 threshold, resolution res, mode workingMode);

	/**
	 * converts any threshold time to the approximated time for the driver
	 * @param time any time is converted to a value, that the driver can handle
	 * @param sample the current sampling (10, 40, 100, 400Hz are supported)
	 */
	uint8 thresholdTime_To_MDFFTMR(uint16 time, mode sample);

	/**
	 * internal method for configuring the sensor, needed this way because
	 * of the f*$%ing protothreads
	*/
	void setMode_internal();

	/**
	 *  This is the Activity Functor
	 *  Pointing to a class and to a member function of
	 *  this class
	 */
	ActivityFunctor<CMA3000, &CMA3000::setMode_internal> initFunctor;

	/**
	 * read out the three axis registers and write it to the uint16 array
	 *
	 * @param data
	 */
	void getData(int16* data);


	//////////////////////////////////////
	// members to handle the protothread
	//////////////////////////////////////

	/*
	 * sampling rate
	 */
	uint16 sRate;

	/**
	 * current mode of sensor operation
	 */
	mode currentMode;

	/**
	 * current resolution (2g or 8g)
	 */
	resolution res;

	/**
	 * shows if interrupts are enabled
	 */
	bool interruptDriven;

	/**
	 * configuration data instance
	 */
	AccelerationSensorConfiguration cfg;
	
		/**
	 * TODO: must be private
	 * triggers the activity
	 */
	Event sample;
	

};


}// ns reflex


#endif // CMA3000_H
