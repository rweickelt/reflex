#ifndef BasicClockModuleConfiguration_h
#define BasicClockModuleConfiguration_h

namespace reflex {

namespace basicClockModule {

enum Definitions {
	CLOCK_FREQUENCY = 8000000,
	EXT1_FREQUENCY = 32768,
	DCOR_VALUE = 0x1
};

} //basicClockModule

enum SerialSettings {
    UMCTL_B300 = 0x6d,
    UMCTL_B600 = 0x4c,
    UMCTL_B1200 = 0x5b,
    UMCTL_B2400 = 0x52,
    UMCTL_B4800 = 0x5b,
    UMCTL_B9600 = 0x09,
    UMCTL_B19200 = 0x5b,
    UMCTL_B31250 = 0x00,
    UMCTL_B38400 = 0x11,
    UMCTL_B56000 = 0xf7,
    UMCTL_B57600 = 0xef,
    UMCTL_B115200 = 0xaa,
    UMCTL_B128000 = 0x55,
    UMCTL_B230400 = 0xdd,
    UMCTL_B460800 = 0x52,
    UMCTL_B921600 = 0x5b
};



} //reflex

#endif

