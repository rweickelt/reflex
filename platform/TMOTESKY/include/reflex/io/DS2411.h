#ifndef ds2411_h
#define ds2411_h

namespace reflex {

/**
 * This class implements the driver for the 1-Wire DS2411 device.
 * CRC is not supported.
 * @author Jonas Hartwig, Christian Hildebrand
 */

class DS2411 {
public:
	/**
	 * constructor - does nothing
	 */
	DS2411() {}

	/**
	 * Set up hardware.
	 */
	void init();

	/**
	 * Reads the ID from chip.
	 */
	void fetch();
	
protected:	
    /**
	 * Waits given time.
	 * @param us time in us.
	 */
	unsigned int wait(unsigned int us);	
	
	/**
	 * Write one byte to One Wire Bus.
	 * @param b byte to write
	 */
	void writeByte(unsigned char b);
	
	/**
	 * Reads one byte from 1-Wire-Bus.
	 * @return byte read
	 */
	unsigned char readByte();
	
	/**
	 * Send a reset pulse.
	 */
	void sendResetPulse();

	/**	
	 * Constants taken from maxim (recommended)
	 */
	enum {
		tB=64,
		tC=60,
		tD=10,
		tE=9,
		tF=55,
		tG=0,
		tH=480,
		tI=70,
		tJ=410
	} OneWireMaximConstants;
	
	/**
	 * Other Constants
	 */
	enum {
		SETZERO=0x00,
		PIN2_4=0x10
	} DriverConstants;

public:	

	/**
	 * size of ID stored in DS24211
	 */
	enum {
		IDSIZE=8
	} PublicDriverConstants;

	/**
	 * ID buffer filled by fetch()
	 */
	unsigned char id[IDSIZE];
};

}// end namespace

#endif
