/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/System.h" //because of getSystem()
#include "reflex/io/TMoteRadio.h"
#include "reflex/io/IEEE802MAC.h"
#include "reflex/memory/SizedFifoBuffer.h"
#include "reflex/memory/Pool.h"
//test
#include "reflex/io/Led.h"

#include "NodeConfiguration.h"
extern NodeConfiguration system;

using namespace reflex;

IEEE802MAC::IEEE802MAC(Queue<Buffer*>* radioQueue) :
input(this),
recMsgQue(this)
{
	this->radioQueue = radioQueue;
	recLength = 0;
	recBuff = 0;
	myPanId = 0xffff;
	my16BitId = 0xffff;
	waitingAssoc = false;
	// first run to get an id
	//trigger();
	// sendAssocRequ(myPanId);
}

void IEEE802MAC::init(Sink1<Buffer*>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
}

void IEEE802MAC::assign(char cRec)
{
	// the first sign is the length
	if ( recLength == 0 ) {
		recLength = cRec; // length of the next Msg. everything else done
		if ( recBuff ) {// that should not be ! -> delete
			recBuff->downRef();
			// recBuff = 0; not necessary
		}
		recBuff = new (&pool) Buffer(); // get a new Buffer to copy data there
		// recBuff->upRef(); /
		return;
	}
	
	recBuff->write(cRec);
	
	recLength--;
	// add into to proceed queue
	if (recLength == 0) { // all received
		recMsgQue.assign(recBuff);
		
		recBuff->upRef();
		system.serial.input.assign(recBuff);
		recBuff=0;
	}
}

void IEEE802MAC::sendAssocRequ(uint16 panID)
{
	waitingAssoc = true;
	
	current = new (&pool) SizedFifoBuffer<IOBufferSize>(); // get a new Buffer to copy data there
	if ( current == 0 ) {
		return;
	}
	
	uint16 fcf = MAC_COMMAND | INTRA_PAN | SOURCE_64Bit;
	current->write(((char*)&fcf)[1]);
	current->write(((char*)&fcf)[0]);
	
	sequenzNr++;// = nextToSend->getLength(); // not used ..  until now, just pseudo
	current->write((char)sequenzNr); // write the FCF ( 2 Bytes) 
	

	uint16 destPANID = 0x7e37;//0xffff; // myPanId
	current->write(((char*)&destPANID)[1]); 
	current->write(((char*)&destPANID)[0]); 
	
	uint16 destADR = 0xffff;
	current->write(((char*)&destADR)[1]); // 
	current->write(((char*)&destADR)[0]); // 

	uint16 sorcePANID = 0xffff; // myPanId
	current->write(((char*)&sorcePANID)[1]); 
	current->write(((char*)&sorcePANID)[0]); 
	
	uint64 sourceADR = NODEID; // not used now
	current->write(((char*)&sourceADR)[7]); // 
	current->write(((char*)&sourceADR)[6]); // 
	current->write(((char*)&sourceADR)[5]); // 
	current->write(((char*)&sourceADR)[4]); // 
	current->write(((char*)&sourceADR)[3]); // 
	current->write(((char*)&sourceADR)[2]); // 
	current->write(((char*)&sourceADR)[1]); // 
	current->write(((char*)&sourceADR)[0]); //*/
	
	/*uint16 PanId = 0xffff; // not used, if intraPAN
	current->write(((char*)&PanId)[1]);  
	current->write(((char*)&PanId)[0]); 
	*/
	
	// MAC COMMAND DATA 
	current->write((char)ASSOCIATION_REQUEST_CMD); 
	// CHECK THIS !!!! :::
	char capInfoField = 0x88; // rec on when idle,alloc adress
	current->write(capInfoField);
	
	radioQueue->assign(current);
	
	
}

void IEEE802MAC::run()
{
	// lock();
	/*if ( my16BitId == 0xffff ) {// not initialised
		if ( !waitingAssoc ) {
			sendAssocRequ(myPanId);
		} else {
			trigger(); // just wait and trigger again
		}
		// wait it first on it
		
		return;
	}*/
	
	// first test, if i have something received.. then first proceed with this
	Buffer* received = recMsgQue.get();
	if ( received != 0 ) { // something received... check what kind of msg
		processMsg(received);
		received->downRef(); // delete, maybe all done
		return;
	}
	
	//current = input.get();
	Buffer* nextToSend = input.get();
	if ( nextToSend == 0) {
		return;
	}
	
	// get new Buffer, insert header and the copy the data
	current = new (&pool) SizedFifoBuffer<IOBufferSize>(); // get a new Buffer to copy data there
	if ( current == 0 ) {
		return;
	}
	
	
	// this->create_header(Frame_Type_Command, true, pan_id, dest_addr, 0xFFFF, my_src_addr);
	//uint16 fcf = DATA |INTRA_PAN | SOURCE_16Bit | DEST_16Bit; // no source field given
	uint16 fcf = MAC_COMMAND | DEST_16Bit;
	current->write(((char*)&fcf)[1]);
	current->write(((char*)&fcf)[0]);
	
	sequenzNr++;// = nextToSend->getLength(); // not used ..  until now, just pseudo
	current->write((char)sequenzNr); // write the FCF ( 2 Bytes) 
	
	uint16 destPANID = 0xffff;
	current->write(((char*)&destPANID)[1]); 
	current->write(((char*)&destPANID)[0]); 
	
	uint16 destADR = 0xffff;
	current->write(((char*)&destADR)[1]); // 
	current->write(((char*)&destADR)[0]); // 
	
	
	/*sourcePANID = 0x0000; // not used, if intraPAN
	current->write(((char*)&sourcePANID)[1]);  
	current->write(((char*)&sourcePANID)[0]); 
	*/
	
	/*uint64 sourceADR = NODEID; // not used now
	current->write(((char*)&sourceADR)[7]); // 
	current->write(((char*)&sourceADR)[6]); // 
	current->write(((char*)&sourceADR)[5]); // 
	current->write(((char*)&sourceADR)[4]); // 
	current->write(((char*)&sourceADR)[3]); // 
	current->write(((char*)&sourceADR)[2]); // 
	current->write(((char*)&sourceADR)[1]); // 
	current->write(((char*)&sourceADR)[0]); //*/
	/*current->write((char)0x00);
	current->write((char)0x00);
	current->write((char)0x74);
	current->write((char)0x39);
	current->write((char)0x02);
	current->write((char)0x05);
	current->write((char)0x01);
	current->write((char)0x44);
	*/
	/*sourcePANID = myPanId; // not used, if intraPAN
	current->write(((char*)&sourcePANID)[1]);  
	current->write(((char*)&sourcePANID)[0]); 
	*/
	// dummy?
	
	//current->write(0x44);
	// current->write(((char*)&myPanId)[1]);  
	// current->write(((char*)&myPanId)[0]); 
	
	// MAC COMMAND DATA 
	
	current->write((char)7);
	
	// insert the length of the frame.
	uint8* msg = nextToSend->getStart();
	for (int i = 0; i < nextToSend->getLength(); i++) {
		// current->write(*(msg++));
	}
	
	nextToSend->downRef(); // not used anymore
	
	radioQueue->assign(current);
}

void IEEE802MAC::processMsg(Buffer* msg)
{
	
	uint8 length = msg->getLength();
	uint8* buff = msg->getStart();
	
	uint16 frameContr;
	((char*)&frameContr)[0] = buff[FRAME_CONTROL+1]; // pos 1
	
	((char*)&frameContr)[1] = buff[FRAME_CONTROL]; // pos 1
	
	if ( (frameContr & FRAME_TYPE) == ACK) {
		//do nothing
		return;
	}
	
	/*if ( ! ( frameContr & INTRA_PAN ) ) { // not intrapan -> i do nothing
		return;
	}*/
	
	// datapos
	uint8 dataPos = 3; // first possible databyte... add the present bytes from dest and source adr
	if ( (frameContr & DEST_16Bit) ) { // DEST ADR there
		frameContr &= ~DEST_16Bit;
		dataPos += 2;
		if ( (frameContr & DEST_64Bit) ) { 
			dataPos += 6; // only 6 ! -> one is allready
		}
		// also destpan-id 
		dataPos += 2;
	}
	
	if ( (frameContr & SOURCE_16Bit) ) { // source adresss there
		frameContr &= ~SOURCE_16Bit;
		dataPos += 2;
		if ( (frameContr & SOURCE_64Bit) ) { 
			dataPos += 6; // only 3 ! -> one is allready
		}
		if ( ! ( frameContr & INTRA_PAN ) ) { // not intrapan -> PAN ID present
			dataPos += 2;
		}
	}
	// send data to other stage...
	if ( (frameContr & FRAME_TYPE) == DATA) { 
		
		SizedFifoBuffer<IOBufferSize> *recMsg = new (&pool) SizedFifoBuffer<IOBufferSize>(); // get a new Buffer to copy data there
		if ( recMsg != 0 ) { // copy data into it
			// length of data: length - datapos - 2 (-2 because of FCS)
			for ( uint8 i = dataPos; i < length - 2; i++ ) {
				recMsg->write(buff[i]);
			}
			if ( receiver) {
				receiver->assign(recMsg);
			} else {
				recMsg->downRef(); // not used... :-( not usefull
			}
		}
		return;
	}
	
	if ( (frameContr & FRAME_TYPE) == MAC_COMMAND) {
		uint8 macCom = buff[dataPos];
		if ( macCom == ASSOCIATION_RESPONSE_CMD ) {
			// remember id
			((char*)&my16BitId)[0] = buff[dataPos+2]; // first 1
			((char*)&my16BitId)[1] = buff[dataPos+1]; // second
			waitingAssoc = false; // now i've got an id
		}
		// all other ignored
	}
}

void IEEE802MAC::notify() // notify from radio -> last packet sent
{
	//current->downRef();
	current = 0;
	//unlock();
}
