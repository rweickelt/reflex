include $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk

CC_SOURCES_APPLICATION += \
	src/TestEnvironment.cc \
	src/TestPlayer.cc \
	src/TestSuite.cc	

CC_SOURCES_LIB += \
	io/OutputChannel.cc \
	
CC_SOURCES_LIB += \
	memory/Buffer.cc \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/PoolManager.cc 