#ifndef Configuration_h
#define Configuration_h

enum
{
	IOBufferSize = 62, // used for OutputChannel
	NrOfStdOutBuffers = 32, // amount of buffers in OutputChannel
	MaxPoolCount = 1
};

enum RunMode
{
	AWAKE = 0x01, SLEEP = 0x02, SECONDARY = 0x04
};

#endif
