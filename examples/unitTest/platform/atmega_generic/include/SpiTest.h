#ifndef SPITEST_H_
#define SPITEST_H_

#include "reflex/io/SpiHardware.h"
#include "TestSuite.h"

namespace unitTest
{
/**
 \brief Test cases for the Spi interface
 */
class SpiTest: public TestSuite
{
public:
	enum TestCases
	{
		RegisterTest = 0, LoopbackTest, NrOfTestCases
	};

	enum
	{
		DataSet0 = 0x55, DataSet1 = 0xAA
	};

	SpiTest(uint8 id);
	~SpiTest();

private:
	reflex::mcu::SpiHardware<> spi;

};

}

#endif /* SPITEST_H_ */
