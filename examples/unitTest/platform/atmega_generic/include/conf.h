#ifndef CONF_H_
#define CONF_H_

enum
{
	MaxPoolCount = 1
};

#define VIRTUAL_TIMERS 2
// speed of system clock (0 for no clock/dynamic ticks)
#define TICK_PERIOD_MSEC 0

#endif

