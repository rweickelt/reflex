#ifndef TESTPORT_H_
#define TESTPORT_H_

#include "reflex/io/IOPort.h"

#include "TestSuite.h"

namespace unitTest
{

typedef reflex::atmega::Core::IOPorts IOPorts;
typedef reflex::atmega::PortPins PortPins;
typedef reflex::atmega::PortPin PortPin;

/**
 \brief Tests control of output registers.

 For this test, Port1 must be connected to Port2. Alternating bitmasks are then written to Port1 which
 have to be read successfully by Port2.

 */
template<IOPorts Port1, IOPorts Port2, uint8 pins>
class PortTest: public TestSuite
{
public:
	enum TestCases
	{
		Registers = 0, Port1ToPort2, NrOfTestCases
	};

	PortTest(uint8 id);
	~PortTest();
private:
};

template<IOPorts Port1, IOPorts Port2, uint8 pins>
PortTest<Port1, Port2, pins>::PortTest(uint8 id) :
	TestSuite(id)
{
	using namespace reflex;
	using namespace atmega;

	switch (currentTestCase())
	{
	case Registers:
		IOPort<Port1>()->DDR |= pins;
		VERIFY(((IOPort<Port1>()->DDR & pins) == pins), READ_FLASH("Port1->DDR expected 0x%X but read 0x%X"), pins, IOPort<Port1>()->PIN & pins);
		IOPort<Port1>()->DDR &= ~pins;
		IOPort<Port2>()->DDR |= pins;
		VERIFY(((IOPort<Port2>()->DDR & pins) == pins), READ_FLASH("Port1->DDR expected 0x%X but read 0x%X"), pins, IOPort<Port2>()->PIN & pins);
		IOPort<Port2>()->DDR &= ~pins;
		break;
	case Port1ToPort2:
		IOPort<Port1>()->DDR |= pins;
		IOPort<Port2>()->DDR &= ~pins; // configure Port2 as input port
		IOPort<Port1>()->PORT |= pins; // write a bitmask to Port1
		VERIFY(((IOPort<Port2>()->PIN & pins) == pins), READ_FLASH("Port1->PIN expected 0x%X but read 0x%X"), pins, IOPort<Port2>()->PIN & pins);
		setResultOk();
		break;
	default:
		setResultSkipped();
		break;
	}
}

template<IOPorts Port1, IOPorts Port2, uint8 pins>
PortTest<Port1, Port2, pins>::~PortTest()
{
	using namespace reflex;
	using namespace atmega;
	IOPort<Port1>()->PORT &= ~pins;
	IOPort<Port1>()->DDR &= ~pins;
	IOPort<Port2>()->PORT &= ~pins;
	IOPort<Port2>()->DDR &= ~pins;
}

}

#endif /* TESTPORT_H_ */
