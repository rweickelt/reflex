#ifndef PINCHANGEINTERRUPTTEST_H_
#define PINCHANGEINTERRUPTTEST_H_

#include "TestSuite.h"
#include "reflex/io/PinChangeInterrupt.h"
#include "reflex/io/IOPort.h"
#include "reflex/memory/Flash.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{

typedef reflex::atmega::Core::IOPorts IOPorts;
typedef reflex::atmega::PortPins PortPins;
typedef reflex::atmega::PortPin PortPin;
typedef reflex::atmega::PinChangeInterrupt::PinGroup PinGroup;


template<IOPorts port>
class PinChangeInterruptTestBase: public TestSuite
{
public:
	enum TestCases
	{
		Registers = 0, //!< Test register configuration
		SinglePinChange, //!< Simulate a falling and rising edge on the test-pin
		IgnoreOtherPins, //!< Toggle all other pins which should not create an event
		NrOfTestsPerPin
	};

	PinChangeInterruptTestBase(uint8 id, PinGroup pinGroup, PortPins pins);
	~PinChangeInterruptTestBase();

private:
	void run_event();
	void handleTimeout();

	PinGroup pinGroup;
	PortPins pins;
	reflex::atmega::PinChangeInterrupt interrupt;
	reflex::Event input;
	reflex::ActivityFunctor<PinChangeInterruptTestBase, &PinChangeInterruptTestBase::run_event> act;
	uint8 interruptCounter;

	typedef reflex::atmega::IOPort<port> TestPort;

};

template<IOPorts port>
PinChangeInterruptTestBase<port>::PinChangeInterruptTestBase(uint8 id, PinGroup pinGroup, PortPins pins) :
	TestSuite(id), act(*this)
{
	this->pinGroup = pinGroup;
	this->pins = pins;
	input.init(&act);
	interrupt.out_event = &input;
	interruptCounter = 0;

	interrupt.setPinGroup(pinGroup);

	switch (currentTestCase())
	{
	case Registers:
		VERIFY(interrupt.pinGroup() == pinGroup, READ_FLASH("setPinGroup()"))
		interrupt.setPinsEnabled(pins);
		VERIFY(interrupt.enabledPins() == pins, READ_FLASH("enable pins"))
		interrupt.setPinsDisabled(pins);
		VERIFY(interrupt.enabledPins() == static_cast<uint8>(~reflex::mcu::AllPins), READ_FLASH("disable pins"))
		setResultOk();
		break;
	case SinglePinChange:
		TestPort()->DDR |= pins;
		TestPort()->PORT = 0;
		interrupt.setPinsEnabled(pins);
		TestPort()->PORT |= pins;
		TestPort()->PORT |= pins;
		TestPort()->PORT |= pins;
		TestPort()->PORT &= ~pins; // 1. edge
		TestPort()->PORT &= ~pins; // 1. edge
		TestPort()->PORT &= ~pins; // 1. edge
		setTimeout(20);
		break;
	case IgnoreOtherPins:
		TestPort()->DDR |= reflex::mcu::AllPins;
		TestPort()->PORT = 0;
		interrupt.setPinsEnabled(pins);
		TestPort()->PORT |= ~pins; // should not create an event
		TestPort()->PORT |= ~pins; // should not create an event
		TestPort()->PORT &= pins;
		TestPort()->PORT &= pins;
		setTimeout(20);
		break;
	default:
		SKIP();
	}
}

template<IOPorts port>
void PinChangeInterruptTestBase<port>::run_event()
{
	interruptCounter++;
}

template<IOPorts port>
void PinChangeInterruptTestBase<port>::handleTimeout()
{
	uint8 currentTest = currentTestCase() % NrOfTestsPerPin;

	if ((currentTest == SinglePinChange) && (interruptCounter > 0))
	{
		setResultOk();
	}
	else if ((currentTest == IgnoreOtherPins) && (interruptCounter == 0))
	{
		setResultOk();
	}
	else
	{
		TestSuite::handleTimeout();
	}
}

template<IOPorts port>
PinChangeInterruptTestBase<port>::~PinChangeInterruptTestBase()
{
	interrupt.setPinsDisabled(reflex::mcu::AllPins);
	TestPort()->DDR = 0;
	TestPort()->PORT = 0;
}

template<reflex::atmega::PinChangeInterrupt::PinGroup pinGroup, reflex::atmega::Core::IOPorts port, reflex::atmega::PortPin pin>
class PinChangeInterruptTest : public PinChangeInterruptTestBase<port>
{
public:
	inline PinChangeInterruptTest(uint8 id) : PinChangeInterruptTestBase<port>(id, pinGroup, pin) { }

};


}

#endif /* PINCHANGEINTERRUPTTEST_H_ */
