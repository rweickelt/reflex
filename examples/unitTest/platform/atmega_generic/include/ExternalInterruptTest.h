#ifndef EXTERNALINTERRUPTTEST_H_
#define EXTERNALINTERRUPTTEST_H_

#include "TestSuite.h"
#include "reflex/io/ExternalInterrupt.h"
#include "reflex/io/IOPort.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{
/*!
 \brief Unit test for the ExternalInterrupt driver of the ATMEGA.

 This test assumes

 */
class ExternalInterruptTest: public TestSuite
{
public:
	enum TestCases
	{
		Registers = 0, FallingEdge, RisingEdge, AnyEdge, LowLevel, NrOfTestCases
	};

	ExternalInterruptTest(uint8 id);
	~ExternalInterruptTest();

private:
	void run_event();
	void handleTimeout();

	reflex::atmega::ExternalInterrupt interrupt;
	reflex::Event input;
	reflex::ActivityFunctor<ExternalInterruptTest, &ExternalInterruptTest::run_event> act;

	typedef reflex::atmega::IOPort<reflex::atmega::Core::PortD> InterruptPort;

	enum
	{
		InterruptPin = reflex::mcu::Pin0
	};

	uint8 counts;

};

}

#endif /* EXTERNALINTERRUPTTEST_H_ */
