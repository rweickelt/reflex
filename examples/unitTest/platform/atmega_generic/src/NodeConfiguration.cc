#include "NodeConfiguration.h"
#include "InitialTest.h"
#include "ExternalInterruptTest.h"
#include "PinChangeInterruptTest.h"
#include "reflex/memory/Flash.h"

using namespace reflex;
using namespace mcu;
using namespace unitTest;


typedef PinChangeInterruptTest<PinChangeInterrupt::PinGroup0, Core::PortB, Pin0> PCInt0;

// Selftest for atmega2560 on the STK600
template<>
const TestListEntry NodeConfiguration<Atmega2560>::testList[] IN_FLASH =
{
{ "InitialTest", 				0, 							&TestSuite::create<InitialTest> },
{ "SpiRegisters", 				SpiTest::RegisterTest, 		&TestSuite::create<SpiTest> },
{ "SpiLoopback", 				SpiTest::LoopbackTest,		&TestSuite::create<SpiTest> },
{ "PortTestABRegisters", 		0 ,							&TestSuite::create<PortTest<Core::PortA, Core::PortB, AllPins> > },
{ "PortTestAB0x01Port1ToPort2", 1 , 						&TestSuite::create<PortTest<Core::PortA, Core::PortB, 0x01> > },
{ "PCInt0Registers", 			PCInt0::Registers, 			&TestSuite::create<PCInt0>  },
{ "PCInt0Change", 				PCInt0::SinglePinChange,	&TestSuite::create<PCInt0>  }
};


// Selftest for atmega644 / atmega1284 on the pollin board http://www.pollin.de/shop/downloads/D810038B.PDF
template<>
const TestListEntry NodeConfiguration<Atmega644>::testList[] IN_FLASH =
{
{ "InitialTest", 				0, 							&TestSuite::create<InitialTest> },
{ "SpiRegisters", 				SpiTest::RegisterTest, 		&TestSuite::create<SpiTest> },
{ "SpiLoopback", 				SpiTest::LoopbackTest,		&TestSuite::create<SpiTest> },
{ "PortTestABRegisters", 		0 ,							&TestSuite::create<PortTest<Core::PortA, Core::PortB, AllPins> > },
{ "PortTestAB0x01Port1ToPort2", 1 , 						&TestSuite::create<PortTest<Core::PortA, Core::PortB, 0x01> > },
{ "PCInt0Registers", 			PCInt0::Registers, 			&TestSuite::create<PCInt0>  },
{ "PCInt0Change", 				PCInt0::SinglePinChange,	&TestSuite::create<PCInt0>  }
};

/* Initialization of NodeConfiguration */
namespace reflex
{
template<>
NodeConfiguration<CurrentMcu>::NodeConfiguration() :
	player(&testList[0], sizeof(testList) / sizeof(TestListEntry))
{
	testEnvironment.getInstance().setOut_message(serial.get_in_tx());
	serial.setBaudRate(115200, 16000000);
	serial.rxHandler().setGroups(AWAKE);
	powerManager.enableGroup(AWAKE);

	testEnvironment.getInstance().setOut_message(serial.get_in_tx());
	player.out_testCase = &testEnvironment->in_testCase;
	serial.set_out_txFinished(&player.in_txFinished);
}
}
