#include "ExternalInterruptTest.h"

using namespace reflex;
using namespace atmega;
using namespace unitTest;

ExternalInterruptTest::ExternalInterruptTest(uint8 id) :
	TestSuite(id), act(*this)
{
	input.init(&act);
	interrupt.out_event = &input;
	counts = 0;

	if (currentTestCase() >= NrOfTestCases)
	{
		setResultSkipped();
		return;
	}

	ExternalInterrupt::Pin currentPin = ExternalInterrupt::INT0;
	setTimeout(500);
	switch (currentTestCase())
	{
	case Registers:
		interrupt.setPin(currentPin);
		VERIFY(interrupt.pin() == currentPin, "setPin()");
		interrupt.setEvent(ExternalInterrupt::AnyEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::AnyEdge, "AnyEdge")
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::FallingEdge, "FallingEdge")
		interrupt.setEvent(ExternalInterrupt::RisingEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::RisingEdge, "RisingEdge")
		interrupt.setEvent(ExternalInterrupt::LowLevel);
		VERIFY(interrupt.event() == ExternalInterrupt::LowLevel, "LowLevel")
		VERIFY(interrupt.pin() == currentPin, "setPin() after levels")
		setResultOk();
		break;
	case RisingEdge:
		interrupt.setPin(currentPin);
		interrupt.setEvent(ExternalInterrupt::RisingEdge);
		InterruptPort()->DDR |= InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		//interrupt.switchOn();
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		break;
	case FallingEdge:
		interrupt.setPin(currentPin);
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		InterruptPort()->DDR |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		interrupt.switchOn();
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		break;
	case AnyEdge:
		interrupt.setPin(currentPin);
		interrupt.setEvent(ExternalInterrupt::AnyEdge);
		InterruptPort()->DDR |= InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		interrupt.switchOn();
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		break;
	case LowLevel:
		interrupt.setPin(currentPin);
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		InterruptPort()->DDR |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		interrupt.switchOn();
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT &= ~InterruptPin;
		InterruptPort()->PORT |= InterruptPin;
		break;
	default:
		SKIP()
		;
		break;
	}
}

ExternalInterruptTest::~ExternalInterruptTest()
{
	interrupt.switchOff();
	InterruptPort()->PORT &= ~InterruptPin;
	InterruptPort()->DDR &= ~InterruptPin;
}

void ExternalInterruptTest::run_event()
{
	switch (currentTestCase())
	{
	case AnyEdge:
		counts++;
		if (counts == 2)
		{
			setResultOk();
		}
		break;
	case LowLevel:
		counts++;
		break;
	case Registers:
		VERIFY(false, "unexpected interrupt")
		break;
	default:
		setResultOk();
	}
}

void ExternalInterruptTest::handleTimeout()
{
	if ((currentTestCase() == LowLevel) && (counts > 0))
	{
		setResultOk();
	}
	else
	{
		TestSuite::handleTimeout();
	}
}
