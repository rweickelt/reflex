#ifndef TESTCASE_H
#define TESTCASE_H

#include "reflex/data_types/ChainLink.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/memory/Flash.h"
#include "reflex/memory/new.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/types.h"
#include "TestEnvironment.h"

#include <stdio.h>

namespace unitTest
{
/**
 \brief An simple test component to implement driver tests in REFLEX.

 This component helps to create unit tests for driver classes which may run multiple
 test cases.

 */
class TestSuite
{
public:

	//! Placement-new operator to create test cases in the test environment.
	typedef TestSuite* (*TestCreator)(void*, uint16);

	enum Result
	{
		Uninitialized, Ok, Failed, Skipped
	};

	enum
	{
		MaxNameLength = 32
	};

	TestSuite(uint16 testCaseId);
	virtual ~TestSuite();

	inline uint8 currentTestCase() const;
	inline Result result() const;

	template<typename TestType>
	static TestSuite* create(void* addr, uint16 id);

protected:
	void setResultFailed();
	void setResultOk();
	void setResultSkipped();
	inline void setTimeout(uint16 milliseconds);
	virtual void handleTimeout();

	reflex::data_types::Singleton<reflex::OutputChannel> outputChannel;
	reflex::data_types::Singleton<TestEnvironment> environment;

private:
	enum
	{
		DefaultTimeout = 1000, MsgSize = 100, PoolSize = 1
	};

	void run_timeout();
	void setName(const char* name);

	uint8 _currentTestCase;
	Result _result;
	reflex::VirtualTimer timer_timeout;
	reflex::Event in_timeout;
	reflex::ActivityFunctor<TestSuite, &TestSuite::handleTimeout> act_timeout;
};

template<typename TestType>
TestSuite* TestSuite::create(void* addr, uint16 id)
{
	return new (addr) TestType(id);
}

uint8 TestSuite::currentTestCase() const
{
	return _currentTestCase;
}

TestSuite::Result TestSuite::result() const
{
	return _result;
}

void TestSuite::setTimeout(uint16 milliseconds)
{
	timer_timeout.set(milliseconds);
}

}

#define VERIFY(expr, ...) \
if (!(expr)) \
{ \
	setResultFailed(); \
	char buffer[30]; \
	sprintf(buffer, __VA_ARGS__); \
	outputChannel->write(buffer); \
	return; \
}

#define SKIP() \
setResultSkipped(); \
return;
#endif /* TESTCASE_H */
