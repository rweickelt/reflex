#ifndef TESTENVIRONMENT_H
#define TESTENVIRONMENT_H

#include "reflex/memory/SizedPool.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/Fifo.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/types.h"

namespace reflex
{
class Buffer;
}

namespace unitTest
{

class TestSuite;

class TestEnvironment
{
public:
	typedef TestSuite* (*TestCreator)(void*, uint16);

	reflex::SingleValue2<TestCreator, uint8> in_testCase;
	reflex::SingleValue1<uint8> in_result;

	enum State
	{
		Idle, Initializing, Running, Finished
	};

	TestEnvironment();

	inline void setOut_message(reflex::Sink1<reflex::Buffer*>* input);
	inline void setOut_finished(reflex::Sink0* input);
	inline State state() const;

private:
	template<uint16 size>
	class Data
	{
		uint8 data[size];
	};

	enum
	{
		TestBufferSize = 1024, MsgSize = 100, PoolSize = 2
	};

	void run_start();
	void run_result();
	inline TestSuite* testSuite();

	uint8 _state;
	uint8 _result;

	reflex::SizedPool<MsgSize, PoolSize> _pool;
	reflex::data_types::Singleton<reflex::OutputChannel> _outputChannel;

	reflex::Event in_stateMachine;
	reflex::ActivityFunctor<TestEnvironment, &TestEnvironment::run_start> act_start;
	reflex::ActivityFunctor<TestEnvironment, &TestEnvironment::run_result> act_result;
	reflex::Sink0* out_finished;
	Data<TestBufferSize> _testSuiteBuffer;
};

void TestEnvironment::setOut_finished(reflex::Sink0* input)
{
	out_finished = input;
}

void TestEnvironment::setOut_message(reflex::Sink1<reflex::Buffer*>* input)
{
	_outputChannel->init(input);
}

TestEnvironment::State TestEnvironment::state() const
{
	return static_cast<State>(_state);
}

TestSuite* TestEnvironment::testSuite()
{
	return reinterpret_cast<TestSuite*> (&_testSuiteBuffer);
}

}

#endif /* TESTENVIRONMENT_H */
