#ifndef TESTPLAYER_H_
#define TESTPLAYER_H_

#include "reflex/data_types/Singleton.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{

class TestListEntry;

class TestPlayer
{
public:
	TestPlayer(const TestListEntry* testList, uint8 size);
	reflex::Event in_txFinished;
	reflex::Sink2<TestSuite::TestCreator, uint8>* out_testCase;

private:
	void run_txFinished();

	reflex::data_types::Singleton<reflex::OutputChannel> outputChannel;
	reflex::ActivityFunctor<TestPlayer, &TestPlayer::run_txFinished> act_txFinished;
	const TestListEntry* testList;
	uint8 size;
	uint8 counter;
};
}

#endif /* TESTPLAYER_H_ */
