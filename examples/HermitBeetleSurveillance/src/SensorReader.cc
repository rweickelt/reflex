/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 SensorReader
 *	Author:		 Stefan Nuernberger
 */

#include "SensorReader.h"

using namespace reflex;

/** constructor */
SensorReader::SensorReader() : readResultFunctor(*this)
{
    humidity.init(&readResultFunctor);
}


/** init
 *  connect outputs of component
 *  @param sensor the hardware sensor (SHTxx)
 *  @param output subsequent output for sensor result
 */
void SensorReader::init(Sink1<uint8> *sensor, Sink1<SensorValues> *output) {
    this->sensor = sensor;
    this->output = output;
}

/** notify
 * implements Sink0
 * triggers sensor reading
 */
void SensorReader::notify() {
    if (!sensor)
        return;

    // enqueue temperature and humidity request
    sensor->assign(SENSOR_TEMPERATURE_COMMAND);
    sensor->assign(SENSOR_HUMIDITY_COMMAND);
}

/** readResults
 *  read the values from temperature and humidity input and copy to
 *  subsequent sink.
 */
void SensorReader::readResults() {
     temperature.get(current.temperature);
     humidity.get(current.humidity);

     if (output)
         output->assign(current);
}

