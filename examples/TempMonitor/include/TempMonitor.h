#ifndef TEMP_PROCESSOR_H
#define TEMP_PROCESSOR_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	TempMonitor
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: Application that monitors the environment temperature.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"

// #include "reflex/io/SHT11.h"

class TempMonitor : public reflex::Activity {
private:
	reflex::Sink0 *sensor_trigger; // triggers sensor functionality
	reflex::Sink1<char> *leds; // output to LEDs

public:
	reflex::SingleValue1<uint16> sensor_result; // buffer for sensor result

	/* constructor */
	TempMonitor();

	/** init
	 *  initialize the component
	 *  @param sensor sensor action trigger
	 *  @param leds led output
	 */
	void init(reflex::Sink0* sensor, reflex::Sink1<char>* leds);

	/** run
	 *  implements Activity
	 *  This reads the received sensor value and sets the LEDs accordingly
	 */
	void run();

private:
	reflex::VirtualTimer timer; // timer for periodic temperature sampling
	uint16 last_result; // last received result (for compare)

	// enum for led temperature indicator
	enum led_colors {
		RED = 0x1,
		BLUE = 0x2,
		GREEN = 0x4
	};

	static const Time SAMPLING_INTERVAL = 1000; // interval in milliseconds
};

#endif
