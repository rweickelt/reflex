#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Sören Höckner
# */

CC_SOURCES_CONTROLLER += \

CC_SOURCES_PLATFORM += \
	io/SHT11.cc\
	io/Led.cc \

CC_SOURCES_LIB += \
