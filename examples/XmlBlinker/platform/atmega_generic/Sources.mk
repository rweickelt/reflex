#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Richard Weickelt <richard@weickelt.de>
# *
# */

CC_SOURCES_CONTROLLER += \
	memory/Flash.cc \
	io/ExternalInterrupt.cc \
	io/PinChangeInterrupt.cc