#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Richard Weickelt <richard@weickelt.de>
# *
# */

CC_SOURCES_CONTROLLER += \
	io/Serial.cc
	
CC_SOURCES_PLATFORM += \
	io/Led.cc