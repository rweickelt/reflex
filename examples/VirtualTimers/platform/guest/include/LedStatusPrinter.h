#ifndef LEDSTATUSPRINTER_H
#define LEDSTATUSPRINTER_H

#include "reflex/io/OutputChannel.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/timer/VirtualTimer.h"

/* helper class to print LED status */
class LedStatusPrinter : public reflex::Activity {
private:
    reflex::SingleValue1<char> input; // led status input
    reflex::OutputChannel *out; // pointer to node
    reflex::VirtualTimer time; // used for system time access

public:
    /** constructor */
    LedStatusPrinter(reflex::OutputChannel *out) {
        this->out = out;
        input.init(this);
    }

    /** return input channel
     */
    Sink1<char>* get_in_input() {
        return &input;
    }

    /** run - prints current system time and led status
     */
    void run() {
        // print timestamp
        out->write("\r");
        out->write((unsigned int) time.getSystemTime());
        out->write(":  ");

        char value = input.get();
        // print status
        if (value & 0x4) // left led state
            out->write("[*]");
        else
            out->write("[ ]");
        if (value & 0x2) // middle led state
            out->write("[*]");
        else
            out->write("[ ]");
        if (value & 0x1) // right led state
            out->write("[*]  ");
        else
            out->write("[ ]  ");
        out->flush();
    }
};

#endif // LEDSTATUSPRINTER_H
