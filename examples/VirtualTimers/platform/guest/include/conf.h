#ifndef Configuration_h
#define Configuration_h

// different system constants
enum { 	IOBufferSize = 62,   	//used for OutputChannel
		NrOfStdOutBuffers = 32,
		MaxPoolCount = 1};	//amount of buffers in OutputChannel

#endif
