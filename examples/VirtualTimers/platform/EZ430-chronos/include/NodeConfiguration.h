#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"

#include "reflex/System.h"
#include "reflex/io/Display.h"

#include "BlinkLogic.h"
#include "DataDisplay.h"

namespace reflex {

/** Define different powergroups for an application.
 */
enum PowerGroups {
    DEFAULT = reflex::PowerManager::GROUP1,
    DISABLED = reflex::PowerManager::DISABLED
};

class NodeConfiguration
    : public System
{
public:
    NodeConfiguration()
        : System()
    {
        // connection of components
        blink.init(&leds, 1000);

        // power management group assignment
        timer.setGroups(DEFAULT); // put system timer in default group

        powerManager.enableGroup(DEFAULT); // this enables all registered entities in DEFAULT group (starts system timer)
        powerManager.disableGroup(~DEFAULT); // disable all others
    }

    class PseudoLeds : public Sink1<char>
    {
    public:
        PseudoLeds()
        {
            bits.init(&display.getUpperLine());
            time.init(&display.getLowerLine());
            display.setGroups(DEFAULT);
        }

        void assign(char val)
        {
            time.input.assign(theTime.getSystemTime());
            bits.input.assign((uint16) val);
        }

    private:
        Display display; ///< EZ430-Chronos Display
        DataDisplay<uint32, display::LowerLine> time; ///< display current system time (first 20 bits in hex)
        DataDisplay<uint16, display::UpperLine> bits; ///< display led bits in hex
        VirtualTimer theTime; ///< to acquire system time
    } leds; ///< pseudo led implementation

    BlinkLogic blink; ///< the blink logic
};


inline NodeConfiguration& getApplication()
{
    extern NodeConfiguration system;
    return system;
}

} // reflex

#endif
