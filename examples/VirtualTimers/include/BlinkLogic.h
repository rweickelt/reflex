#ifndef BlinkLogic_h
#define BlinkLogic_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 BlinkLogic
 *
 *	Author:		 Karsten Walther, Stefan Nuernberger
 *
 *	Description: Implements a binary counting scheme with the LEDs
 *				 The LEDs are each driven by a separate virtual timer.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/timer/VirtualTimer.h"

/** This class lets three LEDs blink in a binary counting scheme.
 * Each LED is toggled by a separate virtual timer event.
 */
class BlinkLogic {
public:
    /** Initializes the members timer and event.
     */
    BlinkLogic();

    /** Initialization of the logic, sets the pointer
     *  to the led driver and starts the timer.
     *
     *  @param leds : pointer to the led driver component
     *  @param period : time between led toggle
     */
    void init(reflex::Sink1<char>* leds, Time period);

private:
    /** virtual timers */
    reflex::VirtualTimer t0, t1, t2;

    /** The event inputs, which are notified at each virtual timer tick.
     */
    reflex::Event e0, e1, e2;


    /** blink methods */
    void blink0();
    void blink1();
    void blink2();

    /** activity functors */
    reflex::ActivityFunctor<BlinkLogic, &BlinkLogic::blink0> func0;
    reflex::ActivityFunctor<BlinkLogic, &BlinkLogic::blink1> func1;
    reflex::ActivityFunctor<BlinkLogic, &BlinkLogic::blink2> func2;

    /** led state */
    char currentState;
    /** blink period */
    unsigned period;

    /** pointer to led driver object
     */
    reflex::Sink1<char>* leds;
};

#endif

