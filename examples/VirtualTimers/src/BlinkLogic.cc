/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 BlinkLogic
 *
 *	Author:		 Karsten Walther, Stefan Nuernberger
 */

#include "BlinkLogic.h"
#include "NodeConfiguration.h"

using namespace reflex;

BlinkLogic::BlinkLogic() :
	t0(VirtualTimer::PERIODIC),
	t1(VirtualTimer::PERIODIC),
	t2(VirtualTimer::PERIODIC),
	func0(*this), func1(*this), func2(*this)
{
	t0.connect_output(&e0);
	t1.connect_output(&e1);
	t2.connect_output(&e2);
	e0.init(&func0);
	e1.init(&func1);
	e2.init(&func2);
}

void BlinkLogic::init(Sink1<char>* leds, Time period)
{
	this->leds = leds;
	currentState = 0;
	this->period = period;
	t2.set(period << 2);
	t1.set(period << 1);
	t0.set(period);
}

void BlinkLogic::blink0() {
	currentState ^= 0x01;
	leds->assign(currentState);
}
void BlinkLogic::blink1() {
	currentState ^= 0x02;
	leds->assign(currentState);
}
void BlinkLogic::blink2() {
	currentState ^= 0x04;
	leds->assign(currentState);
}
