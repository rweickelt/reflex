/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		 Karsten Walther
 */
#include "UpdateDataSend.h"
#include "NodeConfiguration.h"
#include "reflex/updater/UpdateTypes.h"

#define TIMER_TIME 500

using namespace reflex;

extern "C" void* __codeStart__;

UpdateDataSend::UpdateDataSend() : Activity()
	,timer(VirtualTimer::PERIODIC)
	,sendReadyFunctor(*this)
	,receiveFunctor(*this)
	//,setupFunctor(*this)

{
	//successfulSend = true;

    localCounter = 0;


	timer.connect_output(&clockEvent);
	timer.set(TIMER_TIME);

	//connecting events/functors etc.
	clockEvent.init(this);//calls the run method
	sendReady.init(&sendReadyFunctor);
    receiveDataFromRadio.init(&receiveFunctor);
    //setupSensornode.init(&setupFunctor);
    noOfPackets = 0;
    readPos = 0;
    remainder = 0;
    state = META_INIT_STATE;
}


//#define PACKET_NO 11

//becomes active if timer triggers an interrupt
void UpdateDataSend::run()
{
	 Buffer *buf = new(&getApplication().pool) Buffer(&getApplication().pool);


	 getApplication().display.getSymbols().toggle(display::ICON_BEEPER3);


	 switch (state)
	 {
	 case META_INIT_STATE:
	 {

		 UpdateInitPacket packet;

		 packet.type = UPDATE_INIT;
		 packet.setProgramID(persistentUpdateMem.programID + 1, 0, 1);//setting overwrite bit
		 packet.version = 1;

		 // just some irrelevant values
		 packet.hopCount = 2;
		 packet.hopDelayOffset = 3;
		 packet.networkDiameter = 4;
		 packet.updateDelay = 5;
		 packet.reserved = 0xff;

		 //the remaining components are currently not interresting
		 getApplication().showUpperLCD((sizeof(UpdateInitPacket) << 8) | 0xFF);
		 getApplication().showLowerLCD(0);

		 buf->write((void*)&packet, sizeof(UpdateInitPacket));
		 state = META_INFO_STATE;
		 break;
	 }//0


	 case META_INFO_STATE:
	 {
		 UpdateMetaInfoPacket packet;

		 //sending update metapacket
		 readPos = (uint16)(&__codeStart__);

		 noOfPackets =  ((0xFFFF - (uint16)(&__codeStart__)) + 1) / sizeof(UpdateDataPacket().data);
		 packet.remainder =  (0xFFFF - (uint16)(&__codeStart__) + 1) % sizeof(UpdateDataPacket().data);//don't forget 0xffff
		 remainder = packet.remainder;

		 if ( remainder != 0)
			 noOfPackets++;

		 getApplication().showUpperLCD((sizeof(UpdateMetaInfoPacket) << 8) | 0xFF);
		 getApplication().showLowerLCD(readPos);
		 packet.type = UPDATE_META_INFO;
		 packet.setProgramID(persistentUpdateMem.programID + 1, 0, 1);
		 packet.version = 1;
		 packet.packetCount_run =  (3 << RUN_SHIFT) | ( (noOfPackets) << SEQ_NO_SHIFT);
		 packet.probability = 111;//no idea

		 //noOfPackets++;//workaround -> to ensure that enough data packet will be sent
		 localCounter = --noOfPackets;//in this case 511
		// noOfPackets--;

		 //assign data
		 buf->write((void*)&packet, sizeof(UpdateMetaInfoPacket));
		 state = META_UPDATE_STATE;
		 break;
	 }//1

	 case META_UPDATE_STATE:
	 {
		 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		 //forces Reset!
		// *((uint16*)0x015C) = 0;
		 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		 UpdateDataPacket u_packet;
		 getApplication().showUpperLCD(0xAA);
		 getApplication().showLowerLCD(readPos);

		 u_packet.type = UPDATE_DATA;
		 u_packet.setProgramID(persistentUpdateMem.programID + 1, 0, 1);
		 u_packet.version = 1;

		 u_packet.seqNo_run = (0 << RUN_SHIFT) | ((noOfPackets - localCounter) << SEQ_NO_SHIFT);


		 // localCounter is equal to sequence number
		 if(localCounter-- != 0)
		 {
			 for (uint8 i = 0; i < sizeof(u_packet.data); i++)
				 u_packet.data[i] = *((uint8*)readPos++);
		 } else {
			remainder = (!remainder) ? sizeof(u_packet.data) : remainder;

			 //writing the last bytes of the image
			 for (uint8 i = 0; i < remainder; i++)
				 u_packet.data[i] = *((uint8*)readPos++);
			 state = META_INIT_STATE;
		 }

		 //assign data
		 buf->write((void*)&u_packet, sizeof(UpdateDataPacket));
	 }

	 }//case



	 //sending data
	 sendToRadio->assign(buf);
}


//void DetectionSystem::threasholdReached()
//{
//	//currently nothing
//}

//void UpdateDataSend::init(Sink1<Buffer*> *sendToRadio, reflex::Sink1<reflex::Buffer*> *confData)
//{
//    this->sendToRadio = sendToRadio;
//    radioConfData = confData;
//}

void UpdateDataSend::send()
{
	//nothing
}


void UpdateDataSend::receiveData()
{
    Buffer *buf =  (Buffer*)receiveDataFromRadio.get();
    if(buf)
    {
    	buf->downRef(); //buffer is not needed anymore
//    	getApplication().showLowerLCD(buf->getLength());
//    	uint16 tmp = 0;
//    	buf->read(tmp);
//    	getApplication().showUpperLCD(tmp);
    }
    //uint8 size = buf->getLength();

//    if(size > 2)
//    {
//        uint16 tmp;
//    	buf->read(tmp);
//    	getApplication().showLowerLCD(tmp);
//    }
//    uint8 tmp;
//    //byte						  0					  1
//    //					___________________|____________________|____________
//    //bit			   	7	      3		  0	7		  3			0	    ...
//    //expected packet: | reserved | nodeID | reserved | command | payload |
//    buf->read(tmp);
//    //this packet is not addressed to this node or the packet size is too small
//    if((tmp & 0xF) == NODEID && size > 2)
//    {
//        successfulSend = false;
//        getApplication().showUpperLCD(0);
//    	buf->read(tmp);//read command
//    	command cmd = (command)tmp;
//    	//commando interpretation
//    	switch(cmd)
//    	{
//    	case CHANGE_TIMER:
//			{
//				Time newTime;
//				buf->read(newTime);
//				getApplication().showLowerLCD((uint16)newTime);
//				timer.set(newTime);
//
//				//reply to AP -> changing the time was successful
//			    Buffer *sending = new(&getApplication().pool) Buffer(&getApplication().pool);
//			    sendBuf[1] = CHANGE_TIMER;
//			    sending->write(sendBuf, 2);
//			    sendToRadio->assign(sending);
//				break;
//			}
//
//    	default:
//    		//not implemented
//    		break;
//    	}
//    }

}

//void UpdateDataSend::changeSetup()
//{
//	Buffer *buf = (Buffer*)setupSensornode.get();
//
//	// process and interpret data
////	uint8 tmp;
////	RadioConfiguration cfg;
////	buf->read(&tmp, cfg.getSize());
////	cfg.setData(&tmp);
//
//	// inform the radio driver to perform the changes -> need a new buffer??
//	//Buffer *sendCfg = new(&getApplication().pool) Buffer(&getApplication().pool);
////	sendCfg->write(tmp);
//	//confData->assign(buf);
//
//	//release buffer
//	buf->downRef();
//}


