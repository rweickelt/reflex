#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# *
# */

CC_SOURCES_PLATFORM += \
		io/Led.cc\
		io/TMoteRadio.cc\
        io/CC2420.cc\
		io/SPI.cc\
		io/UserButton.cc\

CC_SOURCES_CONTROLLER += \
			io/Serial.cc \
			memory/Flash.cc \
			io/ADConverter12.cc \
			update/Sections.cc \
			update/UpdateManager.cc\


ASM_SOURCES_CONTROLLER +=
