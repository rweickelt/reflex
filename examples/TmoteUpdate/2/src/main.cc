/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	none
  
 *	Author:		Carsten Schulze
 *
 *	Description: main.cc, Standardmain
 */

#include "NodeConfiguration.h"
using namespace reflex;

namespace reflex {
	NodeConfiguration system;
} //reflex


int main()
{
//#if (NODEID!=0)
	//ID FINDING
//	__systemStatusMem.setByte(StatusMem::MYID, NODEID);
//#endif

	/*if ( __systemStatusMem.myid == 0) {
		__systemStatusMem.setByte(StatusMem::UPDATE_DISTRI_HOP_COUNT, 1); // send the Update only over one hop
	}*/
  getSystem().status.load();
	getSystem().scheduler.start();
	return(0);
}
