/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "HelloWorld.h"
#include "NodeConfiguration.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/update/Command.h"


using namespace reflex;


HelloWorld::HelloWorld() :
		timer(Prescaler::PERIODIC),
		tick(0),
		buttonHandler(*this)
	

{
	timer.connect_output(&ready);
	ready.init(this);
	button.init(&buttonHandler);
	timer.set(3500);

	temp = false; // switch between send update and remote reboot
}

void HelloWorld::run()
{
	tick++;
	  getApplication().out.write("version:");
	  getApplication().out.write((int)getApplication().status.version);
	  getApplication().out.write("| cnt:");
	  getApplication().out.write(tick);
	  getApplication().out.writeln();
	

}



void HelloWorld::handleButton()
{

 getApplication().led.blink(Led::BLUE);


 

    Buffer* buffer = new (&getApplication().radioPool) Buffer(&getApplication().radioPool);


    /*
     * Send start msg to updatemanager on this node
     * increase version of software
     */
   if (temp == false)
   {
	temp = true;
	getApplication().status.version++;
    getApplication().status.save();



    Command command;
    command.version = 0; //0 is the magic version number
    command.commandID = Command::START;
    command.startCommand.targetMode = Command::WAIT;
    command.startCommand.nodeID = 0;
    buffer->push((uint8) 1); // 1 determinates that this is a packet for the update manager
    buffer->write(command);

    getApplication().updateMan.input.assign(buffer);
    getApplication().out.write("send init");
   	    getApplication().out.writeln();


   } else
	   /*
	    * send remote reboot command to install image
	    */

   {
	   Command command;
	   command.version = 0;
	    command.commandID = Command::REBIRTH;
	    command.rebirthCommand.doIt = true;

	    buffer->push((uint8) 1); // 1 determinates that this is a packet for the update manager
	    buffer->write(command);

	    getApplication().out.write("send reboot");
	    getApplication().out.writeln();
	    getApplication().led.blink(Led::BLUE);
	    getApplication().radio.input.assign(buffer);
   }


}
