#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/io/Serial.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/io/Led.h"
#include "reflex/io/DS2411.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/misc/Distributor.h"

#include "reflex/System.h"

#include "Sendid.h"
#include "Receiveid.h"

namespace reflex {

enum PowerGroups {
	AWAKE = reflex::PowerManager::GROUP1, 
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};


class NodeConfiguration 
	: public System 
{
public:
	NodeConfiguration() 
		: System()
		, clockDistr()
		, pool(0) //the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
		, radioPool(0)
		, out(&pool)
		, radio(radioPool)
		, serial(1,SerialRegisters::B19200) //initialize the serial interface on USART1 with 19200 baud
	{
		rndNumber[1] = 15417;rndNumber[2] = 83541;rndNumber[3] = 83363;rndNumber[4] = 31376;rndNumber[5] = 65922;rndNumber[6] = 44623;rndNumber[7] = 65805;rndNumber[8] = 93688;rndNumber[9] = 36787;rndNumber[0] = 15064;
		timer.setGroups(AWAKE); //timer is part of groups SLEEP and group AWAKE
		radio.setGroups(AWAKE);
		out.init(&serial.input);
		//wires components together
		ds.init();
		ds.fetch();
		sid.init(&radio.input);
		rid.init();
		radio.init(&rid,&sid);
		clockDistr.registerSink(sid.timer);
		clockDistr.registerSink(rid.timer);
		clock.init(clockDistr);

		once = false;

		powerManager.enableGroup(AWAKE); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
		powerManager.disableGroup(SLEEP); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE.
	}

	void incSendTimer()
	{
		if(!once)
		{
			out.write("Timer inc by:");
			int newtime = ((rand()%50)+1)/10;
			out.write(newtime);
			out.writeln();
			sid.timer.set(50+newtime);
			once = true;
		}
	}

	int rand()
	{
		return rndNumber[seed_base];
	}

	void seed(char *id,size_t size)
	{
		for(unsigned int i = 0;i < size;i++)
			seed_base+=id[i];
		seed_base %= 10;
	}

	int seed_base;     ///< for rng
	int rndNumber[10];
	bool once;

	Distributor<2> clockDistr;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobject with static size
	SizedPool<IOBufferSize,NrOfStdOutBuffers> radioPool;
	OutputChannel out; ///< an object that provides easy formated output. 
	Led leds; ///< driver to bring some colorful light on that platform

	PoolManager poolManager; ///< managed different pools

	TMoteRadio radio;
	Serial serial;	///< the driver for the serial interface

	Sendid sid;
	Receiveid rid;
	DS2411 ds;
};


inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
