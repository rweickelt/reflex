#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Karsten Walther
# *
# *	(c) Carsten Schulze, BTU Cottbus 2005
# */

CC_SOURCES_LIB += \
	memory/Buffer.cc \
	memory/PoolManager.cc \
	memory/Pool.cc \
	memory/FreeList.cc \
	io/OutputChannel.cc \
	

#sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk
