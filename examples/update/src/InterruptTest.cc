
#include "InterruptTest.h"
#include "NodeConfiguration.h"
#include "conf.h"

#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/MachineDefinitions.h"
using namespace reflex;


InterruptTest::InterruptTest() 	: InterruptHandler(interrupts::PORT2,PowerManageAble::SECONDARY )
{
  	numberOfInterrupts = 0;
}

void InterruptTest::removeInterrupt()
{
  this->disable();
  getSystem().guardian.registerInterruptHandler(&InterruptHandler::invalidInterruptHandler, mcu::interrupts::PORT2);
}



void InterruptTest::handle()
{
   numberOfInterrupts++;

  Display::LowerLine& lLine= getApplication().display.getLowerLine();
  lLine=(uint16) numberOfInterrupts;

	//reset interrupt flags
  Port2()->IFG &= ~(0x1F);

}

void InterruptTest::enable()
{
	//select io function for pins
	Port2()->SEL &= ~0x1F;
	//configure pins as input
	Port2()->DIR &= ~( 0x1F );
	//enable internal pulldowns
	Port2()->OUT &= ~(0x1F );
	Port2()->REN |= 0x1F;
	//set interrupts on low to high transition
	Port2()->IES &= ~( 0x1F );
	//reset interrupt flags
	Port2()->IFG &= ~(0x1F);
	//enable the interupts for the connected pins
	Port2()->IE |= 0x1F;
}


void InterruptTest::disable()
{
  Port2()->IE &= ~(0x1F);

}

/**
 * Connect this component to others and start it.
 */
void* startup()
{
  InterruptTest* comp = new (getApplication().memoryManager) InterruptTest();
  comp->enable();
  return (void*) comp;
}

/**
 * Disconnect this component from system
 */
bool shutdown(void* addr)
{

  InterruptTest* comp = (InterruptTest*) addr;
  comp->removeInterrupt();
  MemoryManager::freeRam(addr);
  delete (getApplication().memoryManager, comp);
  return true;
}
