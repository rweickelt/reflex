#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		 Karsten Walther
# */

CC_SOURCES_APPLICATION += \
	src/main.cc \
	src/msgInterpreter.cc \
	src/radioDelay.cc 

CC_SOURCES_COMPONENT_APPLICATION_COMPLEX = \
 	src/Complex.cc \

CC_SOURCES_COMPONENT_APPLICATION_INTERRUPT = \
 	src/InterruptTest.cc \

CC_SOURCES_COMPONENT_PLATFORM_INTERRUPT = \
 	io/Display.cc \

CC_SOURCES_LIB += \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/Buffer.cc \
	memory/PoolManager.cc \


CC_SOURCES_CONTROLLER += \
	flash/FLASH.cc


sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk
