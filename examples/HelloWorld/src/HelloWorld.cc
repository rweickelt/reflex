/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "HelloWorld.h"
#include "NodeConfiguration.h"


HelloWorld::HelloWorld(reflex::OutputChannel *out)
	: timer(reflex::VirtualTimer::PERIODIC)
	, tick(0)
{
    this->out = out;
	ready.init(this);
	timer.connect_output(&ready);
	timer.set(1000);
}

void HelloWorld::run()
{
	tick++;
	
    out->write("Hello World!");
    out->writeln();
}
