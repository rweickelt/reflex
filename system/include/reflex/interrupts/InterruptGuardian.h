#ifndef InterruptGuardian_h
#define InterruptGuardian_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	InterruptGuardian
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Common interrupt handling supervisor.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/types.h"

namespace reflex {

/** InterruptGuardian is the supervisor for interrupts. It implements in main 2
 *  things.
 *
 *  1. Management and calling of interrupthandlers.
 *  In general the Guardian attaches physical interruptvectors to logical ones,
 *  and take care that the corresponding handler is called in case of interrupt.
 *  While the logical vectortable must only contain handles for used interrupts.
 *  This allows significantly reduction of memory used, while count of wrapper
 *  functions can be reduced.
 *
 *  2. Signaling the scheduler occured interrupts.
 *  For coping with nested interrupts or parallel software initiated scheduling
 *  requests in priority based scheduling schemes the scheduler must be informed,
 *  so that interrupt handling is active for proper synchronization in scheduling.
 *
 *  See also Documentation for interruptfunctions and InterruptHandler.
 */
class InterruptGuardian
{
public:
	/**
	 *  Initializes the logical vectortable.
	 */
	InterruptGuardian()
	{
		handlerCount=0;
		init();
	}

	/**
	 *  Calls the specific handler. This method is inline since it is called
	 *  only by the static handle method, and interrupt handling must be
	 *  efficient.
	 */
	void handle(uint8 number)
	{
		handlers[number]->handle();
	}

	/**
	 *  Assigns a logical interrupt vector for this handler. This is done by attaching a
	 *  the an available wrapper to the given physical interruptvector and inserting of
	 *  specific handler in logical interruptvectortable.
     *
	 *  The implementation for this function is in MachineInterruptVector.cc
	 *
	 *  @param handler the handling object
	 *  @param vector the vector of the interrupt to handle given in InterruptVector.h
	 */
	void registerInterruptHandler(InterruptHandler* handler, mcu::InterruptVector vector);


	/*!
	 \brief Removes a \a handler registered to \a vector from the interrupt vector table.

	 On success, \a vector will point to InterruptHandler::invalidInterruptHandler.
	 This method will fail when trying to remove a \a handler from a \a vector that it has
	 not been registered to.
	 */
	void unregisterInterruptHandler(InterruptHandler* handler, mcu::InterruptVector vector);

private:
	/**
	 *  Array for pointer to interrupthandlers, MAX_HANDLERS must be determined by machine
	 *  implementation in MachineInterruptGuardian.h .
	 */
	InterruptHandler* handlers[mcu::interrupts::MAX_HANDLERS];

	/**
	 *  This is the counter for registered handlers - is needed only if for
	 *  resource saving the logical interrupt handler table has less entries
	 *  than the controller has interrupts.
	 */
	uint8 handlerCount;

	/**
	 *	Init is for standard initialzation of the array of interrupthandlers.
	 */
	void init();
};

} //namespace reflex

#endif
