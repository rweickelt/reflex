#ifndef UpdateDaemon_h
#define UpdateDaemon_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	UpdateDaemon
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Masterblock for all update components, controls
 *					the sending and receiving of images. Implemented as
 *					template with the memory device (e.g. internal flash)
 *					as parameter. To include the update, the update manager
 *					from the platform and the sections object are needed.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "reflex/memory/Buffer.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Event.h"
#include "reflex/memory/Pool.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/update/Command.h"
#include "conf.h"
#include "reflex/System.h"
#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptLock.h"
//#include "NodeConfiguration.h"
#include "reflex/SystemStatusBlock.h"
#include "reflex/io/Led.h"



namespace reflex {

template<class D>
class UpdateDaemon : public Activity {
public:




    UpdateDaemon();


    /**
     * Connect component to the application
     * @param MemoryDevice the device the update should be stored and
     * 		  loaded
     * @param ioDevice referece to the radio/mac
     * @param macTimerInput reference to the send confirmation from radio/mac
     * @param applicationOutput reference to the application
     * @param pool reference to a pool
     */
	void init(D* MemoryDevice,
			  Sink1<Buffer*>* ioDevice,
			  Sink0* macTimerInput,
			  Sink1<Buffer*>* applicationOutput,
			  Pool* pool)
	{
		this->MemoryDevice = MemoryDevice;
		this->output = ioDevice;
		this->macTimerInput = macTimerInput;
		this->applicationOutput = applicationOutput;
		this->pool = pool;
	}

	/**
	 * Decide what type of packet was received and distribute it
	 * to the corresponding update function or to the application
	 * logic if not an update packet
	 */
	virtual void run();

	Event forwardingFinished;
	Event waitEvent;


	VirtualTimer timer; //for deferring send
	friend class NodeConfiguration;

	Queue<Buffer*> input;
	Queue<Buffer*> applicationInput;
	Sink1<Buffer*>* output;
	Sink1<Buffer*>* applicationOutput;

	enum MsgType {
		APPLICATION = 0,
		UPDATE = 1


	};

private:
	enum UpdateState {
		IDLE = 0,
		INIT = 1,
		FORWARD = 2,
		REBIRTH = 3
	};

	struct Block {
		char data[UpdateBlockSize];
	};

	bool LOCKED;
	Buffer* packet;
	Buffer* applicationPacket;
	Command currentCommand;
	uint16 count, blockCount;
	uint8 newVersion;
	//MsgType MType;
	uint8 MType;
	uint8 mode;

	uint16 ImageOverallSize, ImagePaketNumber;

	caddr_t currentBlock;
	caddr_t imageStart;
	caddr_t imageEnd;
	Sink0* macTimerInput;
	char bitfield[(UpdateArea/UpdateBlockSize)/8]; //allocate dynamically
	UpdateState state;
	uint8 globalRepeats;
	uint8 localRepeats;
	bool startNode;
	Pool* pool;

	/**
	 * push marker for application data onto buffer and
	 * forward it to the radio
	 */
	void forwardApplicationData();

	/**
	 * radio has finished send operation
	 */
	void handleForwardingFinished();

	/**
	 * send next part of the image if parts left
	 */
	void sendNextImagePart();
	ActivityFunctor<UpdateDaemon,&UpdateDaemon::handleForwardingFinished> forwardingFinishedFunctor;
	ActivityFunctor<UpdateDaemon,&UpdateDaemon::sendNextImagePart> sendNextImagePartFunctor;

	/**
	 * setup sending of own image
	 * construct a packet for the other nodes containing
	 * necessary informations. Start timer for send first
	 * part
	 *
	 * @param packet
	 */
	void startUpdate(Buffer* packet);

	/**
	 * setup receiving image, delete temporary space for the data
	 *
	 * @param packet
	 */
	void initUpdate(Buffer* packet);

	/**
	 * write new part on the right place within the temporary space
	 * if image is complete:
	 * 		a) install image with rebirth()
	 * 		b) forward own image and install
	 * 		c) wait for command
	 * depending on update mode
	 *
	 * @param packet
	 */
	void insertPacket(Buffer* packet);

	/**
	 * save system status and install new image
	 */
	void rebirth();

	bool doInit;

	bool red;

	D* MemoryDevice;

};

} //end namespace reflex

//#include "NodeConfiguration.h"

//doxygen cannot handle that member methods then including that file within
//the reflex namespace
#include "reflex/update/UpdateDaemon.cc"

#endif
