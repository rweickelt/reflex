/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		 "Hannes Menzel"
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */


#ifndef ACCELERATIONSENSORCONFIGURATION_H_
#define ACCELERATIONSENSORCONFIGURATION_H_

#include "reflex/driverConfiguration/ConfigurationObject.h"

/**
 * implements an acceleration independent configuration object
 */
class AccelerationSensorConfiguration : public ConfigurationObject<AccelerationSensorConfiguration>
{

public:


	/**
	 * defines the different working modes of the acceleration sensor
	 */
	enum mode
	{
		MEASUREMENT = 0
		, FREEFALL
		, MOTION
		, DISABLE
		, UNDEF//adding other possibilities
	};

public:

	struct Configuration
	{
		/**
		 * some sensors define a time (in ms), when an interrupt is triggered depending on special detection
		 * mode
		 */
		uint16 time;

		/**
		 * threshold for different interrupt triggering detection modes
		 */
		int16 threshold;

		/**
		 * sample rate in Hz
		 */
		uint16 sampleRate;

		/**
		 * contains the resolution in 1/10 g
		 */
		uint8 resolution;

		/**
		 * the uses measurement mode
		 */
		mode workingMode;

		/**
		 * enables / disables interrupt
		 */
		bool interrupt_enable;
	};

	AccelerationSensorConfiguration()
	{
		clear();
	}

	/**
	 * reset the configuration to 0
	 */
	void clear()
	{
		data.time = 0;
		data.threshold = 0;
		data.resolution = 0;
		data.sampleRate = 0;
		data.workingMode = UNDEF;
		data.interrupt_enable = false;
	}

	/**
	 * when implementing a new configurable driver, then you should start
	 * using the resolution with VERYLOW_RES
	 *
	 */
//	enum resolution
//	{
//		VERYLOW_RES = 0
//		, LOW_RES
//		, MEDIUM_RES
//		, HIGH_RES
//		, VERYHIGH_RES
//	};

	/**
	 * when implementing a new configurable driver, then you should start
	 * using the samplingRate with VERYLOW_SAMPLE
	 */
//	enum samplingRate
//	{
//		VERYLOW_SAMPLE = 0
//		, LOW_SAMPLE
//		, MEDIUM_SAMPLE
//		, HIGH_SAMPLE
//		, VERYHIGH_SAMPLE
//	};




	inline uint8 getResolution()
	{
		return data.resolution;
	}

	inline uint16 getSamplingRate()
	{
		return data.sampleRate;
	}

	inline mode getMode()
	{
		return data.workingMode;
	}

	inline uint16 getTime()
	{
		return data.time;
	}

	inline int16 getThreshold()
	{
		return data.threshold;
	}

	inline bool getInterrupt()
	{
		return data.interrupt_enable;
	}

	/**
	 * @param val a resolution in 1/10g
	 */
	inline void setResolution(uint8 val)
	{
		data.resolution = val;
	}

	/**
	 * @param val a sampling rate in Hz
	 */
	inline void setSamplingRate(uint16 val)
	{
		data.sampleRate = val;
	}


	/**
	 * @param val an allowed working mode of the sensor
	 */
	inline void setMode(mode val)
	{
		data.workingMode = val;
	}

	/**
	 * @param time the duration where the given threshold must be reached
	 */
	inline void setTime(uint16 time)
	{
		data.time = time;
	}

	/**
	 * @param threshold in milli-g
	 */
	inline void setThreshold(int16 threshold)
	{
		data.threshold = threshold;
	}

	/**
	 * @param in true -> acceleration driver have to to throw interrupt
	 * with respect to the current mode
	 * @warning: measurement mode can normally work with or without interrupts
	 * @warning: a motion detection mode becomes only useful if interrupts are enabled
	*/
	inline void setInterruptDriven(bool in)
	{
		data.interrupt_enable = in;
	}



	/**
	 * @param setup expects an instance of a Configuration
	 */
//	void setData(void* setup)
//	{
//		data = *((Configuration*)setup);
//	}

private:

	/**
	 * defines masks and shifts
	 */
//	enum mask
//	{
//		MASK_RES = 0x7
//		, MASK_SAMPLE = 0x38
//		, MASK_MODE = 0xC0
//		//shifts
//		, SHIFT_RES = 0
//		, SHIFT_SAMPLE = 3
//		, SHIFT_MODE = 6
//	};

	void* getData()
	{
		return this;
	}


	uint8 getSize()
	{
		return sizeof(AccelerationSensorConfiguration);
	}

	/**
	 * stores the current configuration
	 */
	Configuration data;

};

#endif /* ACCELERATIONSENSORCONFIGURATION_H_ */
