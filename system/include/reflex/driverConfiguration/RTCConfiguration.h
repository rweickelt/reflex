#ifndef RTCCONFIGURATION_H
#define RTCCONFIGURATION_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Hannes Menzel
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 *
 */

// needed for date-datatype
#include "reflex/rtc/RTC_types.h"



class RTCConfiguration : public ConfigurationObject<RTCConfiguration>
{

public:

	/**
	 * stores the configuration within a object
	 */
	struct Configuration
	{
		/**
		 * kind of interval
		 */
		RTC::RefreshInterval interval;

		/**
		 * number of intervals before fireing an Event
		 */
		uint16 num;
	};

	/**
	 * set interval to seconds and num to 0
	 */
	RTCConfiguration()
	{
		clear();
	}


	void clear()
	{
		cfg.interval = RTC::SECONDS;
		cfg.num = 0;
	}

	/**
	 * this method determines how often a timer event must occur until
	 * an external event is triggered
	 *
	 * calling or triggering this method resets the counter
	 *
	 * Example: setRefreshInterval(MINUTES); setNumberOfIntervals(5);
	 * in this case in 5 minutes an event fires
	 *
	 * @param num number of intervals
	 */
	inline void setNumberOfIntervals(const uint16 num)
	{
		cfg.num = num;
	}


	/**
	 * @return number of stored interval count
	 */
	uint16 getNumberOfIntervals()
	{
		return cfg.num;
	}


	/**
	 * determines the interval, when event is triggered
	 * @param rm interval
	 */
	void setPrescaling(const RTC::RefreshInterval rm)
	{
		cfg.interval = rm;
	}

	/**
	 * returns the stored refresh interval
	 */
	RTC::RefreshInterval getPrescaling()
	{
		return cfg.interval;
	}

private:

	/**
	 * stored configuration for this object
	 */
	Configuration cfg;
};

#endif
