/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		 Karsten Walther, Stefan Nuernberger
 */

// include header file for the IDEs (.cc is included from .h)
#include "reflex/sinks/SingleValue.h"

namespace reflex {

template <typename T1>
void SingleValue1<T1>::assign(T1 value)
{
    InterruptLock lock;

    v1 = value;
    if(free)
    {
        free = false;
        trigger();
    }
}

template <typename T1>
T1 SingleValue1<T1>::get()
{
    InterruptLock lock;
    free = true;

    return v1;
}

template <typename T1>
void SingleValue1<T1>::get(T1& v1)
{
    InterruptLock lock;
    free = true;

    v1 = this->v1;
}


template <typename T1, typename T2>
void SingleValue2<T1,T2>::assign(T1 v1, T2 v2)
{
    InterruptLock lock;

    this->v1 = v1;
    this->v2 = v2;
    if(free)
    {
        free = false;
        trigger();
    }
}

template <typename T1, typename T2>
void SingleValue2<T1,T2>::get(T1& v1, T2& v2)
{
    InterruptLock lock;
    free = true;

    v1 = this->v1;
    v2 = this->v2;
}

template <typename T1, typename T2, typename T3>
void SingleValue3<T1,T2,T3>::assign(T1 v1, T2 v2, T3 v3)
{
    InterruptLock lock;

    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;
    if(free)
    {
        free = false;
        trigger();
    }
}

template <typename T1, typename T2, typename T3>
void SingleValue3<T1,T2,T3>::get(T1& v1, T2& v2, T3& v3)
{
    InterruptLock lock;
    free = true;

    v1 = this->v1;
    v2 = this->v2;
    v3 = this->v3;
}

} // ns reflex

