/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		 Karsten Walther, Stefan Nuernberger
 *
 */

#include "reflex/sinks/Fifo.h"

template <typename T1, uint8 n>
void Fifo1<T1,n>::assign(T1 value)
{
    InterruptLock lock;
    if(count<n){

        vs1[(readPos + count) % n] = value;
        count++;

        trigger();
    }
}

template <typename T1, uint8 n>
T1 Fifo1<T1,n>::get()
{
    InterruptLock lock;
    T1 retVal = vs1[readPos];
    if(count){
        count--;
        readPos = (readPos+1) % n;
    }
    return retVal;
}

/*!
 \brief Returns true if no elements are stored.
 */
template <typename T1, uint8 n>
bool Fifo1<T1,n>::isEmpty() const
{
    return count == 0;
}

template <typename T1, typename T2, uint8 n>
void Fifo2<T1,T2,n>::assign(T1 v1, T2 v2)
{
    InterruptLock lock;
    if(count<n){
        uint8 pos = (readPos + count) % n;
        vs1[pos] = v1;
        vs2[pos] = v2;
        count++;
        trigger();
    }
}

template <typename T1, typename T2, uint8 n>
void Fifo2<T1,T2,n>::get(T1& r1, T2& r2)
{
    InterruptLock lock;

    if(count){
        r1 = vs1[readPos];
        r2 = vs2[readPos];
        count--;
        readPos = (readPos + 1) % n;
    }
}

/*!
 \brief Returns true if no elements are stored.
 */
template <typename T1, typename T2, uint8 n>
bool Fifo2<T1,T2,n>::isEmpty() const
{
    return count == 0;
}

