#ifndef PowerManager_h
#define PowerManager_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	PowerManagement
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Coordinates power management
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/powerManagement/PowerGroups.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/scheduling/Scheduler.h"
#include "reflex/data_types/FifoQueue.h"
#include "reflex/types.h"
#include "conf.h"

extern "C" void _interruptsEnable();

namespace reflex {


/** This class provides basic power management facilities for the application
 *  programmer. The programmer only needs to call one of the functions,
 *  power management then takes the appropriate actions to power down the
 *  system, while keeping it in a consistent state (no more pending activities).
 *
 *	Note that the system always goes into wait mode if it is idle to preserve
 *  energy.
 *
 *	To understand the scheme a deep understanding of event flow systems is
 *  required.
 **/
class PowerManager 
{
public:
	enum {
		NOTHING = powerManagement::NOTHING, //initial groups of powermanageable entities
		DISABLED = powerManagement::DISABLED,
		GROUP1 = powerManagement::GROUP1,
		GROUP2 = powerManagement::GROUP2,
		GROUP3 = powerManagement::GROUP3,
		GROUP4 = powerManagement::GROUP4,
		GROUP5 = powerManagement::GROUP5,
		GROUP6 = powerManagement::GROUP6,
		GROUP7 = powerManagement::GROUP7,
		GROUP8 = powerManagement::GROUP8,
		GROUP_COUNT=powerManagement::GROUP_COUNT //number of distinced groups
	};
public:
	PowerManager();

	void enableObject(PowerManageAble* object);
	void disableObject(PowerManageAble* object);

	/** enable or disable groups inside the powermanager.
	 *	That enables/disables all groups that are assigned to a given group
	 * 
	 * @param value	bitmask of the group setting (enable bit)
	 * @param mask 	bitmask of affected groups that will be changed
	 */
	void switchMode(const powerManagement::PowerGroups value, const powerManagement::PowerGroups mask);

	/** enable a group of powermanageables entities.
	 * @param groups :  could be one or more groups, that should be enabled
	 */
	void enableGroup(const powerManagement::Groups_t groups);

	/** disable a group of powermanageables entities.
	 * @param groups :  could be one or more groups, that should be disabled
	 */
	void disableGroup(const powerManagement::Groups_t groups);

	/**
	 * @return	copy of the bitfield of Powergroups
	 */
	const powerManagement::PowerGroups getEnabledGroups() const { return static_cast<powerManagement::PowerGroups>(currentMode);}

	/** This method is called by the scheduler, when there are no runnable
	 * activities. It enters the deepest sleep that can be entered at a time. Interrupts will be
	 * enabled within this method. Moreocer there are enabled when it returns
	 **/
	inline
	void powerDown() const;

protected:
	//friend class ::reflex::Scheduler;  //friend with typedef works not on all compilers
	friend void ::reflex::Scheduler::start();
	friend class ::reflex::PowerManageAble;
	
	/** register a powermanageable object. This will be done by the class PowerManageAble himself.
	 * @param object : pointer to the a powermangeabled object
	 */
	inline
	void registerObject(PowerManageAble* object)  { queue.enqueue(object); }

	/** powerdown method to bring the system in active mode. This method does nothing since
	*	the active mode is the highest sleep mode. This mode will not save power but it's
	*	intended all modules runs in that mode. That means all hardware modules of a special platform
	*	should have a well defined state, when the system runs in active mode. The platformspecific
	*	implementation is tasked to handle that issue. @see PowerManageAble @see PowerModes
	*/
	static void active();

protected: //attributes
	//PowerManageAble* registeredObjects;
	data_types::FifoQueue<PowerManageAble*> queue;

	uint8 useCounts[mcu::NrOfPowerModes];
	powerManagement::Groups_t currentMode;


	static void (* const modes[mcu::NrOfPowerModes])(); ///< array of functionpointers the the platform dependent sleepfunctions
};

} //end namespace reflex

inline
void reflex::PowerManager::enableGroup(powerManagement::Groups_t groups)
{
	switchMode(powerManagement::PowerGroups(currentMode|groups),powerManagement::NOTHING); //enable groups and disable Nothing;
}

inline
void reflex::PowerManager::disableGroup(powerManagement::Groups_t groups)
{
	switchMode(powerManagement::NOTHING,powerManagement::PowerGroups(groups)); //enable Nothing and disable groups;
}

inline
void reflex::PowerManager::powerDown() const
{
	int i = 0;
	while(!useCounts[i] && i<(mcu::NrOfPowerModes-1)) //look for deepest possible powerdown method
	{
		i++;
	}
	(*modes[i])();	//call powerdown method  interrupts will be enabled inside this method

/*debug issues*/	
	//(*modes[0])(); //only the first powermode. Naturally the most modules are online here
	//_interruptsEnable();
}

#endif
