/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/interrupts/InterruptGuardian.h"

#include "reflex/System.h"

#include "reflex/interrupts/InterruptHandler.h"

using namespace reflex;

/** handle() calls the specific handler, and reserves the
 *	scheduling monitor. If this would not be done here, and
 *	an InterruptHandler enables interrupts in handle method,
 *	it would become possible that scheduling of objects during
 *	handle could be delayed. This would happen if another
 *	InterruptHandler would schedule a higher prioritive activity
 *	than the current running one. So we would have possibly
 *	priority inversion and loss of real-time.
**/
extern "C" void handle(uint8 number)
{
#ifdef EDF_SCHEDULING
	getSystem().scheduler.enterScheduling();
#endif
#ifdef PRIORITY_SCHEDULING
	getSystem().scheduler.enterScheduling();
#endif

	getSystem().guardian.handle(number);

#ifdef EDF_SCHEDULING
	getSystem().scheduler.leaveScheduling();
#endif
#ifdef PRIORITY_SCHEDULING
	getSystem().scheduler.leaveScheduling();
#endif
}

/*void InterruptGuardian::disableAll(InterruptHandler::Property except)
{
	for(int i=0; i<MAX_HANDLERS; i++){
		if(handlers[i] != 0){
			handlers[i]->disable(except);
		}
	}
}

void InterruptGuardian::enableAll()
{
	for(int i=0; i<MAX_HANDLERS; i++){
		if(handlers[i] != 0){
			handlers[i]->enable();
		}
	}
}
*/
