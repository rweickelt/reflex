#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/System.h"

using namespace reflex;

PowerManageAble::PowerManageAble()
	: enabled(0),isSecondary(true)
	,deepestAllowedSleepMode(mcu::ACTIVE) //set the higest available sleepmode by default
	,groups(powerManagement::NOTHING)
{
}

PowerManageAble::PowerManageAble(const PowerManageAble::Priority isSecondary)
	: enabled(0),isSecondary(isSecondary)
	,deepestAllowedSleepMode(mcu::ACTIVE) //set the higest available sleepmode by default
	,groups(powerManagement::NOTHING)
{
#ifndef OMNETPP
	getSystem().powerManager.registerObject(this);
#else
#warning getSystem().powerManager.registerObject(this); is omitted while using OMNeTPP!
#endif
}

void PowerManageAble::switchOn()
{
	getSystem().powerManager.enableObject(this);
}

void PowerManageAble::switchOff()
{
	getSystem().powerManager.disableObject(this);
}
