/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/scheduling/EDFActivity.h"
#include "reflex/System.h"

using namespace reflex;

EDFActivity::EDFActivity()
{
    status = IDLE;
    rescheduleCount = 0;
    locked = false;
    next = 0;
}

void EDFActivity::trigger()
{
	getSystem().scheduler.schedule(this);
}

void EDFActivity::lock()
{
	locked = true;
	getSystem().scheduler.lockCount++; //notify scheduler
}

void EDFActivity::unlock()
{
	getSystem().scheduler.unlock(this); //invoke scheduler for proper unlock
}

void EDFActivity::setDeadline()
{
	deadline = getSystem().clock.getSystemTime() + responseTime;
}

bool EDFActivity::lessEqual(EDFActivity& right)
{
	//overflow aware calculation
	Time systemTime = getSystem().clock.getSystemTime();
	Time leftOffset = this->deadline - systemTime ;
	Time rightOffset = right.deadline - systemTime ;
	return ( leftOffset <= rightOffset );
}


