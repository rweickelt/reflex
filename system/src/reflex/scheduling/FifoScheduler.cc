/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/FifoScheduler.h"
#include "reflex/scheduling/FifoActivity.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/System.h"
#include "reflex/powerManagement/PowerManager.h"

using namespace reflex;

//the endless loop of the system
void FifoScheduler::start()
{
	lockCount = 0;
	while(1){
		while(!queue.front()){
			//first = first->dummy();  //prevents from wrong optimization of some compilers

			getSystem().powerManager.powerDown(); //sleep when idle

			_interruptsDisable();
		}

        dispatch();
	}
}

//dispatching of the first element in the readyList
void FifoScheduler::dispatch()
{
	FifoActivity* current = queue.dequeue();
	_interruptsEnable();	    // interrupts are enabled during execution
	current->run();
	_interruptsDisable();

	current->rescheduleCount--;
	if((current->rescheduleCount) && (!current->locked)){
		//reenqueue
		queue.enqueue(current);
	}else{
		current->status = FifoActivity::IDLE;
	}
}



void FifoScheduler::schedule(FifoActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
    InterruptLock lock;

	if((!act->rescheduleCount) && (!act->locked)){
		act->status = FifoActivity::SCHEDULED;
		queue.enqueue(act);
	}
	act->rescheduleCount++;

}

void FifoScheduler::unlock(FifoActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if((act->status == FifoActivity::IDLE) && (act->rescheduleCount)){
		act->status = FifoActivity::SCHEDULED;
		queue.enqueue(act);
	}
}


