/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/PriorityActivity.h"
#include "reflex/System.h"

using namespace reflex;

PriorityActivity::PriorityActivity()
{
    status = IDLE;
    next = 0;
    rescheduleCount = 0;
    locked = false;
    priority = 50;
}

void PriorityActivity::lock()
{
	locked = true;
	getSystem().scheduler.lockCount++;
}

void PriorityActivity::unlock()
{

	getSystem().scheduler.unlock(this);
}

void PriorityActivity::trigger()
{
	getSystem().scheduler.schedule(this);
}

