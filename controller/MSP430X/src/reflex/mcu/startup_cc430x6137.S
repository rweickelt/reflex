/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 **/


WDTCTL=0x0150 + 0x0C;

.macro MAKE_WEAK name
        .weak __\name
        .set __\name, \name
.endm
        MAKE_WEAK WDTCTL



.section .init3, "ax", @progbits
.weak __low_level_init
.func __low_level_init
__low_level_init:
        mov #0x5a80, &__WDTCTL
.endfunc

	/**
	 * We do not want to use the vectors from the crt430x*.o.
	 * That's why we use another name and
	 * put it into the Linkerscript
	 */
.section .vectors, "ax", @progbits
	.irp	i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44
		.word _invalid_int
	.endr
	.irp	i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
		.word __wrapper_\i ;
	.endr
	.word _reset_vector__; POR, ext. Reset, Watchdog (_reset_vector__)

