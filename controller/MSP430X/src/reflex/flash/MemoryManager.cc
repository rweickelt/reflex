#include "reflex/flash/MemoryManager.h"


#include "reflex/debug/Assert.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"
#include "NodeConfiguration.h"

void* operator new(size_t size, reflex::MemoryManager& mm) throw ()
{
	return mm.allocRam(size, 0);
}

void operator delete(void* addr, reflex::MemoryManager& mm) throw ()
{
	return mm.freeRam(addr);
}

using namespace reflex;
using namespace msp430x;
using namespace flash;


extern char __bss_end__[];
extern char __text_end__[];
extern char __endof_flash__[];
extern char __beginof_files__[];
extern char __endof_files__[];

caddr_t const MemoryManager::BEGINOF_RAM = (caddr_t const) MM_BEGINOF_RAM;
caddr_t const MemoryManager::ENDOF_RAM = (caddr_t const) MM_ENDOF_RAM;

MemoryManager::MemoryManager()
{
	// init for RAM section table
	Section* ram_section_table = (Section*) __bss_end__;
	ram_section_table->size = sizeof(Section);
	ram_section_table->next = 0;
}

/**
 * allocRAM uses a first fit algorithm.  Every section is represented
 * by a struct "Section". Memory leaks are empty sections by definition.
 * The last section has null as next-pointer and is never empty.
 * If a special addr is wanted, allocRAM tries to reserve a section with this address.
 *
 */
caddr_t MemoryManager::allocRam(uint16 needed_space, const caddr_t addr)
{
	Section* section = (Section*) __bss_end__;

	// search for a leak
	while (section->next != 0)
	{
		if (addr != 0)
		{
			if (addr < ((caddr_t) section + section->size + sizeof(Section)))
			{
				return 0;
			}
			if ((caddr_t) addr + needed_space <= (caddr_t) section->next)
			{
				break;
			}
			else
			{
				section = section->next;
				continue;
			}
		}

		// normal case: enough room to insert a a new section
		if ((caddr_t) section + section->size + needed_space + sizeof(Section)
				<= (caddr_t) section->next)
		{
			break;
		}

		// special case: current leak is an empty section
		if ((section->size == sizeof(Section)) && ((caddr_t) section
				+ section->size + needed_space <= (caddr_t) section->next))
		{
			break;
		}
		section = section->next;
	}

	// insert or add a new section
	Section* new_section = 0;
	if (addr != 0)
	{
		new_section = (Section*) (addr - sizeof(Section));
	}
	else
	{
		new_section = (Section*) ((caddr_t) section + section->size);
	}

	// assure, that reserved stack area is not overwritten
	if ((caddr_t) new_section + sizeof(Section) + needed_space > ENDOF_RAM
			- MAX_STACKSIZE)
	{
		return 0;
	}

	// section is empty, so MemoryManager can simply overwrite it
	if (section->size == sizeof(Section))
	{
		section->size = needed_space + sizeof(Section);
		return (caddr_t) section + sizeof(Section);;
	}
	else
	{
		new_section->size = needed_space + sizeof(Section);
		new_section->next = section->next;
		section->next = new_section;
		return (caddr_t) new_section + sizeof(Section);;
	}

}

void MemoryManager::freeRam(const void* addr)
{
	if ((addr == 0) || ((caddr_t) addr > ENDOF_RAM))
	{
		return;
	}
	Section* section = (Section*) ((caddr_t) addr - sizeof(Section));
	section->size = sizeof(Section);
}

/*
 * Data should be written to the ROM so it is nessessary to check if
 * the rom is empty (0xff). If it is so, it can be used.
 */
caddr_t MemoryManager::allocRom(size_t size, const caddr_t addr)
{
 	unsigned char* targetSpace = (unsigned char*) addr;
 	for (size_t offset = 0; offset < size; offset++)
 	  {
 	    if (*(targetSpace + offset) != 0xff)
 	      {
 		return 0;
 	      }
	  }

	return addr;
}

caddr_t MemoryManager::alloc(uint16 needed_space, const caddr_t addr)
{

	if (addr < __beginof_files__)
	{
		return allocRam(needed_space, addr);
	}
	return allocRom(needed_space, addr);

}

void MemoryManager::freeRom(const void* addr, uint16 size)
{
 	caddr_t begin = (caddr_t) ((uint16) addr & ~(SIZEOF_SEGMENT - 1));
 	caddr_t end = (caddr_t) (((uint16) addr) + size);
 	for (caddr_t offset = begin; offset < end; offset += SIZEOF_SEGMENT)
 	{
 		erase(offset);
 	}
}

void MemoryManager::erase(char* addr)
{
  _interruptsDisable();
  //align to full segment
    Registers()->FCTL3 = 0x00;  //unlock flash
    Registers()->FCTL1 = ERASE; //set to segment erase
    *addr = 0;		//dummy write
    Registers()->FCTL1 = 0x00; //clr erase bit
    Registers()->FCTL3 = LOCK; // lock flash

  _interruptsEnable();
  return;

}

bool MemoryManager::writeByte(char* addr, char value)
{
  //if value is initial value after erase do nothing
  //this prevents from multiple writing of 0xffff
  if(*addr == value){
    return true;
  }
  _interruptsDisable();

  Registers()->FCTL3 = 0x00; //clear Lock
  Registers()->FCTL1 = WRT; // set write bit
  *addr = value;
  Registers()->FCTL1 = 0x00;  //clr write bit
  Registers()->FCTL3 = LOCK; // Lock again
  _interruptsEnable();
  return ( *addr == value);
}

/*
 * Destination can be ROM or RAM.
 * When the destination is in ROM, data is overwritten, but no erase is done
 * automatically.
 */
bool MemoryManager::memcopy(const void* source, const void* destination,
		uint16 size, bool destination_in_flash)
{
	char* src = (char*) source;
	char* dst = (char*) destination;
	// when dest space lays within RAM
	if ((dst >= BEGINOF_RAM && dst + size <= ENDOF_RAM)
			&& (destination_in_flash == false))
	{
		for (size_t pos = 0; pos < size; pos++)
		{
			dst[pos] = src[pos];
		}
		return true;
	}
	// when dest space lays within ROM
	else if ((dst >= __text_end__ && dst + size <= __endof_flash__)
			|| (destination_in_flash == true))
	{
		for (size_t offset = 0; offset < size; offset++)
		{
			if (!writeByte(dst + offset, src[offset]))
			{
				// copy all from actual position until end but maximum until section end of section to ram
				// copy current section to RAM
				// overwrite all from actual position until end with 0xff
				// copy everything back
				caddr_t beginof_segment = (caddr_t) (offset & ~(SIZEOF_SEGMENT - 1));
				char* temporary[SIZEOF_SEGMENT];
				memcopy(beginof_segment, temporary, SIZEOF_SEGMENT);
				fill(temporary + offset, size - offset, 0xff);
				erase(beginof_segment);
				memcopy(temporary, beginof_segment, SIZEOF_SEGMENT);
			}
		}
		return true;
	}
	return false;
}


bool MemoryManager::isEqual(const void* source, const void* destination, uint16 size)
{
	char* src = (char*) source;
	char* dst = (char*) destination;

	for (size_t offset = 0; offset < size; offset++)
	  {
	    if (*(dst + offset) != src[offset])
	      {
		return false;
	      }
	  }
	return true;
}


void MemoryManager::resetRam()
{
	Section* sec = (Section*) __bss_end__;
	sec->size = sizeof(Section);
	sec->next = 0;
}

void MemoryManager::fill(const void* start, size_t size, uint8 value)
{
	//fill is only desired for RAM
	if (((caddr_t) start < BEGINOF_RAM) || ((caddr_t) start > ENDOF_RAM))
	  {
	    return;
	  }
 	char* src = (char*) start;
       
	for (size_t offset = 0; offset < size; offset++)
	  {
	    (*(src + offset) = value);
	  }
}

