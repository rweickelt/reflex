#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		 Carsten Schulze
# */

ASM_SOURCES_CONTROLLER += \
	mcu/startup.S \
        mcu/startup_$(DEVICE).S \
	mcu/power.S \
	interrupts/interruptFunctions.S \


CC_SOURCES_CONTROLLER += \
	compiler/gcc_virtual.cc \
	interrupts/MachineInterruptGuardian.cc \
	powerManagement/ControllerPowerManagement.cc \
	memory/memcpy.cc \
	wdt/WDT_A.cc \
	ucs/UCS.cc \
	pmm/PMM.cc \
	timer/Timer0_A5.cc \
	rtc/RTC_A.cc \
	adc/ADC12_A.cc 
	
# msp430 uses virtualized timer facility
CC_SOURCES_LIB += \
	timer/VirtualTimer.cc \
	timer/VirtualizedTimer.cc \
	memory/memset.cc
