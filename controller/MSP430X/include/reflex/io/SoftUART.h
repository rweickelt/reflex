/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	SoftUART
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	A software UART implementation for MSP430X using
 *			hardware timer interrupts for accurate timing.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_SOFTUART_H
#define REFLEX_MSP430X_SOFTUART_H

#include "reflex/interrupts/InterruptFunctor.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/scheduling/Activity.h"

namespace reflex {
namespace msp430x {

/*! \brief Software UART with 9600 baud using a hardware timer
 * We are using a Timer_A with assumed 8 MHz input frequency.
 *
 * The component uses an InterruptFunctor for the Timer_A
 * CCR0 interrupt for TX bit timing. This one is secondary
 * (self) managed.
 * Enabling the SoftUART through the PowerManageAble enables
 * the receive interrupt operation.
 */
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
class SoftUART :
	public PowerManageAble, ///< PM features
	public Sink1<uint8>, ///< TX character input
	public Activity ///< Workaround: asynchronous tx_ready notification
{
private:
	enum BitmasksAndConstants {
		TX = 0x01 << TXPIN, ///< bitmask for transmit pin
		RX = 0x01 << RXPIN, ///< bitmask for receive pin
		BIT_TIMING = 104, ///< clock ticks between bits
		START_BIT_TIMING = 156, ///< 1.5xBIT_TIMING for RX start
		INTER_CHAR_TIMING = 5000 ///< time between chars
	};

	Sink0 *tx_ready; ///< notified after successful transmit
	Sink1<uint8> *rx_output; ///< received characters go here
	uint16 tx_buf, rx_buf; ///< buffers for characters

public:
	//! Configure timer for UART
	SoftUART();

	//! set output for received characters
	inline void set_out_rx(Sink1<uint8> *rx) {
		this->rx_output = rx;
	}

	//! set output for successful asynchronous transmit notification
	inline void set_out_tx_ready(Sink0 *tx_ready) {
		this->tx_ready = tx_ready;
	}

	//! acquire input for character transmit
	inline Sink1<uint8> *get_in_tx() {
		return this;
	}

	/** assign character to send
	 * This method puts the character on the wire.
	 * Finished transmit will be signaled through the
	 * tx_ready event, after which more characters
	 * may follow.
	 * @param ch, character to send
	 */
	void assign(uint8 ch);

	//! PowerManager enable RX CCR1 capture interrupt
	void enable();
	//! PowerManager disable RX CCR1 capture interrupt
	void disable();

	//! handle Timer_A CCR0 interrupt for TX bit timing
	void handle_tx_timer();
	//! PowerManager enable TX bit timing CCR0 compare interrupt
	void enable_tx_timer();
	//! PowerManager disable TX bit timing CCR0 compare interrupt
	void disable_tx_timer();

	//! Activity run, notify the tx_ready output
	void run();

private:
	//! functor for TX bit timing interrupt
	InterruptFunctor<SoftUART, &SoftUART::handle_tx_timer,
			&SoftUART::enable_tx_timer,
			&SoftUART::disable_tx_timer> tx_bit_timing;
};

} // msp430x
} // reflex

/* workaround for template class inclusion */
#include "SoftUART.cc"

#endif // REFLEX_MSP430X_SOFTUART_H
