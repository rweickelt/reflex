/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */

/* we include the .cc in the .h file (template class workaround)
* This is also the reason why the .cc is under the "include" and
* not the "src" directory (so it is found in the include path).
* However we include the .h file here, too so the IDEs aren't confused.
*/
#include "I2C_Master.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {
namespace msp430x {


/* constructor */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
I2C_Master<TPort, CLKPIN, DATAPIN>::I2C_Master()
{
    busy = false;
}

/* i2c_command
 * issue bus control command
 * @param addr, slave address
 * @param cmd, bus command
 * @return bool, true if ack received (only CHECK_ACK), false else
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
bool I2C_Master<TPort, CLKPIN, DATAPIN>::i2c_command(uint8 addr, devices::i2c_control_t cmd)
{
    switch (cmd)
    {
    case devices::START_READ:
    case devices::START_WRITE:
        if (!busy) {
            generate_start();
        } else {
            generate_restart();
        }
        // LSB in cmd byte is 0 for write, 1 for read
        i2c_send((addr << 0x01) | (uint8)(cmd & 0x01));
        break;
    case devices::STOP:
        generate_stop();
        break;
    case devices::CHECK_ACK:
        return recv_ack();
    default: // others and illegal combinations are ignored
        break;
    }

    return false;
}

/**
 * delay
 * just a little spinning for clear signals
 */
inline void delay() {
    asm("   nop");
}

/* generate_start
 * put START condition on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::generate_start()
{
    // START is a high to low transition on SDA while SCL is high
    TPort()->DIR |= SDA; delay(); // data line is output

    TPort()->OUT |= SCL; delay(); // SCL = 1
    TPort()->OUT &= ~SDA; delay(); // SDA = 0
    TPort()->OUT &= ~SCL; delay(); // SCL = 0

    busy = true;
}

/* generate_restart
 * put REPEATED RESTART condition on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::generate_restart()
{
    // RESTART is a high to low transition on SDA while SCL is high
    TPort()->DIR |= SDA; delay(); // data line is output

    TPort()->OUT &= ~SCL; delay(); // SCL = 0
    TPort()->OUT |= SDA; delay(); // SDA = 1
    TPort()->OUT |= SCL; delay(); // SCL = 1
    TPort()->OUT &= ~SDA; delay(); // SDA = 0
    TPort()->OUT &= ~SCL; delay(); // SCL = 0

    busy = true;
}

/* generate_stop
 * put a STOP signal on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::generate_stop()
{
    // STOP is a low to high transition on SDA while SCL is high
    TPort()->DIR |= SDA; delay(); // data line is output

    TPort()->OUT &= ~SCL; delay(); // SCL = 0
    TPort()->OUT &= ~SDA; delay(); // SDA = 0
    TPort()->OUT |= SCL; delay(); // SCL = 1
    TPort()->OUT |= SDA; delay(); // SDA = 1

    busy = false;
}

/* i2c_send
 * put one byte of data on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::i2c_send(uint8 byte)
{
    uint8 bit;
    TPort()->DIR |= SDA; delay(); // data line is output

    for (bit = 0x80; bit != 0; bit >>= 1) //shift bit for masking
    {
            TPort()->OUT &= ~SCL; // SCL = 0
            // set data line
            if (bit & byte)
                    TPort()->OUT |= SDA;	// SDA = 1
            else
                    TPort()->OUT &= ~SDA;	// SDA = 0

            delay();
            // let the clock tick
            TPort()->OUT |= SCL; delay();	// SCL=1
    }

    TPort()->OUT &= ~SCL; // SCL = 0
    TPort()->DIR &= ~SDA; // data line is input
}

/* i2c_recv
 * receive one byte of data from wire
 * @param addr, slave address for RESTART
 * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
 * @return uint8, received byte
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
uint8 I2C_Master<TPort, CLKPIN, DATAPIN>::i2c_recv(uint8 addr, devices::i2c_control_t finish)
{
    uint8 bit, byte = 0;

    TPort()->DIR &= ~SDA; delay();// data line is input
    TPort()->OUT &= ~SCL; delay(); // SCL = 0;

    for (bit = 0x80; bit != 0; bit >>= 1) // shift bit for masking
    {
            TPort()->OUT |= SCL; delay(); // SCL = 1
            if (TPort()->IN & SDA) {
                byte |= bit; // read bit
            }
            TPort()->OUT &= ~SCL; delay(); // SCL = 0;
    }

    TPort()->DIR |= SDA; delay();// data line is output

    // how to finish transaction (ACK / NACK + STOP/START...)
    switch (finish)
    {
    case devices::ACK:
        send_ack();
        break;
    case devices::STOP:
        send_nack();
        generate_stop();
        break;
    case devices::START_READ:
    case devices::START_WRITE:
        send_nack();
        generate_restart();
        // last control bit is 0 for write, 1 for read
        i2c_send((addr << 0x01) | (uint8)(finish & 0x01));
        break;
    default: // should not happen
        break;
    }

    return byte;
}

/* send_ack
 * put an ACK on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::send_ack()
{
    // ACK is a low to high transition on SCL while SDA is kept low
    TPort()->DIR |= SDA; // data line is output

    TPort()->OUT &= ~SCL; delay(); // SCL = 0
    TPort()->OUT &= ~SDA; delay(); // SDA = 0
    TPort()->OUT |= SCL; delay(); // SCL = 1
    TPort()->OUT &= ~SCL; // SCL = 0
}

/* send_nack
 * put a NACK on wire
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::send_nack()
{
    // NACK is a low to high transition on SCL while SDA is kept high
    TPort()->DIR |= SDA; delay(); // data line is output

    TPort()->OUT &= ~SCL; delay(); // SCL = 0
    TPort()->OUT |= SDA; delay(); // SDA = 1
    TPort()->OUT |= SCL; delay(); // SCL = 1
    TPort()->OUT &= ~SCL; // SCL = 0
}

/* recv_ack
 * try to read an ACK from wire
 * @return true if successful, false otherwise (NACK received)
 */
template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
bool I2C_Master<TPort, CLKPIN, DATAPIN>::recv_ack()
{
    bool result = true;

    TPort()->DIR &= ~SDA; // data line is input

    TPort()->OUT &= ~SCL; delay(); // SCL = 0;
    // we want to read zero on SDA for an ACK
    TPort()->OUT |= SCL; delay(); // SCL = 1;
    // SDA should be low now
    if(TPort()->IN & SDA) {
        result = false;
    }
    TPort()->OUT &= ~SCL; // SCL = 0;

    return result;
}

template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::enable()
{
    // set up port
    TPort()->DIR |= SCL; // clock is an output pin
}

template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
void I2C_Master<TPort, CLKPIN, DATAPIN>::disable()
{
    /* WARNING: THIS DISABLES THE BUS! IT WILL NOT WORK WITH MULTI-MASTER MODE! */
    // pull CLK and SDA high to minimize voltage drain through pull-up resistors
    TPort()->DIR |= SCL + SDA; // clock and data are output pins
    TPort()->OUT |= SCL + SDA; // set both pins high
}

} // ns msp430x
} // ns reflex
