#ifndef MachineDefinitions_h
#define MachineDefinitions_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	none
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Platform specific definitions.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/mcu/msp430x.h"
#include "reflex/types.h"

namespace reflex {

namespace msp430x {


enum PowerModes {
	ACTIVE=0,
	LPM0,
	LPM1,
	LPM2,
	LPM3,
	LPM4,
	LPM5,
	DEEPESTSLEEPMODE=LPM4, //Fixme: dont forget to enable LPM5 when implemented
	NrOfPowerModes=7
};

}} //namespace msp430x, reflex


//commented out
#if 0

namespace SFR {

  /**
   * Used Bitmasks and memory mapped registers for the special function registers
   *
   */
  struct Registers {
    volatile unsigned int  SFRIE1;   ///SFRIE1 at address 0x0100,
    volatile unsigned int  SFRIFG1;  ///SFRIFG1 at address 0x0102,
    volatile unsigned int  SFRRPCR;  ///SFRRPCR at address 0x0104,
  };

  enum SFR {
    WDTIE = 0x0001,
    OFIE  = 0x0002,
    VMAIE = 0x0008,
    NMIIE = 0x0010,
    ACCVIE = 0x0002,
    JMBINIE = 0x0008,

    WDTIFG = 0x0001,
    OFIFG  = 0x0002,
    VMAIFG = 0x0008,
    NMIIFG = 0x0010,
    ACCVIFG = 0x0002,
    JMBINIFG = 0x0008,

    SYSNMI = 0x0001,
    SYSNMIIES = 0x0002,
    SYSRSTUP = 0x0004,
    SYSRSTRE = 0x0008
  };


}

namespace PPM {

  /**
   * Used Bitmasks and memory mapped registers for the PPM registers
   *
   */
  struct Registers {
    volatile unsigned int  PPMMCTL0;   ///FCTL1 at address 0x0120,
    volatile unsigned int  PPMMCTL1;   ///FCTL1 at address 0x0122,
    volatile unsigned int  SVSMHCTL;   ///FCTL1 at address 0x0124,
    volatile unsigned int  SVSMLCTL;   ///FCTL1 at address 0x0126,
	uint8 : 32;
    volatile unsigned int  PMMIFG;  ///FCTL3 at address 0x012C,
    volatile unsigned int  PMMIE;  ///FCTL4 at address 0x012E,
    volatile unsigned int  PM5CTL0;  ///FCTL4 at address 0x0130,
  };

  enum PPM {
    //PMMCTL0
    PMMCOREVx = 0x0003,
    PMMCOREV0 = 0x0000,
    PMMCOREV1 = 0x0001,
    PMMCOREV2 = 0x0002,
    PMMCOREV3 = 0x0003,
    PMMSWBOR = 0x0004,
    PMMSWPOR = 0x0008,
    PMMREGOFF = 0x0010,
    PMMHPMRE = 0x0080,
    PMMPW = 0xA500,
    //PMMCTL1
    PMMREFMD = 0x0001,
    PMMCMD00 = 0x0030,
    PMMCMDx = 0x0030,
    PMMCMD01 = 0x0000,
    PMMCMD10 = 0x0010,
    PMMCMD11 = 0x0020,
    //SVSMHCTL
    SVSMHRRL = 0x0007, //???????
    SVSMHDLYST = 0x0008,
    SVSHMD = 0x0010,
    SVSMHEVM = 0x0040,
    SVSMHACE = 0x0080,
    SVSHRVL = 0x0300, //???
    SVSHE = 0x0400,
    SVSHFP = 0x0800,
    SVMHOVPE = 0x1000,
    SVMHE = 0x4000,
    SVMHFP = 0x8000,
    //SVSMLCTL
    SVSMLRRL = 0x0007, //???????
    SVSMLDLYST = 0x0008,
    SVSLMD = 0x0010,
    SVSMLEVM = 0x0040,
    SVSMLACE = 0x0080,
    SVSLRVL = 0x0300, //???
    SVSLE = 0x0400,
    SVSLFP = 0x0800,
    SVMLOVPE = 0x1000,
    SVMLE = 0x4000,
    SVMLFP = 0x8000,
    //PMMIFG
    SVSMLDLYIFG = 0x0001,
    SVMLIFG = 0x0002,
    SVMLVLRIFG = 0x0004,
    SVSMHDLYIFG = 0x0010,
    SVMHIFG = 0x0020,
    SVMHVLRIFG = 0x0040,
    PMMBORIFG = 0x0100,
    PMMRSTIFG = 0x0200,
    PMMPORIFG = 0x0400,
    SVSHIFG = 0x1000,
    SVSLIFG = 0x2000,
    PMMLPM5IFG = 0x800,
    //PMMRIE
    SVSMLDLYIE = 0x0001,
    SVMLIE = 0x0002,
    SVMLVLRIE = 0x0004,
    SVSMHDLYIE = 0x0010,
    SVMHIE = 0x0020,
    SVMHVLRIE = 0x0040,
    SVSLPE = 0x0100,
    SVMLVLRPE = 0x0200,
    SVSHPE = 0x1000,
    SVMHVLRPE = 0x2000,
    //PM5CTL0
    LOCKIO = 0x0001
  };
}




namespace FLASH {

  /**
   * Used Bitmasks and memory mapped registers for the flash registers
   *
   */
  struct Registers {
    volatile unsigned int  FCTL1;   ///FCTL1 at address 0x0140,
	uint8 :16;
    volatile unsigned int  FCTL3;  ///FCTL3 at address 0x0144,
    volatile unsigned int  FCTL4;  ///FCTL4 at address 0x0146,
  };

  enum FLASH {
    //FCTL1
    ERASE = 0x0002,
    MERAS = 0x0004,
    SWRT  = 0x0020,
    WRT  = 0x0040,
    BLKWRT = 0x0080,
    FWRKEY = 0xA500,
    //FCTl3
    BUSY = 0x0001,
    KEYV = 0x0002,
    ACCVIFG = 0x0004,
    WAIT  = 0x0008,
    LOCK  = 0x0010,
    LOCKA = 0x0040,
    FWKEY = 0xA500,
    //FCTl4
    VPE = 0x0001,
    MRG0  = 0x0010,
    MRG1  = 0x0020,
    LOCKINFO = 0x0080,
    FWKEY = 0xA500,
  };


}




namespace CRC16 {

  /**
   * Used Bitmasks and memory mapped registers for the flash registers
   *
   */
  struct Registers {
    volatile unsigned int  CRC16DI;   ///CRC16DI at address 0x0150,
  uint8 :16;
    volatile unsigned int  CRC16INIRES;  ///CRC16INIRES at address 0x0154,

  };

}


namespace RAM {

  /**
   * Used Bitmasks and memory mapped registers for the ram conrtroll register
   *
   */
  struct Registers {
    volatile unsigned int  RCCTL0;   ///RCCTL0 at address 0x0158,
  };

  enum RAM {
    RCRSyOFF = 0x000F, //?
    RCRS7OFF = 0x0080,
    RCKEY = 0x5A00
  };


}


namespace WDT_A {

  /**
   * Used Bitmasks and memory mapped registers for the watchdog registers
   *
   */
  struct Registers {
    volatile unsigned int  WDTCTL;   ///WDTCTL at address 0x015C,
  };

  enum WDT_A {
    WDTIS = 0x0007,
    WDTIS000 = 0x0000,
    WDTIS001 = 0x0001,
    WDTIS010 = 0x0002,
    WDTIS011 = 0x0003,
    WDTIS100 = 0x0004,
    WDTIS101 = 0x0005,
    WDTIS110 = 0x0006,
    WDTIS111 = 0x0007,
    WDTCNTL = 0x0008,
    WDTTMSEL = 0x0010,
    WDTSSELx = 0x0060,
    WDTSSEL00 = 0x0000,
    WDTSSEL01 = 0x0020,
    WDTSSEL10 = 0x0040,
    WDTSSEL11 = 0x0060,
    WDTHOLD = 0x0080,
    WDTPW = 0x5A
  };
}

namespace UCS {

  /**
   * Used Bitmasks and memory mapped registers for the unifief clock system registers
   *
   */
  struct Registers {
    volatile unsigned int  UCSCTL0;   ///UCSCTL0 at address 0x0160,
    volatile unsigned int  UCSCTL1;   ///UCSCTL1 at address 0x0162,
    volatile unsigned int  UCSCTL2;   ///UCSCTL2 at address 0x0164,
    volatile unsigned int  UCSCTL3;   ///UCSCTL3 at address 0x0166,
    volatile unsigned int  UCSCTL4;   ///UCSCTL4 at address 0x0168,
    volatile unsigned int  UCSCTL5;   ///UCSCTL5 at address 0x016A,
    volatile unsigned int  UCSCTL6;   ///UCSCTL6 at address 0x016C,
    volatile unsigned int  UCSCTL7;   ///UCSCTL7 at address 0x016E,
    volatile unsigned int  UCSCTL8;   ///UCSCTL8 at address 0x0170,

  };

  enum UCS {
    //UCSCTL0
    MODx = 0x00F80,
    DCOx = 0x1F00,
    //UCSCTL1
    DISMOD = 0x0001,
    DCORSELx = 0x0070,
    DCORSEL0 = 0x0000,
    DCORSEL1 = 0x0010,
    DCORSEL2 = 0x0020,
    DCORSEL3 = 0x0030,
    DCORSEL4 = 0x0040,
    DCORSEL5 = 0x0050,
    DCORSEL6 = 0x0060,
    DCORSEL7 = 0x0070,
    //UCSCTL2
    FLLNx = 0x03FF,
    FLLDx = 0x7000,
    FLLD1 = 0x0000,
    FLLD2 = 0x1000,
    FLLD4 = 0x2000,
    FLLD8 = 0x3000,
    FLLD16 = 0x4000,
    FLLD32 = 0x5000,
    //UCSCTL3
    FLLREFDIVx = 0x0007,
    FLLREFDIV1 = 0x0000,
    FLLREFDIV2 = 0x0001,
    FLLREFDIV4 = 0x0002,
    FLLREFDIV8 = 0x0003,
    FLLREFDIV12 = 0x0004,
    FLLREFDIV16 = 0x0005,
    SELREFx = 0x0070,
    SELREF000 = 0x0000,
    SELREF010 = 0x0020,
    SELREF000 = 0x0030,
    //UCSCTL4
    SELMx = 0x0007,
    SELMxt1 = 0x0000,
    SELMvlo = 0x0001,
    SELMrefo = 0x0002,
    SELMdco = 0x0003,
    SELMdcodiv = 0x0004,
    SELMxt2 = 0x0005,
    SELSx = 0x0070,
    SELSxt1 = 0x0000,
    SELSvlo = 0x0010,
    SELSrefo = 0x0020,
    SELSdco = 0x0030,
    SELSdcodiv = 0x0040,
    SELSxt2 = 0x0050,
    SELAx = 0x0007,
    SELAxt1 = 0x0000,
    SELAvlo = 0x0100,
    SELArefo = 0x0200,
    SELAdco = 0x0300,
    SELAdcodiv = 0x0400,
    SELAxt2 = 0x0500,
    //UCSCTL5
    DIVMx = 0x0007,
    DIVM1 = 0x0000,
    DIVM2 = 0x0001,
    DIVM4 = 0x0002,
    DIVM8 = 0x0003,
    DIVM16 = 0x0004,
    DIVM32 = 0x0005,
    DIVSx = 0x0070,
    DIVS1 = 0x0000,
    DIVS2 = 0x0010,
    DIVS4 = 0x0020,
    DIVS8 = 0x0030,
    DIVS16 = 0x0040,
    DIVS32 = 0x0050,
    DIVAx = 0x0700,
    DIVA1 = 0x0000,
    DIVA2 = 0x0100,
    DIVA4 = 0x0200,
    DIVA8 = 0x0300,
    DIVA16 = 0x0400,
    DIVA32 = 0x0500,
    DIVPAx = 0x7000,
    DIVPA1 = 0x0000,
    DIVPA2 = 0x1000,
    DIVPA4 = 0x2000,
    DIVPA8 = 0x3000,
    DIVPA16 = 0x4000,
    DIVPA32 = 0x5000,
    //UCSCTL6
    XT1OFF = 0x0001,
    SMCLKOFF = 0x0002,
    XCAPx = 0x000C,
    XCAP0 = 0x0000,
    XCAP1 = 0x0004,
    XCAP2 = 0x0008,
    XCAP3 = 0x000C,
    XT1BYPASS = 0x0010,
    XTS = 0x0020,
    XT1DRIVEx = 0x00C0,
    XT1DRIVE0 = 0x0000,
    XT1DRIVE1 = 0x0040,
    XT1DRIVE2 = 0x0080,
    XT1DRIVE3 = 0x00C0,
    XT2OFF = 0x0100,
    //UCSCTL7
    DCOFFG = 0x0001,
    XT1LFOFFG = 0x0002,
    XT1HFOFFG = 0x0004,
    XT2OFFG = 0x0008,
    //UCSCTL78
    UCSCTL7 = 0x0001,
    MCLKREQEN = 0x0002,
    SMCLKREQEN = 0x0004,
    MODOSCREQEN = 0x0008
  };


}



namespace SHAREDREF {

  /**
   * Used Bitmasks and memory mapped registers for the watchdog registers
   *
   */
  struct Registers {
    volatile unsigned int  REFCTL0;   ///REFCTL0 at address 0x01B0,
  };

  enum SHAREDREF {
    REFON = 0x0001,
    REFOUT = 0x0002,
    REFTCOFF = 0x0008,
    REFVSELx = 0x0030,
    REFVSEL15 = 0x0000,
    REFVSEL20 = 0x0010,
    REFVSEL25 = 0x0020,
    REFMSTR = 0x0080,
    REFGENACT = 0x0100,
    REFBGACT = 0x0200,
    REFGENBUSY = 0x0400,
    BGMODE = 0x0800
  };


}

namespace PORTMAPPING {

  /**
   * Used Bitmasks and memory mapped registers for the watchdog registers
   *
   */
  struct Registers {
    volatile unsigned int  PMAPPWD;   ///PMAPPWD at address 0x1C0,
    volatile unsigned int  PMAPCTL; ///PMAPCTL at address 0x1C2
  unit8 :32;
    volatile unsigned char  P10; /// at address 0x1C8
    volatile unsigned char  P11; /// at address 0x1C9
    volatile unsigned char  P12; /// at address 0x1CA
    volatile unsigned char  P13; /// at address 0x1CB
    volatile unsigned char  P14; /// at address 0x1CC
    volatile unsigned char  P15; /// at address 0x1CD
    volatile unsigned char  P16; /// at address 0x1CE
    volatile unsigned char  P17; /// at address 0x1CF
    volatile unsigned char  P20; /// at address 0x1D0
    volatile unsigned char  P21; /// at address 0x1D1
    volatile unsigned char  P22; /// at address 0x1D2
    volatile unsigned char  P23; /// at address 0x1D3
    volatile unsigned char  P24; /// at address 0x1D4
    volatile unsigned char  P25; /// at address 0x1D5
    volatile unsigned char  P26; /// at address 0x1D6
    volatile unsigned char  P27; /// at address 0x1D7
    volatile unsigned char  P30; /// at address 0x1D8
    volatile unsigned char  P31; /// at address 0x1D9
    volatile unsigned char  P32; /// at address 0x1DA
    volatile unsigned char  P33; /// at address 0x1DB
    volatile unsigned char  P34; /// at address 0x1DC
    volatile unsigned char  P35; /// at address 0x1DD
    volatile unsigned char  P36; /// at address 0x1DE
    volatile unsigned char  P37; /// at address 0x1DF



  };

  enum PORTMAPPING {
    //PMAPPWD
    PMAPPWD = 0x2D52,
    PMAPLOCKED = 0x0001,
    //PMAPCTL
    PMAPRECFG = 0x0002,
    PM_NONE = 0x00,
    PM_CBOUT0 = 0x01,
    PM_TA0CLK = 0x01,
    PM_CBOUT1 = 0x02,
    PM_TA1CLK =0x02,
    PM_ACLK = 0x03,
    PM_MCLK = 0x04,
    PM_SMCLK = 0x05,
    PM_RTCCLK = 0x06,
    PM_ADC12CLK =0x07,
    PM_DMAE0 = 0x07,
    PM_SVMOUT = 0x08,
    PM_TA0CCR0A = 0x09,
    PM_TA0CCR1A = 0x0A,
    PM_TA0CCR2A = 0x0B,
    PM_TA0CCR3A = 0x0C,
    PM_TA0CCR4A = 0x0D,
    PM_TA1CCR0A = 0x0E,
    PM_TA1CCR1A = 0x0F,
    PM_TA1CCR2A = 0x10,
    PM_UCA0RXD = 0x11,
    PM_UCA0SOMI = 0x11,
    PM_UCA0TXD = 0x12,
    PM_UCA0SIMO = 0x12,
    PM_UCA0CLK = 0x13,
    PM_UCB0STE = 0x13,
    PM_UCB0SOMI = 0x14,
    PM_UCB0SCL = 0x14,
    PM_UCB0SIMO = 0x15,
    PM_UCB0SDA = 0x15,
    PM_UCB0CLK = 0x16,
    PM_UCA0STE = 0x16,
    PM_RFGDO0 = 0x17,
    PM_RFGDO1 = 0x18,
    PM_RFGDO2 = 0x19,
    PM_ANALOG = 0xff


  };


}



namespace PORTS {


  template<bool>
    struct PortRegisters;

  /**	Registers for a port which has interrupts.
   */
  template<>
    struct PortRegisters<true>
    {
      data_types::ReadOnly<volatile uint8> IN; //read only
    uint8 :8;
      //volatile const uint8 IN; //read only
      volatile uint8 OUT;
    uint8 :8;
      volatile uint8 DIR;
    uint8 :8;
      volatile uint8 REN;
    unit8 :8;
      volatile uint8 DS;
    uint8 :8;
      volatile uint8 SEL;
    uint8 :8;

      volatile uint8 IES;
    uint8 :8;
      volatile uint8 IE;
    uint8 :8;
      volatile uint8 IFG;


      PortRegisters(){}
    };

  /**Registers for a port which has no interrupts.
   */
  template<>
    struct PortRegisters<false>
    {
      data_types::ReadOnly<volatile uint8> IN; //read only
    uint8 :8;
      //volatile const uint8 IN; //read only
      volatile uint8 OUT;
    uint8 :8;
      volatile uint8 DIR;
    uint8 :8;
      volatile uint8 REN;
    unit8 :8;
      volatile uint8 DS;
    uint8 :8;
      volatile uint8 SEL;
    };

}

namespace timer {

  /**
   * Used Bitmasks and memory mapped registers for timer 0/1
   * Can have 7 capture compare registers, on cc430x613x 5/3
   * Timer 0 has 5 capture compare registers
   * Timer 1 has 3 capture compare registers
   * both register maps are identical
   *
   */
  struct Registers {
    volatile unsigned int TAxCTL;        // TACTL at address 0x0340,
    volatile unsigned int  TAxCCTL0;     ///TACCTL0 at address 0x0342,
    volatile unsigned int  TAxCCTL1;     ///TACCTL1 at address 0x0344,
    volatile unsigned int  TAxCCTL2;     ///TACCTL2 at address 0x0346,
    volatile unsigned int  TAxCCTL3;     ///TACCTL3 at address 0x0348,
    volatile unsigned int  TAxCCTL4;     ///TACCTL4 at address 0x034A,
    // uint8 :32;
    volatile unsigned int  TAxCCTL5;     ///TACCTL5 at address 0x034C,
    volatile unsigned int  TAxCCTL6;     ///TACCTL6 at address 0x034E,

    volatile unsigned int  TAxR;         ///TAR at address 0x0350,
    volatile unsigned int  TAxCCR0;      ///TACCR0 at address 0x0352,
    volatile unsigned int  TAxCCR1;      ///TACCR1 at address 0x0354,
    volatile unsigned int  TAxCCR2;      ///TACCR2 at address 0x0356
    volatile unsigned int  TAxCCR3;      ///TACCR3 at address 0x0358,
    volatile unsigned int  TAxCCR4;      ///TACCR4 at address 0x035A
    //uint8 :32;
    volatile unsigned int  TAxCCR5;      ///TACCR5 at address 0x035C,
    volatile unsigned int  TAxCCR6;      ///TACCR6 at address 0x035E

    volatile unsigned int  TAxEX0;       ///TA at address 0x0360
    volatile unsigned int  TAxIV;        ///TAIV at address 0x036E

  };


  enum TIMER{
    //TAxCTL
    TASSELx     = 0x0300, ///clock source select
    TASSEL00    = 0x0000, ///TACLK as source
    TASSEL01    = 0x0100, ///ACLK as source
    TASSEL10    = 0x0200, ///SMCLK as source
    TASSEL11    = 0x0300, ///INCLK as source
    IDx         = 0x00c0, ///Divider of clock
    ID00	= 0x0000, ///Factor 1
    ID01        = 0x0040, ///Factor 2
    ID10        = 0x0080, ///Factor 4
    ID11        = 0x00c0, ///Factor 8
    MCx         = 0x0030, ///Count mode
    MC00	= 0x0000, ///stop
    MC01        = 0x0010, ///Up mode: the timer counts up to TACCR0
    MC10        = 0x0020, ///continuous mode
    MC11        = 0x0030, ///Up&Down mode
    TACLR       = 0x0004, ///resets divider and TAR
    TAIE        = 0x0002, ///Timer A interrupt enable (overflow)
    TAIFG       = 0x0001, ///Timer A interrupt flag (overflow)
    //TAIV
    TAIVx               = 0x000e,
    TACCR1_CCIFG	= 0x0002,
    TACCR2_CCIFG	= 0x0004,
    TACCR3_CCIFG	= 0x0006,
    TACCR4_CCIFG	= 0x0008,
    TACCR5_CCIFG	= 0x000A,
    TAIFG_TACTL		= 0x000E, //?????????????????????
    //TAxCCTLx
    CMx		=	0xc000,
    CM01	=	0x4000,
    CM10	=	0x8000,
    CM11	=	0xc000,
    CCISx	=	0x3000,
    CCIS01	=	0x1000,
    CCIS10	=	0x2000,
    CCIS11	=	0x3000,
    SCS		=	0x0800,
    SCCI	=	0x0400,
    CAP		=	0x0100, /// capture mode for TACCTLx
    OUTMODx	=	0x00e0,
    OUTMOD001 	=	0x0020,
    OUTMOD010 	=	0x0040,
    OUTMOD011 	=	0x0060,
    OUTMOD100 	=	0x0080,
    OUTMOD101	=	0x00a0,
    OUTMOD110	=	0x00c0,
    OUTMOD111	=	0x00e0,
    CCIE	=	0x0010,
    CCI		=	0x0008,
    OUT		=	0x0004,
    COV		=	0x0002,
    CCIFG	=	0x0001,
    //TAXEX0
    IDEXx       = 0x0007,
    IDEX111     = 0x0007,
    IDEX110     = 0x0006,
    IDEX101     = 0x0005,
    IDEX100     = 0x0004,
    IDEX011     = 0x0003,
    IDEX010     = 0x0002,
    IDEX001     = 0x0001,
    IDEX000     = 0x0000
  };

} //timer



namespace RTC_A {

  /**
   * Used Bitmasks and memory mapped registers for realtime clock
   *
   */
  struct Registers {
    volatile unsigned char RTCCTL0;        // at address 0x04A0,
    volatile unsigned char RTCCTL1;        // at address 0x04A1,
    volatile unsigned char RTCCTL2;        // at address 0x04A2,
    volatile unsigned char RTCCTL3;        // at address 0x04A3,
  unit8: 32;
    volatile unsigned int RTCPS0CTL;        // at address 0x04A8,
    volatile unsigned int RTCPS1CTL;        // at address 0x04AA,
    volatile unsigned char RTCPS0;        // at address 0x04AC,
    volatile unsigned char RTCPS1;        // at address 0x04AD,
    volatile unsigned int RTCIV;        // at address 0x04AE,
    volatile unsigned char RTCSEC;        // at address 0x04B0,
    volatile unsigned char RTCMIN;        // at address 0x04B1,
    volatile unsigned char RTCHOUR;        // at address 0x04B2,
    volatile unsigned char RTCDOW;        // at address 0x04B3,
    volatile unsigned char RTCDAY;        // at address 0x04B4,
    volatile unsigned char RTCMON;        // at address 0x04B5,
    volatile unsigned char RTCYEARL;        // at address 0x04B6,
    volatile unsigned char RTCYEARH;        // at address 0x04B7,
    volatile unsigned char RTCAMIN;        // at address 0x04B8,
    volatile unsigned char RTCAHOUR;        // at address 0x04B9,
    volatile unsigned char RTCADOW;        // at address 0x04BA,
    volatile unsigned char RTCADAY;        // at address 0x04BB,

  };




  enum RTC_A{
    //RTCCTL0
    RTCRDYIFG     = 0x01, ///clock source select
    RTCAIFG  = 0x02,
    RTCTEVIFG = 0x04,
    RTCRDYIE = 0x10,
    RTCAIE = 0x20,
    RTCTEVIE = 0x40,
    //RTCCTL1
    RTCTEVx = 0x03,
    RTCTEVov8 = 0x00, //counter mode overflow events
    RTCTEVov16 = 0x01,
    RTCTEVov24 = 0x02,
    RTCTEVov32 = 0x03,
    RTCTEVminute = 0x00, //calendar mode overflow events
    RTCTEVhour = 0x01,
    RTCTEV0 = 0x02,
    RTCTEV12 = 0x03,
    RTCSSELx = 0x0C,
    RTCSSEL_ACLK = 0x00,
    RTCSSEL_SMCLK = 0x01,
    RTCSSEL_RT1PS = 0x0C,
    RTCRDY = 0x10,
    RTCMODE = 0x20,
    RTCHOLD = 0x40,
    RTCBCD = 0x80,
    //RTCCTL2
    RTCCAL = 0x3F, //callibration
    RTCCALS = 0x80,
    //RTCCTL3
    RTCCALFx = 0x03, // callibration output frequency
    RTCCALFno = 0x00,
    RTCCALF512 = 0x01,
    RTCCALF256 = 0x02,
    RTCCALF1 = 0x03,
    //RTCPS0CTL
    RT0PSIFG = 0x0001,
    RT0PSIE = 0x0002,
    RT0IPx = 0x001C,
    RT0IP2 = 0x0000,
    RT0IP4 = 0x0004,
    RT0IP8 = 0x0008,
    RT0IP16 = 0x000C,
    RT0IP32 = 0x0010,
    RT0IP64 = 0x0014,
    RT0IP128 = 0x0018,
    RT0IP256 = 0x001C,
    RT0PSHOLD = 0x0100,
    RT0PSDIVx = 0x3800,
    RT0PSDIV2 = 0x0000,
    RT0PSDIV4 = 0x0800,
    RT0PSDIV8 = 0x1000,
    RT0PSDIV16 = 0x1800,
    RT0PSDIV32 = 0x2000,
    RT0PSDIV64 = 0x2800,
    RT0PSDIV128 = 0x3000,
    RT0PSDIV256 = 0x3800,
    RT0SSEL = 0x4000,
    //RTCPS1CTL
    RT1PSIFG = 0x0001,
    RT1PSIE = 0x0002,
    RT1IPx = 0x001C,
    RT1IP2 = 0x0000,
    RT1IP4 = 0x0004,
    RT1IP8 = 0x0008,
    RT1IP16 = 0x000C,
    RT1IP32 = 0x0010,
    RT1IP64 = 0x0014,
    RT1IP128 = 0x0018,
    RT1IP256 = 0x001C,
    RT1PSHOLD = 0x0100,
    RT1PSDIVx = 0x3800,
    RT1PSDIV2 = 0x0000,
    RT1PSDIV4 = 0x0800,
    RT1PSDIV8 = 0x1000,
    RT1PSDIV16 = 0x1800,
    RT1PSDIV32 = 0x2000,
    RT1PSDIV64 = 0x2800,
    RT1PSDIV128 = 0x3000,
    RT1PSDIV256 = 0x3800,
    RT1SSEL = 0x4000
  };

} //RTC_A







namespace USCI0 {

  /**
   * Used Bitmasks and memory mapped registers for Universal Serial Communication Interface
   *
   */
  struct Registers {
    volatile unsigned char UCA0CTL1;       // at address 0x05C0,
    volatile unsigned char UCA0CTL0;       // at address 0x05C1,
  unit8 :32;
    volatile unsigned char UCA0BR0;        // at address 0x05C6,
    volatile unsigned char UCA0BR1;        // at address 0x05C7,
    volatile unsigned char UCA0MCTL;       // at address 0x05C8
  unit8 :8;
    volatile unsigned char UCA0STAT;       // at address 0x05CA
  unit8 :8;
    volatile unsigned char UCA0RXBUF;      // at address 0x05CC
  unit8 :8;
    volatile unsigned char UCA0TXBUF;      // at address 0x05CE
  unit8 :8;
    volatile unsigned char UCA0ABCTL;      // at address 0x05D0
  unit8 :8;
    volatile unsigned char UCA0IRTCTL;     // at address 0x05D2
    volatile unsigned char UCA0IRRCTL;     // at address 0x05D3
  uint9 :64;
    volatile unsigned char UCA0IE;         // at address 0x05DC
    volatile unsigned char UCA0IFG;        // at address 0x05DE
    volatile unsigned char UCA0IV;         // at address 0x05E0
    volatile unsigned char UCB0CTL0;       // at address 0x05E0,
    volatile unsigned char UCB0CTL1;       // at address 0x05E1,
  unit8 :32;
    volatile unsigned char UCB0BR0;        // at address 0x05E6,
    volatile unsigned char UCB0BR1;        // at address 0x05E7,
    volatile unsigned char UCB0I2CIE;      // at address 0x05E8 ?? non existing in user guide!
  unit8 :8;
    volatile unsigned char UCB0STAT;       // at address 0x05EA
  unit8 :8;
    volatile unsigned char UCB0RXBUF;      // at address 0x05EC
  unit8 :8;
    volatile unsigned char UCB0TXBUF;      // at address 0x05EE
  unit8 :8;
    volatile unsigned char UCB0I2COA;      // at address 0x05F0
  unit8 :8;
    volatile unsigned char UCB0I2CSA;      // at address 0x05F2
  uint8 :64;
    volatile unsigned char UCB0IE;         // at address 0x05FC
    volatile unsigned char UCB0IFG;        // at address 0x05FD
    volatile unsigned char UCB0IV;         // at address 0x05FE
  };




  enum USCI0{
    //=====UART mode
    //UCAxCTL0
    UCSYNC = 0x01,
    UCMODEx = 0x06,
    UCMODE0 = 0x00,
    UCMODE1 = 0x02,
    UCMODE2 = 0x04,
    UCMODE3 = 0x06,
    UCSPB = 0x08,
    UC7BIT = 0x10,
    UCMSB = 0x20,
    UCPAR = 0x40,
    UCPEN = 0x80,
    //UCAxCTL1
    UCSWRST = 0x01,
    UCTXBRK = 0x02,
    UCTXADDR = 0x04,
    UCDORM = 0x08,
    UCBRKIE= 0x10,
    UCRXEIE= 0x20,
    UCSSELx = 0xC0,
    UCSSELuclk = 0x00,
    UCSSELaclk = 0x40,
    UCSSELsmclk = 0x80,
    //UCAxMCTL
    UCOS16 = 0x01,
    UCBRSx = 0x0E,
    UCBRFx = 0xF0,
    //UCAxSTAT
    UCBUSY = 0x01,
    UCIDLE_UCADDR = 0x02,
    UCRXERR = 0x04,
    UCBRK = 0x08,
    UCPE = 0x10,
    UCOE = 0x20,
    UCFE = 0x40,
    UCLISTEN = 0x80,
    //UCAxIRTCTL
    UCIREN = 0x01,
    UCIRTXCLK = 0x02,
    UCIRTXPLx = 0xFC,
    //UCAxIRRCTL
    UCIRRXFE = 0x01,
    UCIRRXPL = 0x02,
    UCIRRXFLx = 0xFC,
    //UCAxABCTL
    UCABDEN = 0x01,
    UCBTOE = 0x04,
    UCSTOE = 0x08,
    UCDELIMx = 0x30,
    UCDELIM1bit = 0x00,
    UCDELIM2bit = 0x10,
    UCDELIM3bit = 0x20,
    UCDELIM4bit = 0x30,
    //UCAxIE
    UCRXIE = 0x01,
    UCTXIE = 0x02,
    //UCAxIFG
    UCRXIFG = 0x01,
    UCTXIFG = 0x02,
    //UCAxIV
    UCIVx = 0x0006,
    UCIVdr = 0x0002,
    UCIVbe = 0x0004,
    //=====SPI mode
    dummy = 0x0004
  };

} //USCI0

#endif

#endif
