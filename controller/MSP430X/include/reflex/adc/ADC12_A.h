/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_ADC12_A_H
#define REFLEX_MSP430X_ADC12_A_H

#include "reflex/adc/Registers.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/data_types/Partial.h"

namespace reflex {
namespace msp430x {

//! representation of the ADC12 hardware
/*!	a component which provides a sink, which consumes requests which trigger
	the sampling and conversing operation.
	\note the implementation is not finished yet. Currently only one channel is
	used for all triggered measurements. That means one event on the input can
	override a previous event, if the component couln't get the first eventdata.
 */
class ADC12_A
	: public PowerManageAble
{
public:
	typedef adc::Registers Registers;
	//! which values  should be measured
	enum InputChannel {
		  InCh_A0 = adc::INCH_0
		, InCh_A1 = adc::INCH_1
		, InCh_A2 = adc::INCH_2
		, InCh_A3 = adc::INCH_3
		, InCh_A4 = adc::INCH_4
		, InCh_A5 = adc::INCH_5
		, InCh_A6 = adc::INCH_6
		, InCh_A7 = adc::INCH_7
		, InCh_VeRef	= adc::INCH_8
		, InCh_VRefDIV = adc::INCH_9
		, InCh_Temp	= adc::INCH_10
		, InCh_AVccDIV	= adc::INCH_11
		, InCh_A12	= adc::INCH_12
		, InCh_A13	= adc::INCH_13
		, InCh_A14	= adc::INCH_14
		, InCh_A15	= adc::INCH_15
	};
	//! the reference against that the measurement takes place
	enum InputRef {
		 Ref_AVcc_AVss = adc::SREF_0
		,Ref_Vref_AVss = adc::SREF_1
		,Ref_VEref_AVss1 = adc::SREF_2
		,Ref_VEref_AVss2 = adc::SREF_3
		,Ref_Vcc_VErefDIV = adc::SREF_4
		,Ref_Vref_VErefDIV = adc::SREF_5
		,Ref_VEref_VErefDIV1 = adc::SREF_6
		,Ref_VEref_VErefDIV2 = adc::SREF_7

	};
	//! channel on which measuring should take place
	enum Channel {
		 CH0 = 0
		,CH1 = 1
		,CH2 = 2
		,CH3 = 3
		,CH4 = 4
		,CH5 = 5
		,CH6 = 6
		,CH7 = 7
		,CH8 = 8
		,CH9 = 9
		,CH10 = 10
		,CH11 = 11
		,CH12 = 12
		,CH13 = 13
		,CH14 = 14
		,CH15 = 15
	};


	enum InternalRefernce {
	  Ref15 = 0
	  ,Ref20 = 1
	  ,Ref25 = 2
	};

public:
	//! the request for initiating a conversion.
	/*!	An object of this type holds a value for InputChannel,
		which values of interrest, should be measured. Additionally, a reference
		has to be specified with that the input should be measured against.

	  \see	InputChannel
	  \see	InputRef
	  \see	Channel
	*/
	class Request
		: public data_types::Partial<uint16>
	{
		friend class ADC12_A;
	public:
		explicit Request() { this->value=0;}
		Request(const InputRef& ref, const InputChannel& inCh, const InternalRefernce& internalref, const Channel& ch= CH0 ) { value=(internalref<<12)|(ch<<8)|ref|inCh;}

		void setRef(const InputRef& ref) {this->low.high=ref;} //high bits within the lower 8bit value
		void setInCh(const InputChannel& inCh) {this->low.low=inCh;} //low bits within lower the 8bit value
		void setChannel(const Channel& ch){this->high.low=ch;}
		void setInternelRef(const InternalRefernce& internalref){this->high.high=internalref;}

		uint8 getCH() const {return this->high.low; }
		uint8 getCTL() const {return this->low; }
		uint8 getinternalRef() const {return this->high.high; }
	protected:
	};
	class BatteryRequ : public Request { BatteryRequ():Request(Ref_Vref_AVss,InCh_AVccDIV,Ref20) {} };
	class TempRequ : public Request { TempRequ():Request(Ref_Vref_AVss,InCh_Temp,Ref20) {} };

protected:
	//! implements initiating a measure
	class Measure : public Activity, public SingleValue1<Request> {
	public:
		Measure(ADC12_A&);
	protected:
		virtual void run();
	protected:
		ADC12_A& adc;
	};
	//! finalize a previous initiated measure operation and collects data
	class Collector : public Sink0, public Activity {
		friend class ADC12_A;
	public:
		Collector(ADC12_A& adc):adc(adc),output(0){}
	protected:
		//! called when conversion is done
		virtual void notify();
		//! called to handle a finished conversion and propagate the results
		virtual void run();
	protected:
		ADC12_A& adc; //! access to the adc
		Request request; //! the current request
		//Sink1<uint16>* output; //! outgoing eventchannel, where the results are written to
		Sink2<uint8,uint16>* output;
	};
	friend class Measure;
	friend class Collector;
public:
	//! initialize and held in state off
	ADC12_A();
	//! also power off the module
	~ADC12_A();
	//! register some event sink
	//void init(Sink1<uint16>* output) {collector.output=output;}
	void init(Sink2<uint8,uint16>* output) {collector.output=output;}

protected:
	//! enable the module
	virtual void enable();
	//! disable the module
	virtual void disable();

public:
	Measure	input;//! input which triggers the measurement

protected:
	Registers::IVDispatcher ivDispatcher; //! interrupt dispatching obj
	Collector collector;	//! handles finished conversions and value propagation
};

}} // msp430, reflex

#endif // ADC_H


/* /\* */
/*  *	REFLEX - Real-time Event FLow EXecutive */
/*  * */
/*  *	A lightweight operating system for deeply embedded systems. */
/*  * */
/*  * */
/*  * */
/*  * */
/*  * */
/*  *    This file is part of REFLEX. */
/*  * */
/*  *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and  */
/*  *    Operating Systems. All rights reserved. */
/*  *     */
/*  *    Redistribution and use in source and binary forms, with or without  */
/*  *    modification, are permitted provided that the following conditions */
/*  *    are met: */
/*  *     */
/*  *       1. Redistributions of source code must retain the above copyright  */
/*  *          notice, this list of conditions and the following disclaimer. */
/*  *  */
/*  *       2. Redistributions in binary form must reproduce the above copyright */
/*  *          notice, this list of conditions and the following disclaimer in */
/*  *          the documentation and/or other materials provided with the  */
/*  *          distribution. */
/*  *  */
/*  *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED  */
/*  *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED  */
/*  *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF  */
/*  *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN */
/*  *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND  */
/*  *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, */
/*  *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT */
/*  *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, */
/*  *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY */
/*  *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT  */
/*  *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF */
/*  *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
/*  *  */
/*  *    The views and conclusions contained in the software and documentation  */
/*  *    are those of the authors and should not be interpreted as representing  */
/*  *    official policies, either expressed or implied, of BTU Cottbus,  */
/*  *    Department for Distributed Systems and Operating Systems. */
/*  * */
/*  **\/ */

/* #ifndef REFLEX_MSP430X_ADC12_A_H */
/* #define REFLEX_MSP430X_ADC12_A_H */

/* #include "reflex/adc/Registers.h" */
/* #include "reflex/powerManagement/PowerManageAble.h" */
/* #include "reflex/sinks/SingleValue.h" */
/* #include "reflex/scheduling/Activity.h" */
/* #include "reflex/data_types/Partial.h" */

/* namespace reflex { */
/* namespace msp430x { */

/* //! representation of the ADC12 hardware */
/* /\*!	a component which provides a sink, which consumes requests which trigger */
/* 	the sampling and conversing operation. */
/* 	\note the implementation is not finished yet. Currently only one channel is */
/* 	used for all triggered measurements. That means one event on the input can */
/* 	override a previous event, if the component couln't get the first eventdata. */
/*  *\/ */
/* class ADC12_A */
/* 	: public PowerManageAble */
/* { */
/* public: */
/* 	typedef adc::Registers Registers; */
/* 	//! which values  should be measured */
/* 	enum InputChannel { */
/* 		  InCh_A0 = adc::INCH_0 */
/* 		, InCh_A1 = adc::INCH_1 */
/* 		, InCh_A2 = adc::INCH_2 */
/* 		, InCh_A3 = adc::INCH_3 */
/* 		, InCh_A4 = adc::INCH_4 */
/* 		, InCh_A5 = adc::INCH_5 */
/* 		, InCh_A6 = adc::INCH_6 */
/* 		, InCh_A7 = adc::INCH_7 */
/* 		, InCh_VeRef	= adc::INCH_8 */
/* 		, InCh_VRefDIV = adc::INCH_9 */
/* 		, InCh_Temp	= adc::INCH_10 */
/* 		, InCh_AVccDIV	= adc::INCH_11 */
/* 		, InCh_A12	= adc::INCH_12 */
/* 		, InCh_A13	= adc::INCH_13 */
/* 		, InCh_A14	= adc::INCH_14 */
/* 		, InCh_A15	= adc::INCH_15 */
/* 	}; */
/* 	//! the reference against that the measurement takes place */
/* 	enum InputRef { */
/* 		 Ref_AVcc_AVss = adc::SREF_0 */
/* 		,Ref_Vref_AVss = adc::SREF_1 */
/* 		,Ref_VEref_AVss1 = adc::SREF_2 */
/* 		,Ref_VEref_AVss2 = adc::SREF_3 */
/* 		,Ref_Vcc_VErefDIV = adc::SREF_4 */
/* 		,Ref_Vref_VErefDIV = adc::SREF_5 */
/* 		,Ref_VEref_VErefDIV1 = adc::SREF_6 */
/* 		,Ref_VEref_VErefDIV2 = adc::SREF_7 */

/* 	}; */
/* 	//! channel on which measuring should take place */
/* 	enum Channel { */
/* 		 CH0 = 0 */
/* 		,CH1 = 1 */
/* 		,CH2 = 2 */
/* 		,CH3 = 3 */
/* 		,CH4 = 4 */
/* 		,CH5 = 5 */
/* 		,CH6 = 6 */
/* 		,CH7 = 7 */
/* 		,CH8 = 8 */
/* 		,CH9 = 9 */
/* 		,CH10 = 10 */
/* 		,CH11 = 11 */
/* 		,CH12 = 12 */
/* 		,CH13 = 13 */
/* 		,CH14 = 14 */
/* 		,CH15 = 15 */
/* 	}; */


/* 	enum Refernce { */
/* 	  Ref15 = 0x0000 */
/* 	  ,Ref20 = 0x0010 */
/* 	  ,Ref25 = 0x0020 */
/* 	}; */

/* public: */
/* 	//! the request for initiating a conversion. */
/* 	/\*!	An object of this type holds a value for InputChannel, */
/* 		which values of interrest, should be measured. Additionally, a reference */
/* 		has to be specified with that the input should be measured against. */

/* 	  \see	InputChannel */
/* 	  \see	InputRef */
/* 	  \see	Channel */
/* 	*\/ */
/* 	class Request */
/* 		: public data_types::Partial<uint16> */
/* 	{ */
/* 		friend class ADC12_A; */
/* 	public: */
/* 		explicit Request() { this->value=0;} */
/* 		Request(const InputRef& ref, const InputChannel& inCh, const Channel& ch= CH0 ) { value=(ch<<8)|ref|inCh;} */

/* 		void setRef(const InputRef& ref) {this->low.high=ref;} //high bits within the lower 8bit value */
/* 		void setInCh(const InputChannel& inCh) {this->low.low=inCh;} //low bits within lower the 8bit value */
/* 		void setChannel(const Channel& ch){this->high=ch;} */

/* 		uint8 getCH() const {return this->high; } */
/* 		uint8 getCTL() const {return this->low; } */
/* 	protected: */
/* 	}; */
/* 	class BatteryRequ : public Request { BatteryRequ():Request(Ref_Vref_AVss,InCh_AVccDIV) {} }; */
/* 	class TempRequ : public Request { TempRequ():Request(Ref_Vref_AVss,InCh_Temp) {} }; */

/* protected: */
/* 	//! implements initiating a measure */
/* 	class Measure : public Activity, public SingleValue1<Request> { */
/* 	public: */
/* 		Measure(ADC12_A&); */
/* 	protected: */
/* 		virtual void run(); */
/* 	protected: */
/* 		ADC12_A& adc; */
/* 	}; */
/* 	//! finalize a previous initiated measure operation and collects data */
/* 	class Collector : public Sink0, public Activity { */
/* 		friend class ADC12_A; */
/* 	public: */
/* 		Collector(ADC12_A& adc):adc(adc),output(0){} */
/* 	protected: */
/* 		//! called when conversion is done */
/* 		virtual void notify(); */
/* 		//! called to handle a finished conversion and propagate the results */
/* 		virtual void run(); */
/* 	protected: */
/* 		ADC12_A& adc; //! access to the adc */
/* 		Request request; //! the current request */
/* 		//Sink1<uint16>* output; //! outgoing eventchannel, where the results are written to */
/* 		Sink2<uint8,uint16>* output; */
/* 	}; */
/* 	friend class Measure; */
/* 	friend class Collector; */
/* public: */
/* 	//! initialize and held in state off */
/* 	ADC12_A(); */
/* 	//! also power off the module */
/* 	~ADC12_A(); */
/* 	//! register some event sink */
/* 	//void init(Sink1<uint16>* output) {collector.output=output;} */
/* 	void init(Sink2<uint8,uint16>* output) {collector.output=output;} */

/* protected: */
/* 	//! enable the module */
/* 	virtual void enable(); */
/* 	//! disable the module */
/* 	virtual void disable(); */

/* public: */
/* 	Measure	input;//! input which triggers the measurement */

/* protected: */
/* 	Registers::IVDispatcher ivDispatcher; //! interrupt dispatching obj */
/* 	Collector collector;	//! handles finished conversions and value propagation */
/* }; */

/* }} // msp430, reflex */

/* #endif // ADC_H */
