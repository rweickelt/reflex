/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 USCI_I2C_Master
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Bus master driver for i2c using hardware USCI module
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_USCI_I2C_MASTER_H
#define REFLEX_USCI_I2C_MASTER_H

#include "reflex/usci/Registers.h"
#include "reflex/io/I2C.h"

/** USCI_I2C_Master
  *
  * This is an implementation for the master bus driver of an i2c bus.
  * Drivers for i2c devices (connected to the bus) hook up to this
  * class to communicate with the devices.
  *
  * You should not use this class directly but rather derive your slave driver
  * from devices::I2C_Device, which can be connected to any class implementing
  * the devices::I2C_Master interface. Connection with the correct master will
  * be done in the NodeConfiguration of the application.
  *
  * The driver does not use interrupts but polls on the interrupt flags.
  * This way we maximize throughput. It is not effective to signal interrupts
  * on every received/sent byte. We rather offer a synchronous bytewise interface
  * with fine grained bus control.
  *
  * Currently only 7-bit addressing is supported.
  * Multi-Master mode is currently NOT supported.
  */
namespace reflex{
namespace msp430x {
namespace usci {

enum Constants {
    I2C_MASTER_DEFAULT_ADDRESS = 0x002c // default "own" address
};

/* USCI_I2C_Master
 * The i2c master bus driver
 */
template<typename Registers>
class USCI_I2C_Master : public devices::I2C_Master
{
private:
    uint16 masterAddress; // I2C Address of USCI module

public:

    /* constructor
     * @param masterAddress I2C address of master (7 bit only)
     */
    USCI_I2C_Master(uint16 masterAddress = I2C_MASTER_DEFAULT_ADDRESS);

    /* i2c_command
     * issue bus control command
     * @param addr, slave address
     * @param cmd, bus command
     * @return bool, true if ack received (only READ_ACK), false else
     */
    virtual bool i2c_command(uint8 addr, devices::i2c_control_t cmd);

    /* i2c_send
     * send a byte to the device
     * @param byte, the byte to send
     */
    virtual void i2c_send(uint8 byte);

    /* i2c_recv
     * receive a byte from the device
     * @param addr, slave address for RESTART
     * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
     * @return uint8, received byte
     */
    virtual uint8 i2c_recv(uint8 addr, devices::i2c_control_t finish);

    /* features of PowerManageAble */
    void enable();
    void disable();

private:

    /* start
     * start or restart reading or writing
     * @param addr, slave address
     * @param cmd, bus command - should be START_READ or START_WRITE
     */
    void start(uint8 addr, devices::i2c_control_t cmd);

    /* stop
     * transmit a STOP signal
     */
    void stop();
};

} // ns usci
} // ns msp430x
} // ns reflex

/* workaround for template class inclusion */
#include "USCI_I2C_Master.cc"

#endif // REFLEX_USCI_I2C_MASTER_H
