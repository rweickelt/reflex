/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */

/* we include the .cc in the .h file (template class workaround)
* This is also the reason why the .cc is under the "include" and
* not the "src" directory (so it is found in the include path).
* However we include the .h file here, too so the IDEs aren't confused.
*/
#include "reflex/usci/USCI_SPI.h"
#include "reflex/pmc/Registers.h"
#include "reflex/io/Ports.h"

namespace reflex {
namespace msp430x {
namespace usci {

template<typename Registers>
USCI_SPI<Registers>::USCI_SPI() :
	PowerManageAble(PRIMARY)
{
	input.init(this);
	sender = 0;
	receiver = 0;
	current = 0;

	//set deepest allowed sleep mode
	this->setSleepMode(mcu::LPM3);
	Port1()->map(5, pmc::UCA0SOMI); // Map UCA0SOMI output to P1.5
	Port1()->map(6, pmc::UCA0SIMO); // Map UCA0SIMO output to P1.6
	Port1()->map(7, pmc::UCA0CLK); // Map UCA0CLK output to P1.7
	//configure pins for SPI
	Port1()->SEL |= (PIN_CLK | PIN_MISO | PIN_MOSI);
	Port1()->DIR |= (PIN_CLK | PIN_MOSI);
	Port1()->DIR &= ~PIN_MISO;

	//Configure CSN pin
	PortJ()->DIR |= PIN_CSN;
	PortJ()->OUT |= PIN_CSN;

	Registers()->UCIFG = 0;
	Registers()->UCCTL1 |= UCSWRST; ///< Reset State

	Registers()->UCCTL0 |= ( UCMSB | UCMST | UCSYNC| UCCKPH);

	Registers()->UCCTL1 |= UCSSELaclk;
	//Registers()->UCCTL1 |= UCSSELsmclk; //should be sm clock, but its easier to test it with aclock
}

template<typename Registers>
void USCI_SPI<Registers>::init(Sink1<Buffer*>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
}

template<typename Registers>
void USCI_SPI<Registers>::enable()
{
	Registers()->UCCTL1 &= ~UCSWRST; ///< Release State -> usci switch on
	PortJ()->OUT &= ~PIN_CSN;
}

template<typename Registers>
void USCI_SPI<Registers>::disable()
{
	Registers()->UCCTL1 |= UCSWRST; ///< Reset State;
	PortJ()->OUT |= PIN_CSN;
}

template<typename Registers>
void USCI_SPI<Registers>::run()
{
	current = input.get();
	length = current->getLength();
	pos = (char*)current->getStart();

	this->lock(); //lock run because we can handle only one send a time

	do {
		length--;

		uint8 temp = send(*pos);
		*pos++ = temp;
	} while (length);


}

template<typename Registers>
uint8 USCI_SPI<Registers>::send(uint8 ch)
{
	while(Registers()->UCSTAT&UCBUSY);	//hopefully usci isn't busy right now
	Registers()->UCTXBUF = ch;		//send byte
	while(Registers()->UCSTAT&UCBUSY);	//wait for completion
	return (Registers()->UCRXBUF);		//and read byte
}

} // usci
} // msp430x
} // reflex
