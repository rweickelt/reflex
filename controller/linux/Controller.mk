#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Karsten Walther Soeren Hoeckner
# */

ifdef DEVICE
	DEFINED_REFLEX_VARS +=\t"DEVICE ="\t$(DEVICE)\n
else
	MISSING_REFLEX_VARS +=\t"DEVICE :"\t"so far x86"\n
endif

ifdef GENSYS
DEFINED_REFLEX_VARS += \t"GENSYS ="\t$(GENSYS)\n
else
MISSING_REFLEX_VARS += \t"GENSYS:"\t"LINUX, "\t"DARWIN or CYGWIN"\n
endif

EXECUTABLE = $(APPLICATION)
MAIN_TARGET = $(EXECUTABLE)


TOOLPATH ?=
TOOLPREFIX ?= $(TOOLPATH)
# force to use the nasm
ifeq ($(origin AS), default)
	AS = $(TOOLPREFIX)nasm
endif

# check if debugging flags should be set

ifeq ($(DEBUG), 1)
	DEBUG_FLAGS += -g -DDEBUG
endif

ifeq ($(ASSERT), 1)
	DEBUG_FLAGS += -DDO_ASSERT
endif


ifeq ($(GENSYS), LINUX)
	ASFLAGS += -f elf -D$(GENSYS)
endif

ifeq ($(GENSYS), CYGWIN)
	ASFLAGS += -f gnuwin32 -D$(GENSYS)
endif

# because -D is disturbing gcc if nothing is behind
ifdef GENSYS
	GENSYSOPTION = -D$(GENSYS)
endif

CFLAGS += $(DEBUG_FLAGS) $(GENSYSOPTION)  -Wall
CXXFLAGS += $(DEBUG_FLAGS) $(GENSYSOPTION) -O2 -Wall -Wno-non-virtual-dtor
LDFLAGS += -lstdc++

OBJDUMP_FLAGS += -SDx

###################### includes and base sources for the controller ########
INCLUDES += -I$(REFLEXPATH)/controller/$(CONTROLLER)/include
include $(REFLEXPATH)/controller/$(CONTROLLER)/Sources.mk



