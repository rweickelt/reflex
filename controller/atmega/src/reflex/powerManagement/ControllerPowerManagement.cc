#include "reflex/MachineDefinitions.h"
#include "reflex/powerManagement/PowerManager.h"

extern "C" {
	void _idle();
	void _adc_noise();
	void _powerdown();
	void _powersave();
	void _standby();
	void _extstandby();
}

using namespace reflex;

void (* const PowerManager::modes[mcu::NrOfPowerModes])() = {PowerManager::active,_idle,_adc_noise,_powerdown,_powersave,_standby,_extstandby};
