#include "reflex/io/ADConverter.h"
#include "reflex/io/ADChannel.h"

using namespace reflex;
using namespace atmega;

ADConverter::ADConverter() :
	InterruptHandler(Core::INTVEC_ADC, PowerManageAble::SECONDARY)
{
	this->input_request.init(this);
	this->setSleepMode(AdcNoiseReduction);

	this->setReference(ExternalAVCC);
	Registers()->ADCSRA |= Core::ADPS2 | Core::ADPS1 | Core::ADPS0;
}

void ADConverter::run()
{
	// Switch on ADC first, to enable the clock
	if (!this->isEnabled())
	{
		this->switchOn();
	}

	this->lock();
	this->currentChannel = this->input_request.get();
	const uint8 channel = this->currentChannel->channel();

	Registers()->ADCSRB &= ~Core::MUX5; // MUX 5
	Registers()->ADMUX &= ~(Core::MUX2 | Core::MUX1 | Core::MUX0); // MUX 4:0
	Registers()->ADCSRB |= channel & Core::MUX5; // MUX 5
	Registers()->ADMUX |= channel & (Core::MUX2 | Core::MUX1 | Core::MUX0); // MUX 4:0

	// single shot
	Registers()->ADCSRA |= Core::ADSC;
}

void ADConverter::handle()
{
	if (this->currentChannel->out_value)
	{
		uint16 result = Registers()->ADCL;
		result += Registers()->ADCH << 8;
		this->currentChannel->out_value->assign(result);
	}
	//switch converter off if no pending tasks
	if (!input_request.front())
	{
		this->switchOff();
	}
	Registers()->ADCSRA &= ~Core::ADSC;
	this->unlock();
}

void ADConverter::enable()
{
	Registers()->ADCSRA |= Core::ADEN | Core::ADIE;
}

void ADConverter::disable()
{
	Registers()->ADCSRA &= ~(Core::ADEN |Core::ADSC | Core::ADIE);
}

void ADConverter::setReference(ADConverter::Reference reference)
{
	Registers()->ADMUX = (Registers()->ADMUX & ~(Core::REFS1 | Core::REFS0)) | reference;
}
