#ifndef FLASH_H_
#define FLASH_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/MachineDefinitions.h"
namespace reflex
{

namespace atmega
{

/*!
 \ingroup atmega
 \brief Driver to access ATMEGA program memory

 The ATMEGA contains a harvard architecture, which seperates address spaces
 for code and memory. To access the internal FLASH, a sequence of special
 commands is needed.

 This driver offers synchronous read-access to the program memory. Reading a
 single byte needs only a few clock cycles.

 Flash offers only a set of static methods. There is no need to create
 an object instance.

 \sa IN_FLASH, FLASH_PTR, Eeprom

 */
class Flash
{
public:

	/*!
	 Encapsulates a plain array which otherwise could not be used as return type
	 for Flash::read();
	 */
	template<typename T, size_t size> struct Array
	{
		operator T*()
		{
			return &data[0];
		}
		T data[size];
	};

	template<typename T, size_t n> static Array<T, n> read(const T(*addr)[n]);
	template<typename T> inline static T read(T const* addr);

	static void memcopy(void* dest, const void* source, size_t length);
	static uint8 strcopy(char* dest, const char* source);

private:
	Flash();
	Flash(const Flash& other);
	Flash& operator=(const Flash& other);
};


/**
 \ingroup atmega
 \brief Places a variable in flash.

 The ATMEGA contains a harvard architecture, which seperates address spaces
 for code and memory. This means, that even static constants will be placed
 in RAM because the compiler can not determine the real destination of a
 pointer. This is very often not the wanted behaviour, so the programmer
 must emphasize, which location to access.

 This macro moves a static variable into the progmem.data section during
 link-time.

 Use it as follows:
 \code
 struct Storage {
 static uint16 ramData;
 static uint16 flashData;
 }

 uint16 Storage::ramData IN_FLASH = 0xFF;

 void function() {
 uint16 data = Flash::read(&Storage::ramData);
 }
 \endcode

 \sa READ_FLASH, FLASH_PTR, Flash

 */
#define IN_FLASH __attribute__((__progmem__))

/**
 \ingroup atmega
 \brief Places a variable in flash string during compile-time and emits code
 to read it back during run-time.

 \code
 print(READ_FLASH("Hello World")); // print reads from RAM memory
 \endcode

 \sa IN_FLASH

 */
#define READ_FLASH(s) (__extension__({static const char __c[] IN_FLASH = {s};\
		char __string[sizeof(__c)];\
		reflex::mcu::Flash::memcopy(__string, __c,sizeof(__c));\
		&__string[0];}))

/**
 \ingroup atmega
 \brief Stores a variable in flash and returns a pointer to it during compile-time.

 Unlike #READ_FLASH, this macro does not emit code to read the data.

 \code
 // Example 1
 char* string = FLASH_PTR("Hello World");
 char c = Flash::read(&string[0]);

 // Example 2 print
 print_p(FLASH_PTR("Hello World")); // print_p reads from program memory
 \endcode

 \sa READ_FLASH, Flash
 */
#define FLASH_PTR(s) (__extension__({static const char __c[] IN_FLASH = (s); &__c[0];}))

//! \cond

namespace atmega
{

//! Loads a char from flash.
#define __LPM_enhanced__(addr)  \
(__extension__({                \
    uint16 __addr16 = (uint16)(addr); \
    uint8 __result;           \
    __asm__                     \
    (                           \
        "lpm %0, Z" "\n\t"      \
        : "=r" (__result)       \
        : "z" (__addr16)        \
    );                          \
    __result;                   \
}))

//! Loads a char from flash
#define __ELPM_enhanced__(addr)     \
(__extension__({                    \
    uint32 __addr32 = (uint32)(addr); \
    uint8 __result;               \
    __asm__                         \
    (                               \
        "out %2, %C1" "\n\t"        \
        "movw r30, %1" "\n\t"       \
        "elpm %0, Z+" "\n\t"        \
        : "=r" (__result)           \
        : "r" (__addr32),           \
          "I" (_SFR_IO_ADDR(RAMPZ)) \
        : "r30", "r31"              \
    );                              \
    __result;                       \
}))

//! Loads a 16bit value from flash.
#define __LPM_word_enhanced__(addr)         \
(__extension__({                            \
    uint16 __addr16 = (uint16)(addr);   \
    uint16 __result;                      \
    __asm__                                 \
    (                                       \
        "lpm %A0, Z+"   "\n\t"              \
        "lpm %B0, Z"    "\n\t"              \
        : "=r" (__result), "=z" (__addr16)  \
        : "1" (__addr16)                    \
    );                                      \
    __result;                               \
}))

//! Loads a 16bit value from flash.
#define __ELPM_word_enhanced__(addr)    \
(__extension__({                        \
    uint32 __addr32 = (uint32)(addr); \
    uint16 __result;                  \
    __asm__                             \
    (                                   \
        "out %2, %C1"   "\n\t"          \
        "movw r30, %1"  "\n\t"          \
        "elpm %A0, Z+"  "\n\t"          \
        "elpm %B0, Z"   "\n\t"          \
        : "=r" (__result)               \
        : "r" (__addr32),               \
          "I" (_SFR_IO_ADDR(RAMPZ))     \
        : "r30", "r31"                  \
    );                                  \
    __result;                           \
}))

//! Loads a 32bit value from flash.
#define __LPM_dword_enhanced__(addr)        \
(__extension__({                            \
    uint16 __addr16 = (uint16)(addr);   \
    uint32 __result;                      \
    __asm__                                 \
    (                                       \
        "lpm %A0, Z+"   "\n\t"              \
        "lpm %B0, Z+"   "\n\t"              \
        "lpm %C0, Z+"   "\n\t"              \
        "lpm %D0, Z"    "\n\t"              \
        : "=r" (__result), "=z" (__addr16)  \
        : "1" (__addr16)                    \
    );                                      \
    __result;                               \
}))

}

//! \endcond

/*!
 \brief Reads value of type \a T from flash memory address \a addr
 into the RAM and returns it.

 */
template<typename T>
T Flash::read(T const * addr)
{
	T result;
	switch (sizeof(T))
	{
	case 1:
		asm("" : "=r" (result) : "0" __LPM_enhanced__(addr));
		break;
	case 2:
		asm("" : "=r" (result) : "0" __LPM_word_enhanced__(addr));
		break;
	case 4:
		asm("" : "=r" (result) : "0" __LPM_dword_enhanced__(addr));
		break;
	default:
		Flash::memcopy((void*) &result, (void*) addr, sizeof(T));
		break;
	}
	return result;
}

/*!
 \brief Reads an array of type \a T from flash memory address \a addr
 into the RAM and returns it.
 */
template<typename T, size_t size>
Flash::Array<T, size> Flash::read(const T(*addr)[size])
{
	Array<T, size> string;
	for (size_t i = 0; i < 30; i++)
	{
		string.data[i] = Flash::read(&((*addr)[i]));
	}
	return string;
}

}

}

#endif /* FLASH_H */
