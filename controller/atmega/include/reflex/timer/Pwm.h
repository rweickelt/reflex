/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef PWM_H_
#define PWM_H_

#include "reflex/MachineDefinitions.h"
#include "reflex/timer/Timer.h"
#include "reflex/types.h"
#include "reflex/sinks/Sink.h"

namespace reflex
{
namespace atmega {
/*!
 \ingroup atmega
 \brief Driver for the waveform generation unit of Timer on the ATMEGA
 controller family.

 This class configures one of the hardware timers as PWM generator with
 a variable resolution. It is complementary to Timer. Therefore, all basic
 settings have to be configured via a Timer object.
 At the moment, only ::Timer1, ::Timer3, ::Timer4 and ::Timer5 are implemented,
 because they offer very similar features.

 \see TimerNr, TimerChannel

 \todo This driver will undergo a complete API change in the nearer future.

 */
template<TimerNr timerNr, TimerChannel channel>
class Pwm
{
public:
	//! Constructs a PWM driver object, but leaves all registers unchanged.
	Pwm();

	//! Sets the PWM \a resolution in timer-ticks.
	void setResolution(uint16 resolution);

	//! Sets the input capture register to \a ticks.
	void setICRn(uint16 ticks);

	//! Sets the output compare register to \a ticks.
	void setOCRn(uint16 ticks);

	//! Enables the output pin.
	void enable();

	//! Disables the output pin.
	void disable();

private:
	typedef Core::TimerRegisters<timerNr> Register;
};

template<TimerNr timerNr, TimerChannel channel>
Pwm<timerNr, channel>::Pwm()
{

}

template<TimerNr timerNr, TimerChannel channel>
void Pwm<timerNr, channel>::setResolution(uint16 resolution)
{
	this->setICRn(resolution);
}

template<TimerNr timerNr, TimerChannel channel>
void Pwm<timerNr, channel>::setICRn(uint16 ticks)
{
	Register()->ICRnH = ticks >> 8;
	Register()->ICRnL = static_cast<uint8> (ticks) & 0xFF;
}

template<TimerNr timerNr, TimerChannel channel>
void Pwm<timerNr, channel>::setOCRn(uint16 ticks)
{
	switch (channel)
	{
	case ChannelA:
		if ((timerNr != Timer0) && (timerNr != Timer2))
		{
			Register()->OCRnAH = ticks >> 8;
		}
		Register()->OCRnAL = static_cast<uint8> (ticks) & 0xFF;
		break;
	case ChannelB:
		if ((timerNr != Timer0) && (timerNr != Timer2))
		{
			Register()->OCRnBH = ticks >> 8;
		}
		Register()->OCRnBL = static_cast<uint8> (ticks) & 0xFF;
		break;
	case ChannelC:
		if ((timerNr != Timer0) && (timerNr != Timer2))
		{
			Register()->OCRnCH = ticks >> 8;
		}
		Register()->OCRnCL = static_cast<uint8> (ticks) & 0xFF;
		break;
	}

}

template<TimerNr timerNr, TimerChannel channel>
void Pwm<timerNr, channel>::enable()
{
	switch (timerNr)
	{
	case 0:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnA |= static_cast<uint8> (Core::COM0A1);
			break;
		case ChannelB:
			Register()->TCCRnA |= static_cast<uint8> (Core::COM0B1);
			break;
		default:
			// static assert, atmega does not provide output c at timer0
			break;
		}
		break;
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnA |= static_cast<uint8> (Core::COMnA1);
			break;
		case ChannelB:
			Register()->TCCRnA |= static_cast<uint8> (Core::COMnB1);
			break;
		default:
			Register()->TCCRnA |= static_cast<uint8> (Core::COMnC1);
			break;
		}
		break;
	}

}

template<TimerNr timerNr, TimerChannel channel>
void Pwm<timerNr, channel>::disable()
{
	switch (timerNr)
	{
	case 0:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnA &= ~static_cast<uint8> (Core::COM0A1);
			break;
		case ChannelB:
			Register()->TCCRnA &= ~static_cast<uint8> (Core::COM0B1);
			break;
		}
		break;
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnA &= static_cast<uint8> (~(Core::COMnA0 |Core::COMnA1));
			break;
		case ChannelB:
			Register()->TCCRnA &= static_cast<uint8> (~(Core::COMnB0 | Core::COMnB1));
			break;
		case ChannelC:
			Register()->TCCRnA &= static_cast<uint8> (~(Core::COMnC0 | Core::COMnC1));
			break;
		}
		break;
	}
}

}
}

#endif /* PWM_H_ */
