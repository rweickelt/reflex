/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef COMPARE_MATCH_H_
#define COMPARE_MATCH_H_

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/Timer.h"
#include "reflex/types.h"

namespace reflex
{
namespace atmega
{
/*!
 \ingroup atmega
 \brief Handler for timer compare-match interrupts on ATMEGA controllers.

 \todo Add documenation and test cases

 */
template<TimerNr timerNr = Timer0, TimerChannel channel = ChannelA>
class CompareMatch: public InterruptHandler
{
public:
	Sink0* output;

	CompareMatch();
	void setMatchAt(uint16 count);
	void forceOutputCompareMatch();

protected:
	void disable();
	void enable();
	void handle();
private:
	typedef Core::TimerRegisters<timerNr> Register;
};

template<TimerNr timerNr, TimerChannel channel>
CompareMatch<timerNr, channel>::CompareMatch() :
			InterruptHandler(
					(channel == ChannelA) ? Register::COMPA
							: (channel == ChannelB) ? Register::COMPB : Register::COMPC, SECONDARY)
{
	if (timerNr == Timer2)
	{
		this->setSleepMode(PowerSave);
	}
	else
	{
		this->setSleepMode(Idle);
	}
	this->output = 0;
}

template<TimerNr timerNr, TimerChannel channel>
void CompareMatch<timerNr, channel>::enable()
{
	switch (channel)
	{
	case ChannelA:
		Register()->TIMSKn |= Core::OCIEnA;
		break;
	case ChannelB:
		Register()->TIMSKn |= Core::OCIEnB;
		break;
	case ChannelC:
		Register()->TIMSKn |= Core::OCIEnC;
		break;
	}
}

template<TimerNr timerNr, TimerChannel channel>
void CompareMatch<timerNr, channel>::disable()
{
	switch (channel)
	{
	case ChannelA:
		Register()->TIMSKn &= ~Core::OCIEnA;
		break;
	case ChannelB:
		Register()->TIMSKn &= ~Core::OCIEnB;
		break;
	case ChannelC:
		Register()->TIMSKn &= ~Core::OCIEnC;
		break;
	}
}

template<TimerNr timerNr, TimerChannel channel>
void CompareMatch<timerNr, channel>::setMatchAt(uint16 count)
{
	switch (timerNr)
	{
	case Timer1:
	case Timer3:
	case Timer4:
	case Timer5:
		switch (channel)
		{
		case ChannelA:
			Register()->OCRnAH = (count >> 8);
			break;
		case ChannelB:
			Register()->OCRnBH = (count >> 8);
			break;
		case ChannelC:
			Register()->OCRnCH = (count >> 8);
			break;
		}
	case Timer0:
	case Timer2:
		switch (channel)
		{
		case ChannelA:
			Register()->OCRnAL = count;
			Register()->TIFRn |= Core::OCFnA; // TODO: Why? Remove it.
			break;
		case ChannelB:
			Register()->OCRnBL = count;
			Register()->TIFRn |= Core::OCFnB; // TODO: Why? Remove it.
			break;
		case ChannelC:
			Register()->OCRnCL = count;
			Register()->TIFRn |= Core::OCFnC; // TODO: Why? Remove it.
			break;
		}
		break;
	}
}

template<TimerNr timerNr, TimerChannel channel>
void CompareMatch<timerNr, channel>::handle()
{
	if (this->output)
	{
		this->output->notify();
	}
}

template<TimerNr timerNr, TimerChannel channel>
void CompareMatch<timerNr, channel>::forceOutputCompareMatch()
{
	switch (timerNr)
	{
	case Timer0:
	case Timer2:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnB |= Core::FOCnA;
			break;
		case ChannelB:
			Register()->TCCRnB |= Core::FOCnB;
			break;
		case ChannelC:
			Register()->TCCRnB |= Core::FOCnC;
			break;
		}
		break;
	case Timer1:
	case Timer3:
	case Timer4:
	case Timer5:
		switch (channel)
		{
		case ChannelA:
			Register()->TCCRnC |= Core::FOCnA;
			break;
		case ChannelB:
			Register()->TCCRnC |= Core::FOCnB;
			break;
		case ChannelC:
			Register()->TCCRnC |= Core::FOCnC;
			break;
		}
		break;
	}
}
}
}

#endif /* COMPARE_MATCH_H_ */
