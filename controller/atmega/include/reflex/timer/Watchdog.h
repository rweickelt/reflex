#ifndef WATCHDOG_H_
#define WATCHDOG_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses): Watchdog
 *	Revision:	 $Id: Watchdog.h 656 2010-10-28 17:17:31Z PENET\weickelt $
 *	Author:		 Richard Weickelt <richard@weickelt.de>
 *  Created on:  27.10.2010
 *
 *	Description: Driver for the AVR watchdog.
 *
 */
#include "reflex/types.h"
#include "reflex/MachineDefinitions.h"
namespace reflex {

namespace atmega {

/*!
 \ingroup atmega
 \brief Interface for the watchdog timer unit.

 The watchdog timer is an independent timer unit which can generate interrupt
 and reset events. The intention is, to prevent Watchdog  from timing out by
 periodically calling Watchdog::reset() from within the application.
 Once the application hangs, Watchdog will detect this.
 It should be mentioned, that the watchdog timer unit is the only possible
 way to perform hardware resets.

 All methods of Watchdog are static. It is not necessary to create a dedicated
 object.

 Watchdog::reset() should be called from an activity within the event flow,
 but never synchronously by an interrupt handler e.g. timer interrupt.
 Thus it is possible to guarantee a working scheduler.

 When Watchdog has triggered a hardware reset, the watchdog timer stays enabled
 even after the reset. It is then always configured to ::Interval16ms.
 This may lead to endless resets. Therefore, configuring the watchdog timer
 should be the first action during boot time. See the datasheet for detailled
 explanation.

 */
class Watchdog {
public:

	/*!
	 Timeout timings in milliseconds
	 */
	enum Interval {
		Interval16ms = 0,
		Interval32ms = mcu::Core::WDP0,
		Interval64ms = mcu::Core::WDP1,
		Interval125ms = mcu::Core::WDP0 + mcu::Core::WDP1,
		Interval250ms = mcu::Core::WDP2,
		Interval500ms = mcu::Core::WDP2 + mcu::Core::WDP0,
		Interval1000ms = mcu::Core::WDP2 + mcu::Core::WDP1,
		Interval2000ms = mcu::Core::WDP2 + mcu::Core::WDP1 + mcu::Core::WDP0,
		Interval4000ms = mcu::Core::WDP3,
		Interval8000ms = mcu::Core::WDP3 + mcu::Core::WDP0
	};

	/*!
	 Returns the configured interval.
	 */
	static Interval interval();

	/*!
	 Resets the watchdog timer.

	 Do not forget to call this method within the timeout interval.
	 */
	static void reset();

	/*!
	 Sets the timeout interval to \a time, but
	 leaves the timer state untouched.
	 */
	static void setInterval(Interval time);

	/*!
	 Enables the watchdog timer using a previously configured interval.
	 */
	static void start();

	/*!
	 Sets the timeout interval to \a time and enables the watchdog.
	 */
	static void start(Interval time);

	/*!
	 Disables the watchdog and leaves the timeout interval untouched.
	 */
	static void stop();

private:
	static void inline _wdr();
};

/**
 * After a reset caused by the watchdog, we have to disable it immediately.
 */
void initWatchdog();

}

}

#endif /* WATCHDOG_H_ */
