#ifndef Serial_h
#define Serial_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Serial
 *
 *	Authors:		Carsten Schulze, Karsten Walther
 *
 *	Description: Driver for serial interface.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Queue.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptFunctor.h"
#include "reflex/memory/Buffer.h"
#include "reflex/io/SerialRegisters.h"

namespace reflex {

/**
 * This class implements the driver for the serial USART0/1 interface.
 * @author Carsten Schulze, Karsten Walther
 */

class Serial : public Activity
{
protected:
	void enableRX();
	void disableRX();
	void handleRX();

	void enableTX();
	void disableTX();
	void handleTX();

	/** needed cause we have to wait for finishing the transmission before
	 *	we can disable that transmitter
	 */
	void transmitterFinished();

public:
	//conecting the interrupt handle routine with the Fuctions
	//handleTXI() and handleRXI(), which are used in these class
	InterruptFunctor<Serial,&Serial::handleRX,&Serial::enableRX,&Serial::disableRX> rxHandler;
	InterruptFunctor<Serial,&Serial::handleTX,&Serial::enableTX,&Serial::disableTX> txHandler;

protected:
	ActivityFunctor<Serial,&Serial::transmitterFinished> txDisableAct;

public:
	/**
	* Constructor for Serial.
	* Initializes the serial Interface (baudrate registers). The driver has do be 
	* enabled via powermanager @see PowerManager.
	* @param id :	1 for USART1 0 for USART0 
	* @param baud :	Which Baudrate should be used
	*/
	Serial(uint8 id, SerialRegisters::Baud baud);

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<char>* receiver, Sink0* sender);

	/**
	 * These method is called from the scheduler.
	 * It starts sending the next buffer from the queue.
	 */
	virtual void run();

	/**
	 * All data waiting for transmission is stored in this queue
	 * to get schedueled
	 */
	Queue<Buffer*> input;

protected:

	/**
	 * When the SCI receives data, it will be sent to the receiver
	 */
	Sink1<char>* receiver;

	/**
	 * The sender is notified, if last send request is finished
	 */
	Sink0* sender;

	/**
	 * holds the buffer, which is currently sending
	 */
	Buffer* current;

	/**
	 * pointer to the registers belonging to the USART1
	 */
	volatile struct SerialRegisters* regs;
	volatile struct SFRRegisters* sfrRegs;

	/**
	 * the length of the data, which has to be sent
	 */
	uint8 length;

	/**
	 * the position of the byte, which is sent next
	 */
	char* pos;

	/**
	 * number which serial USART is used (0 or 1 )
	 */
	uint8 id;
};

}// end namespace

#endif
