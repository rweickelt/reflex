#ifndef Interruptvector_h
#define Interruptvector_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InterruptVector
 *
 *	Author:	     Carsten Schulze
 *
 *	Description: InterruptVector for MSP430
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

namespace reflex {

namespace msp430 {
namespace interrupts {
/**
 * It is just a nummeration of the possible Interrupts,
 * each Interrupt get its own number, so InterruptGuardian::handle
 * can identify, witch handler is called.
 * The interrupts 0-incl.9 and 11-incl.13 are device specific
 * That is why here are the most used (or used so far) sources
 * for the interrupts given.
 */
enum InterruptVector {
	INTERRUPT_TABLE    = 0xffe0, /// lowest adr of interrupt table
	INVALID_INTERRUPT  = -1,/// no interrupt, only a useful dummy
	DAC12_DMA          = 0, /// lowest, mostly DMA or DAC12
	P2                 = 1, /// multiple sources from Port 2
	INTVEC_USART1_TXI  = 2, /// USART1 transmit interrupt
	INTVEC_USART1_RXI  = 3, /// USART1 receive interrupt
	P1                 = 4, /// multiple sources from Port 1
	TIMER_A1_A2_TAR    = 5, /// depending on module (Timer A 1-2)
	TIMER_A0           = 6, /// depending on module (Timer A0)
	ADC12              = 7, /// ADC12
	INTVEC_USART0_TXI  = 8, /// USART0 transmit interrupt
	INTVEC_USART0_RXI  = 9, /// USART0 receive interrupt
	WDTIFG             = 10, /// WATCHDOG TIMER on all MSP430
	COMPARATOR         = 11, /// Comparator A
	TIMER_B1_TBR       = 12, /// depending on module (Timer B 1-?)
	TIMER_B0           = 13, /// depending on module (Timer B0)
	NMIOSCFMAV         = 14, /// NMI, OScilator fault, Flash acc. violation
	RESET              = 15,/// highest, RESET (Watchdog,power up...)
	MAX_HANDLERS = 16 /// No interrupt,only number of interrupts
};
}//ns interrupts

typedef interrupts::InterruptVector InterruptVector;
}// ns msp430

} //reflex

#endif
