/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/timer/BasicClockModule.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/types.h"
#include "reflex/debug/Assert.h"

#include "NodeConfiguration.h"

using namespace reflex;
using namespace basicClockModule;

BasicClockModule::BasicClockModule()
{
	//volatile int* WDTCTL = (int*) 0x0120; //Init watchdog timer
	//*WDTCTL = (int) (0x5a00 | 0x0080) ; // WDTPW|WDTHOLD

	registers = (Registers*)mcu::BASIC_CLOCK_MODULE_BASE;
	timerARegisters = (timerA::Registers*) mcu::TIMERA_REGISTERS_BASE;

	registers->BCSCTL1 |= XT2OFF;
	//use of external Resistor for Oscillator (ROSC), lower temperature sensibility
	//all other bits are set to 0, DCO/1 -> MCLK, DCO/1 -> SMCLK
	registers->BCSCTL2 = DCOR_VALUE;

	registers->IE1 &= ~OFIE1; //disable oscilator fault interrupts

	calibrateDCO();
}

void BasicClockModule::calibrateDCO()
{
	//set DCO/1 as source for SMCLK
    registers->BCSCTL2 &= ~(SELS | DIVSx);
	//set LFXT1CLK/0 as source for ACLK
	registers->BCSCTL1 &= ~DIVAx;

	//set SMCLK/1 as input for TimerA, set to continous mode and reset TimerA
    timerARegisters->TACTL = timerA::TASSEL10;
	//set captureMode for TimerA channel 1 - detect rising edge
    //timerARegisters->TACCTL1 = timerA::CM01 | timerA::CCIS01;

	char rsel;
	char dco;

	int ticksLast;
	int ticksCurrent;
	for(rsel = 7; rsel>=0; rsel--) {
		setDCO(rsel,7,0);
		ticksLast = measureCalibrationPeriod();
		for(dco = 6; dco>=0; dco--) {
			setDCO(rsel,dco,0);
	    	ticksCurrent = measureCalibrationPeriod();
    		if(ticksCurrent <= CALIBRATE_TICKS){
    			goto hell;
    		}
    		ticksLast = ticksCurrent;
		}
	}
hell:
	char mod = ((CALIBRATE_TICKS - ticksCurrent) * 32) / (ticksLast - ticksCurrent);


	setDCO(rsel,dco,mod);
}

int BasicClockModule::measureCalibrationPeriod()
{
	//set SMCLK/1 as input for TimerA, set to continous mode and reset TimerA
    timerARegisters->TACTL = timerA::TASSEL10 | timerA::MC10;
    timerARegisters->TACCTL2 = timerA::CCIS01;

/*    for(int i=0; i<100; i++){
        //await low level of low speed clock
    	while(timerARegisters->TACCTL2 & timerA::CCI);
        //await high level of low speed clock
    	while(!(timerARegisters->TACCTL2 & timerA::CCI));
	}
*/

    //await low level of low speed clock
    while(timerARegisters->TACCTL2 & timerA::CCI);

	//reset counter
    timerARegisters->TAR = 0;

	//clear COV flag, set captureMode for TimerA channel 2 - detect rising edge
    timerARegisters->TACCTL2 = timerA::CM01 | timerA::CCIS01 | timerA::CAP;

    //await high level of low speed clock
    while(!(timerARegisters->TACCTL2 & timerA::CCI));
	unsigned first = timerARegisters->TACCR2;

    //clear COV flag, set captureMode for TimerA channel 2 - detect rising edge
    timerARegisters->TACCTL2 = timerA::CM01 | timerA::CCIS01 | timerA::CAP;

    //await low level of low speed clock
    while(timerARegisters->TACCTL2 & timerA::CCI);

    //await high level of low speed clock
    while(!(timerARegisters->TACCTL2 & timerA::CCI));
	unsigned second = timerARegisters->TACCR2;

	//switch timer off
    timerARegisters->TACTL = 0;
	timerARegisters->TACCTL2 = 0;

    return second-first;
}

void BasicClockModule::setDCO(int rsel, int dco, int mod)
{
	registers->DCOCTL = (dco<<5) | mod;
	registers->BCSCTL1 = XT2OFF | rsel;
}

