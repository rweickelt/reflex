#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# */

ASM_SOURCES_CONTROLLER += \
	interrupts/interruptFunctions.S \
	power.S

CC_SOURCES_CONTROLLER += \
	stop.cc\
	compiler/delete.cc \
	compiler/gcc_virtual.cc \
	interrupts/MachineInterruptGuardian.cc \
	interrupts/PortInterruptGuardian.cc \
	interrupts/PortInterruptHandler.cc \
	powerManagement/ControllerPowerManagement.cc \
	memory/memcpy.cc \
	timer/BasicClockModule.cc \
	timer/HardwareTimerMilli.cc \
	timer/TimerA.cc	\
	timer/TimerB.cc

# msp430 uses virtualized timer facility
CC_SOURCES_LIB += \
	timer/VirtualTimer.cc \
	timer/VirtualizedTimer.cc \
