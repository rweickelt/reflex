;/*
; *	REFLEX - Real-time Event FLow EXecutive
; *
; *	A lightweight operating system for deeply embedded systems.
; *
; *
; *	Class(ses):
;  
; *	Author:		Sörem Höckner
; *
; *	Description:	assembler interrupt stuff
; */
.global _interruptsEnable
.global _interruptsDisable

.section .startup
	jmp main

.section .text

_interruptsEnable:
	ser r24
	brie _intEnable1
	com r24
_intEnable1:
	sei
	ret

_interruptsDisable:
	ser r24
	brie _intDisable1
	com r24
_intDisable1:
	cli
	ret
.global RESET
.global _reset
_reset:
RESET:
	; r1 is assumed to be always zero, so clear it right now
	eor r1, r1
	; initialize stack pointer
	ldi r16, lo8(__stack)
	out 0x3d, r16
	ldi r16, hi8(__stack)
	out 0x3e, r16
	rjmp __start



.section .text
.macro wrapper p
.global _wrapper_\p
_wrapper_\p:
	push r24
	ldi r24, \p
	rjmp _wrapper_body
.endm

.irp i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33
wrapper \i
.endr

_wrapper_body:
	; r1 - r5 seems not to be used from gcc
	push r0
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	push r16
	push r17
	push r18
	push r19
	push r20
	push r21
	push r22
	push r23
	push r25
	push r26
	push r27
	push r28
	push r29
	push r30
	push r31

	; not needed on unsigned in handle(unsigned char) method
	;mov r25, r1 ; clear hByte of function parameter of type unsigned
	
	in r26, 0x3f ;save the status register at port-adr 0x3f
	push r26

	;rcall handle
	call handle

	pop r26
	out 0x3f, r26

	pop r31
	pop r30
	pop r29
	pop r28
	pop r27
	pop r26
	pop r25
	pop r23
	pop r22
	pop r21
	pop r20
	pop r19
	pop r18
	pop r17
	pop r16
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r0
	pop r24
	
	reti

.section .vectors

vectors:
	jmp RESET
.irp i 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33
	jmp _wrapper_\i
.endr


