##
#	Makefile to compile all examples applications for all platforms or even one
#	application for all platforms.
#	make all; make clean
#	make HelloWorld
#
#	invoke "make CONTINUE=1 target" if make should'nt fail if a platform fails
##

#default Makefile which is assumed in every example application
MAKEFILE=Makefile_default

#find all paths of the MAKEFILE
SUB_MAKEFILES=$(sort $(shell find ./ -iname ${MAKEFILE}))

#take all subdirs from current directory, excluding files
#NOTE: this needs the "ls -p" command
SUB_DIRS=$(sort $(filter %/,$(shell ls -p)))

QTCREATOR_PROJECT_NAME=reflex

#SCM_FILES_CMD=hg locate
SCM_FILES_CMD=git ls-files
#SCM_FILES_CMD=svn list --depth infinity

QTCREATOR_PRJ_FILES_FILE=$(QTCREATOR_PROJECT_NAME).files


#force make to build my targest
.PHONY: all clean update_creator ${SUB_MAKEFILES} ${SUB_DIRS} $(QTCREATOR_PRJ_FILES_FILE)

EXEC_SUBMAKE=${IGNOREMARK}@$(MAKE) -C $(dir $@) -f $(notdir $@) all

all: ${SUB_MAKEFILES}

clean: $(addsuffix .clean,${SUB_MAKEFILES})

#calls the submakes
${SUB_MAKEFILES}:
  ifdef CONTINUE
	#ignores the returnvalue and resumes execution
	-$(EXEC_SUBMAKE)
  else
	#fails on executions
	$(EXEC_SUBMAKE)
  endif

%.clean:
	@$(MAKE) -C $(dir $@) -f $(notdir $(subst .clean,,$@)) clean

%:
	@echo -e "use make all \t\tto build all examples and all platforms"
	@echo -e "use make target\t\tto build all platforms of one example application only"
	@echo -e "add the var CONTINUE=1 to the cmdline and the build will do all platforms even then one platform fails to build"
	@exit 1

##force evaluation of prerequisites
.SECONDEXPANSION:

#this targets matches each subdir
${SUB_DIRS}: $$(shell find $$@ -iname ${MAKEFILE})
	@echo $+
	@echo @$(MAKE) -C $(dir $@) -f $(notdir $(subst .clean,,$@)) clean
	@echo $@ DONEOC






### create files list for qtcreator, you want to use qtcreator

update_creator: $(QTCREATOR_PRJ_FILES_FILE)


$(QTCREATOR_PRJ_FILES_FILE):
	$(SCM_FILES_CMD) > $@
	@echo DONE

DOLLAR=$$

#$(PRJ_FILES_FILE): $(PRJ_FILES_FILE).cache
#	$(shell cat $<| sed "s/.*\/$(DOLLAR)//" >$@)
#	@echo DONE
