/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:	Karsten Walther
 */

//Distributor0
template<const unsigned char maxElems>
void Distributor<maxElems>::notify()
{
	for(unsigned char i=0;i<counter;i++){
		sinks[i]->notify();
	}
}

template<const unsigned char maxElems>
void Distributor<maxElems>::registerSink(Sink0& sink)
{
	InterruptLock lock;
   	if(counter < maxElems){
   		sinks[counter] = &sink;
   		counter++;
   	}
}

template<const unsigned char maxElems>
void Distributor<maxElems>::removeSink(Sink0& sink)
{
	InterruptLock lock;
	for(int i=0; i<counter; i++) {
		if(sinks[i] == &sink) {
			sinks[i] = 0;
			for(int j=i; j<counter; j++) {  //move following pointers
				sinks[j] = sinks[j+1];
			}
			counter--;
			break;
		}
	}
}

// Distributor1
template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::assign(T value)
{
	for(unsigned char i=0;i<counter;i++){
		sinks[i]->assign(value);
	}
}

template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::registerSink(Sink1<T>* sink)
{
	InterruptLock lock;
   	if(counter < maxElems){
   		sinks[counter] = sink;
   		counter++;
   	}
}


template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::removeSink(Sink1<T>* sink)
{
	InterruptLock lock;
	for(int i=0; i<counter; i++) {
		if(sinks[i] == sink) {
			sinks[i] = 0;
			for(int j=i; j<counter; j++) {
				sinks[j] = sinks[j+1];
			}
			counter--;
			break;
		}
	}
}
