#ifndef reflex_Packetizer_h
#define reflex_Packetizer_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	Packetizer
 *
 *	Author:		Soeren Hoeckner, Karsten Walther
 *
 *	Description:	Driver implementation for the Packetizer-Module
 *					The Packetizer-Module guarentee a one-to-one replicate
 *					of the sendBuffer at the receiver.
 *					ATTENTION: you need additional four byte for the header
 *						information.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/memory/Pool.h"
#include "reflex/sinks/Sink.h"
#include "reflex/memory/Buffer.h"

#include "reflex/debug/Assert.h"

namespace reflex
{

/**	This class holds the basic methods for building the Packet
 *
 */
class Packetizer :
			public Sink1<char>,
			public Sink1<Buffer*>
{
public:
	/**
	*	Constructor
	*/
	Packetizer(Pool* pool);

	/**
	 *	initialization.
	 *
	 *	@param lowerOut	sender object, which gets a Buffer (at the moment we got the serial)
	 *	@param upperOut	receiver object, which awaits a Buffer from the air
	 *	@param busyOut	notified then driver busy
	 **/
	void init( Sink1<Buffer*> *lowerOut, Sink1<Buffer*> *upperOut);

	/** upperIn.
	 *	Input from upper Layer.
	 *
	 *	@param buf Argument has to be derived from Buffer
	 **/
	virtual void assign(Buffer *buffer);

	/**	lowerIn
	 *	.
	 *	called asynchronously from driver we receive a byte-Stream
	 **/
	virtual void assign(char byte);

	/**
	 *	.
	 *	@return false if receiving
	 **/
	bool isBusy();

public:
	Pool* pool;/**< global pool-obj*/

protected:
	Sink1<Buffer*> *lowerOut; /**< to the driver */
	Sink1<Buffer*> *upperOut; /**< waits for buffers from the air*/

	Buffer	*receiveBuffer; /**< hold an unfinished received Buffer*/
	uint8 	receiveCount;	/**< counting bytes will be received */
	/** the tosOffset from the sent buffer */
	uint8 receiveOffset;

	unsigned preamble;

	enum State {
		IDLE = 0,
		SIZE = 1,
		CRC = 2,
		DATA = 3,
		OFFSET = 4
	};

	State state;
	char crc;
};






} // end namespace reflex

#endif
