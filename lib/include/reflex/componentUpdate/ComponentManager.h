#ifndef COMPONENTMANAGER_H
#define COMPONENTMANAGER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	ComponentManager, ComponentHeader
 *	Author:		Richard Weickelt <richard@weickelt.de>, 
 *                      Andre Sieber <as@informatik.tu-cottbus.de>
 *
 *	Description: Manages Loading and Unloading of Reflex-components
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reflex/flash/MemoryManager.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Fifo.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/types.h"
#include "reflex/lib/TList.h"
#include "conf.h"
#include "reflex/componentUpdate/Command.h"

namespace reflex
{

/**
 * ComponentManager is Responsible for Reflex-components
 * manages:
 *   - placement of components in memory
 *   - loading and unloading into system
 *
 * The ComponentManager is implemented as a state machine with short running
 * tasks to meet Reflex' Event-Flow model.
 *
 */
class ComponentManager
{

public:

	/**
	 * Component Header stores metadata for components
	 * This header is independent from the format, but not from the platform
	 * It can describe ELF16 and ELF32
	 */
	struct Header
	{
		uint16 id; // md4-hash of components name
		uint16 info; // flags how to treat the component
		void* startup; // locates startup function
		void* shutdown; // locates shutdown function
		caddr_t file; // pointer to section header table
		uint16 size; // overall size of component's binary data
	};

	/**
	 * Saves informations about the component's runtime state.
	 * It is set during the component startup and deleted when
	 * the component is stopped.
	 * This struct can be used as Element in a TList.
	 */
	struct RuntimeInfo
	{
		void* addr;
		Header* header;
		RuntimeInfo* next;

		/**
		 * This function is not needed, but we have to include it.
		 *
		 * @param than element to compare
		 * @return always false
		 */
		bool lessEqual(RuntimeInfo& than)
		{
			return false;
		}
	};

	/**
	 * Stores metadata of component's residenting in RAM.
	 * It is set during the component installation and deleted when
	 * the component is uninstalled.	 
	 * This struct can be used as Element in a TList.
	 */
	struct RamComponent
	{
		Header header;
		RamComponent* next;

		/**
		 * This function is not needed, but we have to include it.
		 *
		 * @param than element to compare
		 * @return always false
		 */
		bool lessEqual(RuntimeInfo& than)
		{
			return false;
		}
	};




	enum Info
	{
		PERSISTENT = 0x04,
		REPLACE = 0x08,
		AUTOSTART = 0x10
	};

	enum State
	{
		IDLE = 0,
		RECEIVING_HEADER = 1,
		RECEIVING_COMPONENT = 2,
		STARTING_UP = 3,
		ERROR = -1
	};

	enum Respond
	{
		NO_SPACE_FOR_HEADER = 1,
		NO_SPACE_FOR_COMPONENT = 2,
		NO_SLOT_FOR_COMPONENTHEADER = 3,
		INSTALLATION_FAILED = 4,
		INSTALLATION_SUCCESSFUL = 5,
		STARTUP_FAILED = 6,
		STARTUP_SUCCESSFUL = 7,
		SHUTDOWN_FAILED = 8,
		SHUTDOWN_SUCCESSFUL = 9,
		COMPONENT_LIST = 10,
		UNINSTALL_FAILED = 11,
		UNINSTALL_SUCCESSFUL = 12,
		DATA_RECEIVED = 13,
		ABORT_SUCCESSFUL = 14,
		DATA_ALREADY_RECEIVED = 247,
		ALREADY_INSTALLED = 248,
		COMMAND_INVALID_RECEIVE = 249,
		PACKET_INVALID = 250,
		COMMAND_INVALID = 251,
		ALREADY_RUNNING = 252,
		UNKNOWN_COMPONENT = 253,
		ERROR_STATE = 254
	};

	/*
	 * Component header table lays in an independent region.
	 * G++ moves theses constants into ROM. It was just a try
	 * to reduce ugly casts in the source code.
	 */
	static Header* const BEGINOF_COMPONENTHEADERS;
	static Header* const ENDOF_COMPONENTHEADERS;

	/**
	 * Standard Constructor
	 *
	 * @param memoryManager dynamic memory management
	 * @param a pool to create buffers for pc-communication
	 */
	ComponentManager(MemoryManager* memoryManager, Pool* pool);

	/**
	 * Finds a component header by its id. Returns 0 if not present.
	 *
	 * @param id the components id (a hash value)
	 * @return pointer to component header or 0 if component not found
	 */
	Header* getComponentById(uint16 id);

	/**
	 * Counts number of permanently installed components;
	 *
	 * @return components number
	 */
	uint8 getROMcomponentNumber();

	/**
	 * Counts number of temporary installed components;
	 *
	 * @return components number
	 */
	uint8 getRAMcomponentNumber();

	/**
	 * Connects the CM to the outgoing communication device
	 * @param receiver
	 */
	void set_outgoingPacket_receiver(Sink1<Buffer*>* receiver)
	{
		this->outgoingPacket_receiver = receiver;
	}

	/**
	 * Returns the component manager's command input
	 *
	 * @return component manager's command input
	 */
	Sink2<Command, uint16>* get_command_input()
	{
		return &command_input;
	}

	/**
	 * Returns the component manager's packet input
	 *
	 * @return component manager's packet input
	 */
	Sink1<Buffer*>* get_packet_input()
	{
		return &packet_input;
	}

	/**
	 * Activity of command_handler
	 * Processes incoming commands and performs further actions.
	 */
	virtual void handleCommand();

	/**
	 * Activity of packet_handler
	 * Processes incoming packets received by a MAC service
	 * Packets contain a one byte command for the component manager
	 * followed by data.
	 */
	virtual void handlePacket();

	/**
	 * Prints a list of all installed components
	 * @return current state
	 */
	State listComponents();

	/**
	 * Receives and processes the component's header.
	 * Decides, how to continue with it.
	 * @return current state
	 */
	State receiveHeader();

	/**
	 * Forwards incoming packages to the flash or ram until complete component
	 * is received.
	 * @return current state
	 */
	State receiveComponent();

	/**
	 * Clears received component parts and returns to idle state
	 * @return current state
	 */
	State abortInstallation();

	/**
	 * Adds component permanent to component table.
	 *
	 * @return current state
	 */
	State installComponent();

	/**
	 * Removes Component from Component table if persistent
	 *
	 * @param comp pointer to component header
	 * @return current state
	 */
	State uninstallComponent(Header* comp);

	/**
	 * Integrates a component in to the Reflex event flow.
	 *
	 * @param comp pointer to component header
	 * @return current state
	 */
	State startupComponent(Header* comp);

	/**
	 * Starts all installed components as startupComponent() does with one
	 * @return current state
	 */
	State startupAllComponents();

	/**
	 * Revokes all registrations made during startupComponent()
	 *
	 * @param comp pointer to component header
	 * @return current state
	 */
	State shutdownComponent(Header* comp);

	/**
	 * Sends a message back to the host. The message has to fit into one Buffer.
	 * The message contains custom data provided in a buffer with a trailing
	 * argument msg of the type Respond, which identifies the packet.
	 *
	 * @param msg main content
	 * @param buffer
	 */
	void respond(Respond msg, Buffer* buffer = 0);

	State state; //current state


	Buffer* buffer;
	Pool* pool;
	reflex::MemoryManager* memoryManager;

	Header* component;

	TList<RuntimeInfo> runtimeInfo_list;
	TList<RamComponent> RamComponent_list;

	caddr_t receive_pos;
	uint16 bytes_received;
	uint8 acknowledgement_request;

	ActivityFunctor<ComponentManager, &ComponentManager::handleCommand> command_handler;
	ActivityFunctor<ComponentManager, &ComponentManager::handlePacket> packet_handler;

	Fifo2<Command, uint16, 8> command_input; // receives commands and notifications from system
	Fifo1<Buffer*, 3> packet_input; // is notified when a packet has been received
	Sink1<Buffer*>* outgoingPacket_receiver; // sink for answers and notifications to system

};
}
#endif
