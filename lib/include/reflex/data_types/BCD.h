/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_LIB_DATA_TYPES_BCD_H
#define REFLEX_LIB_DATA_TYPES_BCD_H
#include "reflex/types.h"
#include "reflex/data_types/Partial.h"
#include "reflex/data_types/Encode.h"

namespace reflex{

//! encoded data types
namespace data_types {
/*! \ingroup DataTypes @{ */

	//! presents two binary coded dezimals, also 2 digets
	/*!
		\tparam	TCount	number of digits presented with this type
	*/
	template<size_t TCount> class BCD;

	template<>
	class BCD<2>
		: public data_types::Partial<uint8> //holds the uin8 value and provides easy access to it
	{
		friend class Encode<BCD>;
	public:
		typedef data_types::Partial<uint8> ValueType;
		typedef ValueType::LowType LowType;
		typedef ValueType::HighType HighType;

	public:
		BCD()	{this->value=0;}
		//! construct a new BCD type with a bcd encoded value
		/*! \param value bcd encoded value	*/
		BCD(const uint8& value)	{this->value=value;}
	public:
		//! encode a binary coded value into BCD
		void encode(const uint8&);
		//! decode the stored BCD into a binary encoded one
		uint8 decode() const;
	protected:
	};

	template<>
	class BCD<4>
		: public data_types::Partial<uint16>// holds the 4 Byte value and provides easy access to it
	{
		friend class Encode<BCD>;
	public:
		typedef data_types::Partial<uint16> ValueType;
		typedef ValueType::LowType LowType;
		typedef ValueType::HighType HighType;
	public:
		//! construct an empty BCD obj. '0000'
		BCD()	{this->value=0;}
		//! construct a new BCD type with a bcd encoded value
		/*! \param value bcd encoded value	*/
		BCD(const uint16& value) {this->value=value;}

	public:
		//! encode a binary coded value into BCD
		void encode(const uint16&);
		//! decode the stored BCD into a binary encoded one
		uint8 decode() const;
	protected:
	};


/*! @} */

}}
#endif // BCD_H
