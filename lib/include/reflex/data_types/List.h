/*
 * List.h
 *
 *  Created on: 30.08.2011
 *      Author: sbuechne
 */

#ifndef LIST_H_
#define LIST_H_

#include "reflex/data_types/ChainLink.h"
#include "reflex/debug/Assert.h"

namespace reflex {
namespace data_types{

class List
{
public:
	List()
	{
		front.unlink();
	}

	void enqueueFront(ChainLink* newElem)
	{
		Assert(!newElem->isLinked());
		newElem->next = front.next;
		front.next = newElem;
	}

	ChainLink* dequeueFront()
	{
		if(front.next == NULL)
			return NULL;

		ChainLink* dummy = front.next;
		front.next = dummy->next;
		dummy->unlink();

		return dummy;
	}

	void copyElementsTo(List *list)
	{
		ChainLink* dummy = NULL;

		while(front.next != NULL)
		{
			dummy = front.next;
			front.next = front.next->next;

			dummy->unlink();

			list->enqueueFront(dummy);
		}
	}

	void begin()
	{
		current = &front;
	}

	void next()
	{
		current = current->next;
	}

	void removeCurrent()
	{
		ChainLink* dummy = current->next;
		current->next = dummy->next;
		dummy->unlink();
	}

	ChainLink* getCurrent()
	{
		return current->next;
	}

	bool end()
	{
		return current->next == NULL;
	}


	ChainLink* top()
	{
		return front.next;
	}
private:
	ChainLink front;
	ChainLink* current;
};

}
}


#endif /* LIST_H_ */
