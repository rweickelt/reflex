/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	VirtualizedTimer
 *
 *	Author:		Stefan Nuernberger
 **/

#include "reflex/timer/VirtualizedTimer.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/debug/Assert.h"

using namespace reflex;

/**
 * constructor
 */
VirtualizedTimer::VirtualizedTimer()
{
    active = false;
    registered = 0;
    last_update = 0;
    min_remaining = MAX_DELTA;
    hardware_alarm.init(this); // run activity on hardware timer alarm
}

/**
 * setHardwareTimer
 * @param timer HardwareTimer to be virtualized
 */
void VirtualizedTimer::setHardwareTimer(HardwareTimer *timer) {
    hwtimer = timer;
}

/*!
 \brief Adds a new VirtualTimer instance \a timer to the system.

 This method fails when the amount of timers exceeds \a VIRTUAL_TIMERS
 or when the timer has been already registered.

 \sa removeTimer()

 */
void VirtualizedTimer::registerTimer(VTimerBase *timer) {
    Assert(registered < VIRTUAL_TIMERS);
    timers[registered] = timer;
    registered++;
}

/*!
 \brief Removes the instance \a timer from the system.

 It may then be safely destroyed. Please ensure, that \a timer has been
 stopped before.

 This method fails, if the timer has not been registered.

 \sa registerTimer()
 */
void VirtualizedTimer::removeTimer(VTimerBase *timer) {
    InterruptLock lock;
    for (uint8 i = 0 ; i < registered; i++)
    {
        if (timers[i] == timer)
        {
            registered--;
            timers[i] = timers[registered];
            return;
        }
    }

    Assert(false); 	// Timer not found
}


/**
 * run
 * main method of Activity. Get current time
 * and update timers accordingly.
 */
void VirtualizedTimer::run() {
    Time now = hwtimer->getNow();

    /**
     * NOTE: We have to prevent concurrent changes to any associated VTimerBase
     * while this method is running! For now we disable preemption completely.
     */
    InterruptLock lock;

    last_update = now;
    min_remaining = MAX_DELTA;
    active = false;

    for (uint8 i = 0; i != registered; ++i) {
        if (timers[i]->isRunning) {
            Time elapsed = now - timers[i]->t0;
            int32 remaining = timers[i]->delta - elapsed; // needs to be signed.

            if (remaining <= 0) {
                // fire and reset virtual timer
                if (timers[i]->isOneshot) { // ONESHOT
                    timers[i]->isRunning = false; // deactivate timer
                    timers[i]->notify();
                    continue; // next timer, skip update of min_remaining
                } else { // PERIODIC
                    /* If the interrupt handling takes too long, and
                     * timer interrupts happen too frequently, we issue all
                     * missed/pending interrupts. This is important when we
                     * trigger a Sink0 (e.g. the Prescaler). When triggering
                     * an Event, the additional notifies are silently discarded.
                     * We MUST HAVE positive remaining time (>0) after
                     * this loop, since min_remaining is unsigned!
                     */
                    do {
                        remaining += timers[i]->delta; // update remaining
                        timers[i]->t0 += timers[i]->delta; // update timer start
                        timers[i]->notify();
                    } while (remaining <= 0);
                }
            } // if timer expired

            // update remaining interval
            if ((Time)remaining <= min_remaining) {
                min_remaining = remaining;
                active = true;
            }
        } // if timer running
    } // for registered timers

    // set timer
    if (active)
        hwtimer->startAt(now, min_remaining);
}

/**
 * set
 * update a value for a VirtualTimer
 */
void VirtualizedTimer::set(VTimerBase *timer) {
    if (timer->delta == 0) {
        timer->isRunning = false; // deactivate because stopped
        return; // no need to update timers
    }

    timer->isRunning = true;
    timer->t0 = hwtimer->getNow(); // update start time

    /* only update if no other timer will expire in time */
    Time expire = last_update - timer->t0 + min_remaining;
    if (!active || (timer->delta < expire)) {
        // update needs to be atomic
        InterruptLock lock;
        // this timer will be the next to fire...
        last_update = timer->t0;
        min_remaining = timer->delta;
        active = true;
        // set hardware timer for next timeout
        hwtimer->startAt(last_update, min_remaining);
    }
}

/**
 * disableAll
 * switch off all registered virtual timers
 * @param prioritized whether prioritized timers are switched off, too
 */
void VirtualizedTimer::disableAll(bool prioritized) {
    InterruptLock lock;
    for (int i = 0; i != registered; ++i)
        if (!timers[i]->priority || prioritized)
            timers[i]->isRunning = false;
}
