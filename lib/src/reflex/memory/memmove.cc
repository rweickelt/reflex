/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author: Hannes Menzel
 */

#include "reflex/memory/memmove.h"

namespace reflex {

void* memmove(void *dest, void *src, size_t size)
{

	char *p1 = (char*)dest;
	 char *p2 = ( char*)src;

	/* check for overlapping memory regions */
	if (p2 < p1 && p1 < p2 + size) {
		p2 += size;
		p1 += size;
		while (size-- != 0)
			*--p1 = *--p2;
	} else {
		while (size-- != 0)
			*p1++ = *p2++;
	}
	return dest;

}

}//reflex
