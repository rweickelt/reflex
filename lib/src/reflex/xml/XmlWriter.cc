#include "reflex/memory/Pool.h"
#include "reflex/xml/MemoryPolicy.h"
#include "reflex/xml/XmlHandler.h"
#include "reflex/xml/XmlTagTable.h"
#include "reflex/xml/XmlWriter.h"

using namespace reflex;
using namespace xml;

XmlWriter::XmlWriter() :
	input_message(&act_startMessage), act_startMessage(*this), act_bufferSent(*this),
			act_pollRequest(*this)
{
	this->input_chunkHandled.init(&this->act_bufferSent);
	this->input_pollRequest.init(&this->act_pollRequest);
	this->state = Idle;
}

void XmlWriter::init(XmlTagTable* table, Pool* pool)
{
	this->table = table;
	this->pool = pool;
	this->tag = new (pool) Buffer(pool);
	Assert(this->tag);
	this->txBuffer = new (pool) Buffer(pool);
	Assert(this->txBuffer);
}

void XmlWriter::connectTo(Sink1<Buffer*>* input_sendBuffer)
{
	this->output_chunk = input_sendBuffer;
}

/**
 * This activity will only initiate the parsing process.
 */

void XmlWriter::run_startMessage()
{
	act_startMessage.lock();
	act_pollRequest.lock();

	input_message.get(this->filePos, this->output_endDocument);

	state = ScanForElement;
	/* The first parser iteration */
	this->parse();
}

void XmlWriter::run_pollRequest()
{
	tag->reset();
	const LookupEntry* entry = input_pollRequest.get();

	/* Copy tag string into buffer */
	for (uint8 pos = 0; char c = MemoryPolicy::read(&entry->tag[pos]); pos++)
	{
		tag->write(c);
	}

	handler = MemoryPolicy::read(&entry->handler);

	if (handler != 0)
	{
		/* Lock inputs as long as the request is handled */
		act_startMessage.lock();
		act_pollRequest.lock();

		state = PollStartElement;
		/* Start first iteration */
		this->parse();
	}
}

/*
 * The state machine always starts with state Idle. During every
 * iteration, a char from flash is being read, parsed and the next
 * state is set. Once a XML start, end or empty element has been found,
 * a handler is called and the parser will remain in the current file
 * position and state as long as the handler provides data.
 * This method runs at least until txBuffer is full or an event handler
 * is called which will always result in txBuffer being sent.
 */

void XmlWriter::parse()
{
	bytesLeft = txBuffer->getBufferSize();
	while (bytesLeft > 0)
	{
		if (state < AfterStartElement)
		{
			c = MemoryPolicy::read(filePos);
			filePos++;
			/* A null character terminates a message and has to end up rendering in any case */
			if (c == '\0')
			{
				state = EndDocument;
			}
			else
			{
				txBuffer->write(c);
				bytesLeft--;
			}
		}

		/*
		 * The msp-gcc produces nice jump-tables,
		 * the avr-gcc only for atmegas with flash < 64KiB :-(
		 */
		switch (state)
		{
		/* States where the message skeleton is read from Flash */
		case Idle:
			handleScanForElement();
			break;
		case ScanForElement:
			handleScanForElement();
			break;
		case ElementOpened:
			handleElementOpened();
			break;
		case IgnoreElement:
			handleIgnoreElement();
			break;
		case StartElement:
			handleStartElement();
			break;
		case AttributesOrEmptyElement:
			handleAttributesOrEmptyElement();
			break;
		case EmptyElement:
			handleEmptyElement();
			break;
		case EndElement:
			handleEndElement();
			break;
		case EndDocument:
			handleEndDocument();
			break;
			/* States where XmlHandlers fill content into the message */
		case AfterStartElement:
			handleAfterStartElement();
			break;
		case AfterEndElement:
			handleAfterEndElement();
			break;
		case AfterEmptyElement:
			handleAfterEmptyElement();
			break;
			/* States for poll requests */
		case PollStartElement:
			handlePollStartElement();
			break;
		case PollContent:
			handlePollContent();
			break;
		case PollEndElement:
			handlePollEndElement();
			break;
		case AfterPollEndElement:
			break;
		}

		/*
		 * I have thought a long time, whether to let the handlers check the remaining buffer
		 * size or not. I have also spend some hours to test a robust implementation and came up to
		 * the decision, that to guarantee every handler an empty txBuffer is the best approach.
		 * This will make handler implementation more easy and fast although the buffers may not
		 * be filled optimally.
		 */
		if (state >= AfterStartElement)
		{
			bytesLeft = 0;
		}
	}

	if (output_chunk)
	{
		output_chunk->assign(txBuffer);
		txBuffer = 0;
	}
	else
	{
		txBuffer->reset();
	}

	if (this->state == EndDocument)
	{
		if (this->output_endDocument)
		{
			this->output_endDocument->notify();
		}
		state = Idle;
	}
	else if (this->state == AfterPollEndElement)
	{
		if (this->output_pollFinished)
		{
			this->output_pollFinished->notify();
		}
		state = Idle;
	}
}

void XmlWriter::handleScanForElement()
{
	if (c == '<')
	{
		state = ElementOpened;
	}
}

void XmlWriter::handleElementOpened()
{
	if (c == '/')
	{
		state = EndElement;
	}
	else if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')))
	{
		tag->reset();
		tag->write(c);
		state = StartElement;
	}
	else
	{
		state = IgnoreElement;
	}
}

void XmlWriter::handleIgnoreElement()
{
	if (c == '>')
	{
		state = ScanForElement;
	}
}

void XmlWriter::handleStartElement()
{
	if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || (c == '-') || (c == '_'))
	{
		tag->write(c);
	}
	else if (c == '>')
	{
		tag->write('\0'); // terminate tag for lookup procedure
		state = AfterStartElement;
	}
	else if (c == '/')
	{
		state = EmptyElement;
	}
	else if (c == ' ')
	{
		state = AttributesOrEmptyElement;
	}
}

void XmlWriter::handleAttributesOrEmptyElement()
{
	if (c == '/')
	{
		state = EmptyElement;
	}
	else if (c == '>')
	{
		tag->write('\0'); // terminate tag for lookup procedure
		state = AfterStartElement;
	}
}

void XmlWriter::handleEmptyElement()
{
	if (c == '>')
	{
		tag->write('\0'); // terminate tag for lookup procedure
		state = AfterEmptyElement;
	}
}

void XmlWriter::handleEndElement()
{
	if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || (c == '-') || (c == '_'))
	{
		tag->write(c);
	}
	else if (c == '>')
	{
		tag->write('\0'); // terminate tag for lookup procedure
		state = AfterEndElement;
	}
}

void XmlWriter::handleEndDocument()
{
	bytesLeft = 0;
}

void XmlWriter::handleAfterStartElement()
{
	handler = table->handlerByName(reinterpret_cast<char*> (tag->getStart()));
	uint8 action = XmlHandler::XmlNoAction;
	if (handler)
	{
		action = handler->handleSendEvent(XmlHandler::XmlAfterStartElement, txBuffer);
	}

	if (action == XmlHandler::XmlNoAction)
	{
		state = ScanForElement;
	}
}

void XmlWriter::handleAfterEndElement()
{
	uint8 action = XmlHandler::XmlNoAction;
	if (handler)
	{
		action = handler->handleSendEvent(XmlHandler::XmlAfterEndElement, txBuffer);
	}

	/* Normal case: One buffer filled, proceed with XML parsing. */
	if (action == XmlHandler::XmlNoAction)
	{
		handler = 0;
		state = ScanForElement;
	}
	else if (action & XmlHandler::XmlMoreBuffers)
	{
		/* Do not change state, wait for the next iteration */
	}
}

void XmlWriter::handleAfterEmptyElement()
{
	handler = table->handlerByName(reinterpret_cast<char*> (tag->getStart()));
	if (handler)
	{
		handler->handleSendEvent(XmlHandler::XmlAfterEmptyElement, txBuffer);
	}
	state = ScanForElement;
}

/**
 * Constructs a start element from the tag name.
 * txBuffer will be sent automatically on return.
 */

void XmlWriter::handlePollStartElement()
{
	txBuffer->write('<');
	txBuffer->write(tag->getStart(), tag->getLength());
	txBuffer->write('>');
	state = PollContent;
}

/**
 * Let the handler fill one buffer.
 */

void XmlWriter::handlePollContent()
{
	handler->handleSendEvent(XmlHandler::XmlAfterStartElement, txBuffer);
	state = PollEndElement;
}

void XmlWriter::handlePollEndElement()
{
	txBuffer->write('<');
	txBuffer->write('/');
	txBuffer->write(tag->getStart(), tag->getLength());
	txBuffer->write('>');
	state = AfterPollEndElement;
}

void XmlWriter::run_bufferSent()
{
	txBuffer = new (pool) Buffer(pool); // get back the buffer

	if (state > Idle)
	{
		this->parse();
	}
	else
	{
		act_startMessage.unlock();
		act_pollRequest.unlock();
	}
}

