/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Maik Kr�ger, Karsten Walther
 */
#include "reflex/misc/Timetable.h"

namespace reflex {

void Timetable::removeActivity(Activity *activity)
{
	unsigned char i = 0;
	for ( ; i<counter; i++)
		if (act[i] == activity) break;
	if (i < counter) removeActivity(i);
}

void Timetable::registerActivity(Activity* act, unsigned int millisec)
{
	if(counter < maxElems){
		this->act[counter] = act;
		countdown[counter] = millisec/TICK_PERIOD_MSEC;
		counter++;
	}
}

void Timetable::notify()
{
	for(unsigned char i=0; i<counter; i++){
		if ( !(--countdown[i]) ){
			act[i]->trigger();
			removeActivity(i);
		}
	}
}

};

