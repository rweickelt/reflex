/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/io/InteractiveElement.h"

using namespace reflex;

void InteractiveElement::init(const char* name)
{
	id = system.ioManager.registerOutputElement(this);
}

