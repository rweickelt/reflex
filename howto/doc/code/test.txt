reflex
|-- applications
|-- controller
|   |-- Atmega128
|   |-- H8300
|   |-- HCS12
|   |-- MIPS32
|   |-- MSP430
|   `-- linux
|-- devices
|-- doc
|-- lib
|-- platform
|   |-- CardS12
|   |-- ESB
|   |-- MICA2
|   |-- MICA2DOT
|   |-- Mega128
|   |-- OMNetPP
|   |-- RCX
|   |-- SERNet
|   |-- SK-XC164CS
|   |-- TMOTESKY
|   `-- guest
`-- system
