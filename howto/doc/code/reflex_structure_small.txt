reflex
|-- system 
|-- lib
|-- controller
|   |-- Atmega128
|   |-- MSP430
|   |-- ...
|   `-- linux
|-- platform
|   |-- ESB
|   |-- Mega128
|   |-- OMNetPP
|   |-- RCX
|   |-- TMOTESKY
|   |-- ...
|   `-- guest
|-- devices
`-- applications/examples
