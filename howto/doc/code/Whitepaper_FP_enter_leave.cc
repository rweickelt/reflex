//is always called with interrupts off
void PriorityScheduler::enterScheduling()
{
    stackedSchedules++;

    //If there is a running PriorityActivity mark it INTERRUPTED.
    PriorityActivity* first = (PriorityActivity*)(readyList.first());
    if( (first!=0) ){
    	//because we entered a dispatch monitor, in front of the readyList
    	//is either an interrupted or a running activity, therefore the
    	//check is not needed, Note this is different for the 2-list
    	//FP-scheduler
        first->status = PriorityActivity::INTERRUPTED;
    }
}

//is always called with interrupts off
void PriorityScheduler::leaveScheduling()
{
	stackedSchedules--;
	if(stackedSchedules==0){
		dispatch();
	}
}
