Serial::Serial(uint8 id, SerialRegisters::Baud baud) :
	txHandler((InterruptVector)(INTVEC_USART0_TXI - (6*id)) ,*this,true),
	rxHandler((InterruptVector)(INTVEC_USART0_RXI - (6*id)) ,*this)
{
	//set deepest allowed sleep mode
	txHandler.setSleepMode(LPM0);
	rxHandler.setSleepMode(LPM0);
}

void Serial::handleRX()
{
	char tmp = regs->URXBUF;
	if (receiver) receiver->assign(tmp);
}

void Serial::run()
{
	this->lock();

	txHandler.switchOn();

	current = input.get();
	length = current->getLength();
	pos = (char*)current->getStart();

	length--;
	regs->UTXBUF = *pos++;
}

void Serial::enableRX()
{
	sfrRegs->ME |= SerialRegisters::URXE0  >> (id * 2);
	sfrRegs->IE |=  SerialRegisters::URXIE0 >> (id * 2);
}


void Serial::disableTX()
{
	sfrRegs->ME &= ~(SerialRegisters::UTXE0  >> (id * 2));
	sfrRegs->IE &= ~(SerialRegisters::UTXIE0 >> (id * 2));
}
