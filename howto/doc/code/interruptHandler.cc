#include "EXAMPLE.h"

//specify the interrupt source which schould be used 
EXAMPLE::EXAMPLE() : InterruptHandler(INTVEC_XY)
{
  this->setSleepMode(XYZ);
}
	
	
void EXAMPLE::enable()
{
  //enable interrupt source
}

void EXAMPLE::disable()
{
  //disable interrupt source
}

void EXAMPLE::handle()
{
  //do handling stuff here
}
