/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "Sense.h"

using namespace reflex;

Sense::Sense() : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1000);
}


void Sense::init(Sink1<char>* out)
{
	this->out = out;
}

#include "NodeConfiguration.h"
void Sense::run()
{
	getApplication().out.write('.');
	getApplication().out.flush();
	out->assign(5); //sense on channel 5
}
